package madcat.studio.dialogs;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;


public class UsageLimitDialog extends Dialog {
	private static final int PADDING = 10;
	
	private Context mContext = null;

	private LinearLayout mUsageLimitLayout = null;
	private EditText mUsageLimitResetDateInput = null, mUsageLimitNetworkInput = null, mUsageLimitCallInput = null, mUsageLimitSmsInput = null;
	private Button mSaveButton = null, mCancelButton = null;
	
	private int mResetDate = 1;
	private long mNetworkLimit = Constants.USAGE_LIMIT_DEFAULT_VALUE;
	private long mCallLimit = Constants.USAGE_LIMIT_DEFAULT_VALUE;
	private long mSmsLimit = Constants.USAGE_LIMIT_DEFAULT_VALUE;
	
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	public UsageLimitDialog(Context context, int styleId, int applicationTheme) {
		// TODO Auto-generated constructor stub
		super(context, styleId);
		mContext = context;
		mApplicationTheme = applicationTheme;
	
		SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mResetDate = pref.getInt(Constants.USAGE_LIMIT_RESET_DATE, Constants.USAGE_LIMIT_DEFAULT_DATE);
		mNetworkLimit = pref.getLong(Constants.USAGE_LIMIT_NETWOKR, Constants.USAGE_LIMIT_DEFAULT_VALUE);
		mCallLimit = pref.getLong(Constants.USAGE_LIMIT_CALL, Constants.USAGE_LIMIT_DEFAULT_VALUE);
		mSmsLimit = pref.getLong(Constants.USAGE_LIMIT_SMS, Constants.USAGE_LIMIT_DEFAULT_VALUE);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_usage_limit);
		
		mUsageLimitLayout = (LinearLayout)findViewById(R.id.dialog_usage_limit_layout);
		
		mUsageLimitResetDateInput = (EditText)findViewById(R.id.dialog_usage_limit_reset_input);
		mUsageLimitNetworkInput = (EditText)findViewById(R.id.dialog_usage_limit_network_input);
		mUsageLimitCallInput = (EditText)findViewById(R.id.dialog_usage_limit_call_input);
		mUsageLimitSmsInput = (EditText)findViewById(R.id.dialog_usage_limit_sms_input);
		
		mSaveButton = (Button)findViewById(R.id.dialog_usage_limit_save_button);
		mCancelButton = (Button)findViewById(R.id.dialog_usage_limit_cancel_button);
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mUsageLimitLayout.setBackgroundResource(R.drawable.dialog_background_pisces);
			mSaveButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mCancelButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mUsageLimitLayout.setBackgroundResource(R.drawable.dialog_background_aries);
			mSaveButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mCancelButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mUsageLimitLayout.setBackgroundResource(R.drawable.dialog_background_taurus);
			mSaveButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mCancelButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		mUsageLimitLayout.setPadding(PADDING, PADDING, PADDING, PADDING);
		
		EditTextEventHandler editTextEventHandler = new EditTextEventHandler();
		mUsageLimitResetDateInput.setOnFocusChangeListener(editTextEventHandler);
		mUsageLimitNetworkInput.setOnFocusChangeListener(editTextEventHandler);
		mUsageLimitCallInput.setOnFocusChangeListener(editTextEventHandler);
		mUsageLimitSmsInput.setOnFocusChangeListener(editTextEventHandler);
		
		ButtonEventHandler buttonEventHandler = new ButtonEventHandler();
		mSaveButton.setOnClickListener(buttonEventHandler);
		mCancelButton.setOnClickListener(buttonEventHandler);
		
		mUsageLimitResetDateInput.setText(mContext.getString(R.string.usage_dialog_limit_reset_prefix) + mResetDate + mContext.getString(R.string.usage_dialog_limit_reset_postfix));
		if (mNetworkLimit == 0) {
			mUsageLimitNetworkInput.setText(mContext.getString(R.string.usage_dialog_unlimited));
		} else {
			mUsageLimitNetworkInput.setText(mNetworkLimit + mContext.getString(R.string.usage_dialog_limit_network_postfix));
		}
		if (mCallLimit == 0) {
			mUsageLimitCallInput.setText(mContext.getString(R.string.usage_dialog_unlimited));
		} else {
			mUsageLimitCallInput.setText(mCallLimit + mContext.getString(R.string.usage_dialog_limit_call_postfix));
		}
		if (mSmsLimit == 0) {
			mUsageLimitSmsInput.setText(mContext.getString(R.string.usage_dialog_unlimited));
		} else {
			mUsageLimitSmsInput.setText(mSmsLimit + mContext.getString(R.string.usage_dialog_limit_sms_postfix));
		}
		
		mUsageLimitLayout.requestFocusFromTouch();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		dismiss();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
	}
	
	private class ButtonEventHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.dialog_usage_limit_save_button:
				SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE).edit();
				editor.putInt(Constants.USAGE_LIMIT_RESET_DATE, mResetDate);
				editor.putLong(Constants.USAGE_LIMIT_NETWOKR, mNetworkLimit);
				editor.putLong(Constants.USAGE_LIMIT_CALL, mCallLimit);
				editor.putLong(Constants.USAGE_LIMIT_SMS, mSmsLimit);
				editor.commit();
				break;
			case R.id.dialog_usage_limit_cancel_button:
				break;
			}
			
			dismiss();
		}
	}
	
	private class EditTextEventHandler implements View.OnFocusChangeListener {
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			// TODO Auto-generated method stub
			if (hasFocus) {
				switch (v.getId()) {
				case R.id.dialog_usage_limit_reset_input:
					mUsageLimitResetDateInput.setText(mResetDate + "");
					break;
				case R.id.dialog_usage_limit_network_input:
					mUsageLimitNetworkInput.setText(mNetworkLimit + "");
					break;
				case R.id.dialog_usage_limit_call_input:
					mUsageLimitCallInput.setText(mCallLimit + "");
					break;
				case R.id.dialog_usage_limit_sms_input:
					mUsageLimitSmsInput.setText(mSmsLimit + "");
					break;
				}
			} else {
				switch (v.getId()) {
				case R.id.dialog_usage_limit_reset_input:
					if (!isValid(mUsageLimitResetDateInput.getText().toString(), CHECK_TYPE_DATE)) {
						mUsageLimitResetDateInput.setText(mContext.getString(R.string.usage_dialog_limit_reset_prefix) + mResetDate + mContext.getString(R.string.usage_dialog_limit_reset_postfix));
					}
					break;
				case R.id.dialog_usage_limit_network_input:
					if (!isValid(mUsageLimitNetworkInput.getText().toString(), CHECK_TYPE_NETWORK)) {
						if (mNetworkLimit == 0) {
							mUsageLimitNetworkInput.setText(mContext.getString(R.string.usage_dialog_unlimited));
						} else {
							mUsageLimitNetworkInput.setText(mNetworkLimit + mContext.getString(R.string.usage_dialog_limit_network_postfix));
						}
					}
					break;
				case R.id.dialog_usage_limit_call_input:
					if (!isValid(mUsageLimitCallInput.getText().toString(), CHECK_TYPE_CALL)) {
						if (mCallLimit == 0) {
							mUsageLimitCallInput.setText(mContext.getString(R.string.usage_dialog_unlimited));
						} else {
							mUsageLimitCallInput.setText(mCallLimit + mContext.getString(R.string.usage_dialog_limit_call_postfix));
						}
					}
					break;
				case R.id.dialog_usage_limit_sms_input:
					if (!isValid(mUsageLimitSmsInput.getText().toString(), CHECK_TYPE_SMS)) {
						if (mSmsLimit == 0) {
							mUsageLimitSmsInput.setText(mContext.getString(R.string.usage_dialog_unlimited));
						} else {
							mUsageLimitSmsInput.setText(mSmsLimit + mContext.getString(R.string.usage_dialog_limit_sms_postfix));
						}
					}
					break;
				}
			}
		}
	}
	
	private static final int CHECK_TYPE_DATE	= 0;
	private static final int CHECK_TYPE_NETWORK	= 1;
	private static final int CHECK_TYPE_CALL	= 2;
	private static final int CHECK_TYPE_SMS		= 3;
	
	private boolean isValid(String value, int type) {
		if (value == null || value.trim().equals("") || value.trim() == "") {
			Toast.makeText(mContext, "값을 입력해주세요.", Toast.LENGTH_SHORT).show();
			
			switch (type) {
			case CHECK_TYPE_DATE:
				mUsageLimitResetDateInput.requestFocusFromTouch();
				break;
			case CHECK_TYPE_NETWORK:
				mUsageLimitNetworkInput.requestFocusFromTouch();
				break;
			case CHECK_TYPE_CALL:
				mUsageLimitCallInput.requestFocusFromTouch();
				break;
			case CHECK_TYPE_SMS:
				mUsageLimitSmsInput.requestFocusFromTouch();
				break;
			}
			
			return false;
		} else {
			long temp = 0;
			try {
				temp = Long.parseLong(value);
			} catch(NumberFormatException e) {
				Toast.makeText(mContext, "올바른 값이 아닙니다.", Toast.LENGTH_SHORT).show();
				
				switch (type) {
				case CHECK_TYPE_DATE:
					mUsageLimitResetDateInput.requestFocusFromTouch();
					break;
				case CHECK_TYPE_NETWORK:
					mUsageLimitNetworkInput.requestFocusFromTouch();
					break;
				case CHECK_TYPE_CALL:
					mUsageLimitCallInput.requestFocusFromTouch();
					break;
				case CHECK_TYPE_SMS:
					mUsageLimitSmsInput.requestFocusFromTouch();
					break;
				}
				
				return false;
			}
			
			switch (type) {
			case CHECK_TYPE_DATE:
				if (temp >= 1 && temp <= 31) {
					mResetDate = (int)temp;
					mUsageLimitResetDateInput.setText(
							mContext.getString(R.string.usage_dialog_limit_reset_prefix) + value +
							mContext.getString(R.string.usage_dialog_limit_reset_postfix));
					return true;
				} else {
					Toast.makeText(mContext, "날짜는 1~31 사이의 값을 입력해야 합니다.", Toast.LENGTH_SHORT).show();
					mUsageLimitResetDateInput.requestFocusFromTouch();
					return false;
				}
			case CHECK_TYPE_NETWORK:
				if (temp >= 0) {
					if (temp > (1024 * 1024)) {	//Upper 100GB
						Toast.makeText(mContext, "너무 큰 숫자입니다. 무제한을 원하시면 0을 입력하세요.", Toast.LENGTH_SHORT).show();
						mUsageLimitResetDateInput.requestFocusFromTouch();
						return false;
					} else {
						if (temp == 0) {
							mUsageLimitNetworkInput.setText(mContext.getString(R.string.usage_dialog_unlimited));
						} else {
							mUsageLimitNetworkInput.setText(value + mContext.getString(R.string.usage_dialog_limit_network_postfix));
						}
						mNetworkLimit = temp;
						return true;
					}
				} else {
					mUsageLimitResetDateInput.requestFocusFromTouch();
					return false;
				}
			case CHECK_TYPE_CALL:
				if (temp > 60 * 24 * 31) {	//Upper 1 Month
					Toast.makeText(mContext, "너무 큰 숫자입니다. 무제한을 원하시면 0을 입력하세요.", Toast.LENGTH_SHORT).show();
					mUsageLimitCallInput.requestFocusFromTouch();
					return false;
				} else {
					if (temp == 0) {
						mUsageLimitCallInput.setText(mContext.getString(R.string.usage_dialog_unlimited));
					} else {
						mUsageLimitCallInput.setText(value + mContext.getString(R.string.usage_dialog_limit_call_postfix));
					}
					mCallLimit = temp;
					return true;
				}
			case CHECK_TYPE_SMS:
				if (temp > 10000 * 30) {	//Upper 3,000,000
					Toast.makeText(mContext, "너무 큰 숫자입니다. 무제한을 원하시면 0을 입력하세요.", Toast.LENGTH_SHORT).show();
					mUsageLimitCallInput.requestFocusFromTouch();
					return false;
				} else {
					if (temp == 0) {
						mUsageLimitSmsInput.setText(mContext.getString(R.string.usage_dialog_unlimited));
					} else {
						mUsageLimitSmsInput.setText(value + mContext.getString(R.string.usage_dialog_limit_sms_postfix));
					}
					mSmsLimit = temp;
					return true;
				}
			}
			
			return false;
		}
	}
}
