package madcat.studio.dialogs;

import java.util.ArrayList;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.utils.UsageData;
import madcat.studio.utils.Util;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class UsageDialog extends Dialog {
	public static final int USAGE_DIALOG_NETWORK = 0;
	public static final int USAGE_DIALOG_CALL = 1;
	public static final int USAGE_DIALOG_SMS = 2;
	
	private Context mContext = null;
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	private int mType = USAGE_DIALOG_NETWORK;

	private LinearLayout mUsageLayout = null;

	private RadioGroup mMenuButtonGroup = null;
	private RadioButton mMenuSummaryButton = null, mMenuGraphButton = null;
	
	private LinearLayout mSummaryLayout = null, mGraphLayout = null;
	
	//Summary Page Views
	private TextView mSummaryTitle = null, mSummaryDayTitle = null, mSummaryMonthTitle = null;
	
	private TextView mSummaryDayRxTitle = null, mSummaryDayTxTitle = null, mSummaryDayTotalTitle = null;
	private TextView mSummaryDayRxValue = null, mSummaryDayTxValue = null, mSummaryDayTotalValue = null;

	private TextView mSummaryMonthRxTitle = null, mSummaryMonthTxTitle = null, mSummaryMonthTotalTitle = null, mSummaryMonthRemainTitle = null;
	private TextView mSummaryMonthRxValue = null, mSummaryMonthTxValue = null, mSummaryMonthTotalValue = null, mSummaryMonthRemainValue = null;
	
	//Graph Page Views
	private TextView mGraphTitle = null, mGraphRemainTitle = null, mGraphDayTitle = null, mGraphMonthTitle = null;
	
	private ProgressBar mGraphRemainBar = null;
	private TextView mGraphRemainUsed = null, mGraphRemainFree = null;
	
	private RadioGroup mGraphDayToggleGroup = null, mGraphMonthToggleGroup = null;
	private RadioButton mGraphDayToggleRxBtn = null, mGraphDayToggleTxBtn = null;
	private RadioButton mGraphMonthToggleRxBtn = null, mGraphMonthToggleTxBtn = null;
	
	private TextView mGraphDayValues[] = {null, null, null, null, null};
	private ImageView mGraphDayGraphs[] = {null, null, null, null, null};
	private TextView mGraphDayRemarks[] = {null, null, null, null, null};
	
	private TextView mGraphMonthValues[] = {null, null, null, null, null};
	private ImageView mGraphMonthGraphs[] = {null, null, null, null, null};
	private TextView mGraphMonthRemarks[] = {null, null, null, null, null};
	
	private long mDayRxUsage[] = {0, 0, 0, 0, 0};
	private long mDayTxUsage[] = {0, 0, 0, 0, 0};
	
	private long mMonthRxUsage[] = {0, 0, 0, 0, 0};
	private long mMonthTxUsage[] = {0, 0, 0, 0, 0};
	
	//0 : Network, 1 : Call, 2 : SMS
	private long mLimitUsage[] = {Constants.USAGE_LIMIT_DEFAULT_VALUE, Constants.USAGE_LIMIT_DEFAULT_VALUE, Constants.USAGE_LIMIT_DEFAULT_VALUE};
	private int mLimitResetDate = Constants.USAGE_LIMIT_DEFAULT_DATE;
	
	public UsageDialog(Context context, int styleId, int type, int applicationTheme) {
		// TODO Auto-generated constructor stub
		super(context, styleId);
		mContext = context;
		mApplicationTheme = applicationTheme;
		mType = type;
	}
	
	public void setUsage(long date) {
		SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE);
		
		mLimitUsage[0] = pref.getLong(Constants.USAGE_LIMIT_NETWOKR, Constants.USAGE_LIMIT_DEFAULT_VALUE) * 1024 * 1024;
		mLimitUsage[1] = pref.getLong(Constants.USAGE_LIMIT_CALL, Constants.USAGE_LIMIT_DEFAULT_VALUE);
		mLimitUsage[2] = pref.getLong(Constants.USAGE_LIMIT_SMS, Constants.USAGE_LIMIT_DEFAULT_VALUE);
		mLimitResetDate = pref.getInt(Constants.USAGE_LIMIT_RESET_DATE, Constants.USAGE_LIMIT_DEFAULT_DATE);
		
		UsageData dayData = new UsageData();
		ArrayList<UsageData> monthData = new ArrayList<UsageData>();
		
		long criteria = System.currentTimeMillis();
		
		for (int i = 0; i < 5; i++) {
			dayData.clear();
			dayData = Util.loadDateUsage(mContext, Util.getPreviousDateLong(criteria, (4 - i)), mType);
			
			mDayRxUsage[i] = dayData.getRxData();
			mDayTxUsage[i] = dayData.getTxData();
		}
		
		for (int i = 0; i < 5; i++) {
			monthData.clear();
			monthData = Util.loadMonthDataUsage(mContext, Util.getPreviousMonthLong(criteria, (4 - i)), mLimitResetDate);
			
			for (int j = 0; j < monthData.size(); j++) {
				UsageData data = monthData.get(j);
				if (data.getType() == mType) {
					mMonthRxUsage[i] += data.getRxData();
					mMonthTxUsage[i] += data.getTxData();
				}
			}
		}
		
		switch (mType) {
		case USAGE_DIALOG_NETWORK:
			mDayRxUsage[4] += pref.getLong(Constants.RECORD_NETWORK_RX_USAGE, 0) + pref.getLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, 0);
			mDayTxUsage[4] += pref.getLong(Constants.RECORD_NETWORK_TX_USAGE, 0) + pref.getLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, 0);
			break;
		case USAGE_DIALOG_CALL:
			mDayRxUsage[4] += pref.getLong(Constants.RECORD_CALL_RX_USAGE, 0);
			mDayTxUsage[4] += pref.getLong(Constants.RECORD_CALL_TX_USAGE, 0);
			break;
		case USAGE_DIALOG_SMS:
			mDayRxUsage[4] += pref.getLong(Constants.RECORD_SMS_RX_USAGE, 0);
			mDayTxUsage[4] += pref.getLong(Constants.RECORD_SMS_TX_USAGE, 0);
			break;
		}
		
		mMonthRxUsage[4] += mDayRxUsage[4];
		mMonthTxUsage[4] += mDayTxUsage[4];
	}
	
	private static final String PREFIX = "dialog_usage_graph_";
	private static final String POSTFIX[] = {"day_", "month_"};
	private static final String EXTENDER[] = {"value_", "graph_", "remark_"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_usage);
		
		mUsageLayout = (LinearLayout)findViewById(R.id.dialog_usage_main_layout);
		
		mMenuButtonGroup = (RadioGroup)findViewById(R.id.dialog_usage_menu_group);
		mMenuSummaryButton = (RadioButton)findViewById(R.id.dialog_usage_menu_summary);
		mMenuGraphButton = (RadioButton)findViewById(R.id.dialog_usage_menu_graph);
		
		mSummaryLayout = (LinearLayout)findViewById(R.id.dialog_usage_summary_layout);
		mGraphLayout = (LinearLayout)findViewById(R.id.dialog_usage_graph_layout);
		
		mSummaryTitle = (TextView)findViewById(R.id.dialog_usage_summary_title);
		
		mSummaryDayTitle = (TextView)findViewById(R.id.dialog_usage_summary_day_title);
		mSummaryDayRxTitle = (TextView)findViewById(R.id.dialog_usage_summary_day_rx_title);
		mSummaryDayTxTitle = (TextView)findViewById(R.id.dialog_usage_summary_day_tx_title);
		mSummaryDayTotalTitle = (TextView)findViewById(R.id.dialog_usage_summary_day_total_title);
		mSummaryDayRxValue = (TextView)findViewById(R.id.dialog_usage_summary_day_rx_value);
		mSummaryDayTxValue = (TextView)findViewById(R.id.dialog_usage_summary_day_tx_value);
		mSummaryDayTotalValue = (TextView)findViewById(R.id.dialog_usage_summary_day_total_value);
		
		mSummaryMonthTitle = (TextView)findViewById(R.id.dialog_usage_summary_month_title);
		mSummaryMonthRxTitle = (TextView)findViewById(R.id.dialog_usage_summary_month_rx_title);
		mSummaryMonthTxTitle = (TextView)findViewById(R.id.dialog_usage_summary_month_tx_title);
		mSummaryMonthTotalTitle = (TextView)findViewById(R.id.dialog_usage_summary_month_total_title);
		mSummaryMonthRemainTitle = (TextView)findViewById(R.id.dialog_usage_summary_month_remain_title);
		mSummaryMonthRxValue = (TextView)findViewById(R.id.dialog_usage_summary_month_rx_value);
		mSummaryMonthTxValue = (TextView)findViewById(R.id.dialog_usage_summary_month_tx_value);
		mSummaryMonthTotalValue = (TextView)findViewById(R.id.dialog_usage_summary_month_total_value);
		mSummaryMonthRemainValue = (TextView)findViewById(R.id.dialog_usage_summary_month_remain_value);
		
		mGraphTitle = (TextView)findViewById(R.id.dialog_usage_graph_title);
		
		mGraphRemainTitle = (TextView)findViewById(R.id.dialog_usage_graph_remain_title);
		mGraphRemainBar = (ProgressBar)findViewById(R.id.dialog_usage_graph_remain_bar);
		mGraphRemainUsed = (TextView)findViewById(R.id.dialog_usage_graph_remain_used);
		mGraphRemainFree = (TextView)findViewById(R.id.dialog_usage_graph_remain_free);
		
		mGraphDayTitle = (TextView)findViewById(R.id.dialog_usage_graph_day_title);
		mGraphDayToggleGroup = (RadioGroup)findViewById(R.id.dialog_usage_graph_day_toggle_group);
		mGraphDayToggleRxBtn = (RadioButton)findViewById(R.id.dialog_usage_graph_day_toggle_rx);
		mGraphDayToggleTxBtn = (RadioButton)findViewById(R.id.dialog_usage_graph_day_toggle_tx);
		
		mGraphMonthTitle = (TextView)findViewById(R.id.dialog_usage_graph_month_title);
		mGraphMonthToggleGroup = (RadioGroup)findViewById(R.id.dialog_usage_graph_month_toggle_group);
		mGraphMonthToggleRxBtn = (RadioButton)findViewById(R.id.dialog_usage_graph_month_toggle_rx);
		mGraphMonthToggleTxBtn = (RadioButton)findViewById(R.id.dialog_usage_graph_month_toggle_tx);
		
		for (int i = 0; i < 5; i++) {
			mGraphDayValues[i] = (TextView)findViewById(mContext.getResources().getIdentifier(PREFIX + POSTFIX[0] + EXTENDER[0] + i, "id", mContext.getPackageName()));
			mGraphDayGraphs[i] = (ImageView)findViewById(mContext.getResources().getIdentifier(PREFIX + POSTFIX[0] + EXTENDER[1] + i, "id", mContext.getPackageName()));
			mGraphDayRemarks[i] = (TextView)findViewById(mContext.getResources().getIdentifier(PREFIX + POSTFIX[0] + EXTENDER[2] + i,  "id", mContext.getPackageName()));
		
			mGraphMonthValues[i] = (TextView)findViewById(mContext.getResources().getIdentifier(PREFIX + POSTFIX[1] +EXTENDER[0] + i, "id", mContext.getPackageName()));
			mGraphMonthGraphs[i] = (ImageView)findViewById(mContext.getResources().getIdentifier(PREFIX + POSTFIX[1] +EXTENDER[1] + i, "id", mContext.getPackageName()));
			mGraphMonthRemarks[i] = (TextView)findViewById(mContext.getResources().getIdentifier(PREFIX + POSTFIX[1] +EXTENDER[2] + i, "id", mContext.getPackageName()));
		}
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mUsageLayout.setBackgroundResource(R.drawable.dialog_background_pisces);

			mMenuSummaryButton.setTextColor(mContext.getResources().getColor(R.color.pisces_blue));
			mMenuGraphButton.setTextColor(mContext.getResources().getColor(R.color.white));
			
			mMenuSummaryButton.setBackgroundResource(R.drawable.com_menu_usage_pisces);
			mMenuGraphButton.setBackgroundResource(R.drawable.com_menu_usage_pisces);
			
			mGraphRemainBar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.com_progressbar_pisces));
			
			for (int i = 0; i < 5; i++) {
				mGraphDayGraphs[i].setBackgroundResource(R.color.pisces_white_blue);
				mGraphMonthGraphs[i].setBackgroundResource(R.color.pisces_white_blue);
			}
			
			mGraphDayToggleRxBtn.setBackgroundResource(R.drawable.com_menu_usage_pisces);
			mGraphDayToggleTxBtn.setBackgroundResource(R.drawable.com_menu_usage_pisces);
			mGraphMonthToggleRxBtn.setBackgroundResource(R.drawable.com_menu_usage_pisces);
			mGraphMonthToggleTxBtn.setBackgroundResource(R.drawable.com_menu_usage_pisces);
			
			mGraphDayToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.pisces_blue));
			mGraphDayToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.white));
			mGraphMonthToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.pisces_blue));
			mGraphMonthToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.white));
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mUsageLayout.setBackgroundResource(R.drawable.dialog_background_aries);
			
			mMenuSummaryButton.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
			mMenuGraphButton.setTextColor(mContext.getResources().getColor(R.color.white));
			
			mMenuSummaryButton.setBackgroundResource(R.drawable.com_menu_usage_aries);
			mMenuGraphButton.setBackgroundResource(R.drawable.com_menu_usage_aries);
			
			mGraphRemainBar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.com_progressbar_aries));
			
			for (int i = 0; i < 5; i++) {
				mGraphDayGraphs[i].setBackgroundResource(R.color.aries_pink);
				mGraphMonthGraphs[i].setBackgroundResource(R.color.aries_pink);
			}
			
			mGraphDayToggleRxBtn.setBackgroundResource(R.drawable.com_menu_usage_aries);
			mGraphDayToggleTxBtn.setBackgroundResource(R.drawable.com_menu_usage_aries);
			mGraphMonthToggleRxBtn.setBackgroundResource(R.drawable.com_menu_usage_aries);
			mGraphMonthToggleTxBtn.setBackgroundResource(R.drawable.com_menu_usage_aries);
			
			mGraphDayToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
			mGraphDayToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.white));
			mGraphMonthToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
			mGraphMonthToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.white));
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mUsageLayout.setBackgroundResource(R.drawable.dialog_background_taurus);
			
			mMenuSummaryButton.setTextColor(mContext.getResources().getColor(R.color.taurus_white_brown));
			mMenuGraphButton.setTextColor(mContext.getResources().getColor(R.color.white));
			
			mMenuSummaryButton.setBackgroundResource(R.drawable.com_menu_usage_taurus);
			mMenuGraphButton.setBackgroundResource(R.drawable.com_menu_usage_taurus);
			
			mGraphRemainBar.setProgressDrawable(mContext.getResources().getDrawable(R.drawable.com_progressbar_taurus));
			
			for (int i = 0; i < 5; i++) {
				mGraphDayGraphs[i].setBackgroundResource(R.color.taurus_white_brown);
				mGraphMonthGraphs[i].setBackgroundResource(R.color.taurus_white_brown);
			}
			
			mGraphDayToggleRxBtn.setBackgroundResource(R.drawable.com_menu_usage_taurus);
			mGraphDayToggleTxBtn.setBackgroundResource(R.drawable.com_menu_usage_taurus);
			mGraphMonthToggleRxBtn.setBackgroundResource(R.drawable.com_menu_usage_taurus);
			mGraphMonthToggleTxBtn.setBackgroundResource(R.drawable.com_menu_usage_taurus);
			
			mGraphDayToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.taurus_white_brown));
			mGraphDayToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.white));
			mGraphMonthToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.taurus_white_brown));
			mGraphMonthToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.white));
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		RadioGroupEventHandler radioGroupEventHandler = new RadioGroupEventHandler();
		
		mMenuButtonGroup.setOnCheckedChangeListener(radioGroupEventHandler);
		mGraphDayToggleGroup.setOnCheckedChangeListener(radioGroupEventHandler);
		mGraphMonthToggleGroup.setOnCheckedChangeListener(radioGroupEventHandler);
		
		setUI();
	}
	
	private void setUI() {
		long criteria = System.currentTimeMillis();
		
		float usedPercent = 0;
		
		long maxValue = 0;
		float weight = 0;
		
		LinearLayout.LayoutParams valueParams = null;
		LinearLayout.LayoutParams graphParams = null;
		
		switch (mType) {
		case USAGE_DIALOG_NETWORK:
			//SET TITLE VALUE
			mMenuSummaryButton.setText(mContext.getString(R.string.usage_dialog_network_usage_summary));
			mMenuGraphButton.setText(mContext.getString(R.string.usage_dialog_network_usage_graph));
			
			mSummaryTitle.setText(mContext.getString(R.string.usage_dialog_network_usage));
			mSummaryDayTitle.setText(mContext.getString(R.string.usage_dialog_summary_today) + mContext.getString(R.string.usage_dialog_network_usage));
			mSummaryMonthTitle.setText(mContext.getString(R.string.usage_dialog_summary_month) + mContext.getString(R.string.usage_dialog_network_usage));
			
			mSummaryDayTotalTitle.setText(mContext.getString(R.string.usage_dialog_summary_total) + mContext.getString(R.string.usage_dialog_network_usage));
			mSummaryDayRxTitle.setText(mContext.getString(R.string.usage_dialog_network_usage_download) + mContext.getString(R.string.usage_dialog_network_usage));
			mSummaryDayTxTitle.setText(mContext.getString(R.string.usage_dialog_network_usage_upload) + mContext.getString(R.string.usage_dialog_network_usage));
			
			mSummaryMonthTotalTitle.setText(mContext.getString(R.string.usage_dialog_summary_total) + mContext.getString(R.string.usage_dialog_network_usage));
			mSummaryMonthRxTitle.setText(mContext.getString(R.string.usage_dialog_network_usage_download) + mContext.getString(R.string.usage_dialog_network_usage));
			mSummaryMonthTxTitle.setText(mContext.getString(R.string.usage_dialog_network_usage_upload) + mContext.getString(R.string.usage_dialog_network_usage));
			mSummaryMonthRemainTitle.setText(mContext.getString(R.string.usage_dialog_summary_remain) + mContext.getString(R.string.usage_dialog_network_usage));
			
			mGraphTitle.setText(mContext.getString(R.string.usage_dialog_network_usage));
			mGraphRemainTitle.setText(mContext.getString(R.string.usage_dialog_graph_remain) + mContext.getString(R.string.usage_dialog_network_usage));
			mGraphDayTitle.setText(mContext.getString(R.string.usage_dialog_graph_today) + mContext.getString(R.string.usage_dialog_network_usage));
			mGraphMonthTitle.setText(mContext.getString(R.string.usage_dialog_graph_month) + mContext.getString(R.string.usage_dialog_network_usage));
			
			//SET USAGE VALUE
			//SUMMARY VALUES
			mSummaryDayTotalValue.setText(Util.dataFormatSize(mDayRxUsage[4] + mDayTxUsage[4], true));
			mSummaryDayRxValue.setText(Util.dataFormatSize(mDayRxUsage[4], true));
			mSummaryDayTxValue.setText(Util.dataFormatSize(mDayTxUsage[4], true));
			
			mSummaryMonthTotalValue.setText(Util.dataFormatSize(mMonthRxUsage[4] + mMonthTxUsage[4] + mDayRxUsage[4] + mDayTxUsage[4], true));
			mSummaryMonthRxValue.setText(Util.dataFormatSize(mMonthRxUsage[4] + mDayRxUsage[4], true));
			mSummaryMonthTxValue.setText(Util.dataFormatSize(mMonthTxUsage[4] + mDayTxUsage[4], true));
			
			if (mLimitUsage[mType] == 0) {	//UNLIMITED.
				mSummaryMonthRemainValue.setText(Util.dataFormatSize(mMonthRxUsage[4] + mMonthTxUsage[4] + mDayRxUsage[4] + mDayTxUsage[4], true));
			} else {
				mSummaryMonthRemainValue.setText(Util.dataFormatSize(mLimitUsage[mType] - (mDayRxUsage[4] + mDayTxUsage[4]), true));
			}
			
			mGraphDayToggleRxBtn.setText(mContext.getString(R.string.usage_dialog_upload));
			mGraphDayToggleTxBtn.setText(mContext.getString(R.string.usage_dialog_download));
			mGraphMonthToggleRxBtn.setText(mContext.getString(R.string.usage_dialog_upload));
			mGraphMonthToggleTxBtn.setText(mContext.getString(R.string.usage_dialog_download));
			
			//GRAPH VALEUS
			if (mLimitUsage[mType] == 0) {
				usedPercent = 50;
				mGraphRemainBar.setProgress((int)usedPercent);
				mGraphRemainUsed.setText(Util.dataFormatSize((mMonthRxUsage[4] + mMonthTxUsage[4]), false) + mContext.getString(R.string.usage_dialog_used));
				mGraphRemainFree.setText(mContext.getString(R.string.usage_dialog_unlimited));
			} else {
				usedPercent = getPercent(mLimitUsage[mType], (mMonthRxUsage[4] + mMonthTxUsage[4]));
				mGraphRemainBar.setProgress((int)usedPercent);
				mGraphRemainUsed.setText(Util.dataFormatSize((mMonthRxUsage[4] + mMonthTxUsage[4]), false) + mContext.getString(R.string.usage_dialog_used));
				mGraphRemainFree.setText(Util.dataFormatSize(mLimitUsage[mType] - (mMonthRxUsage[4] + mMonthTxUsage[4]), false) + mContext.getString(R.string.usage_dialog_remain));
			}
			
			maxValue = getMaxValue(MAX_OF_DAY_RX);
			weight = 0;
			
			for (int i = 0; i < 5; i++) {
				weight = getPercent(maxValue, mDayRxUsage[i]) * 0.8f;
				
				valueParams = (LinearLayout.LayoutParams)mGraphDayValues[i].getLayoutParams();
				graphParams = (LinearLayout.LayoutParams)mGraphDayGraphs[i].getLayoutParams();
				
				valueParams.weight = 100 - weight;
				graphParams.weight = weight;
				
				mGraphDayValues[i].setText(Util.dataFormatSize(mDayRxUsage[i], false));
				mGraphDayRemarks[i].setText(Util.getPreviousDateStr(criteria, (4 - i)));
				
				mGraphDayValues[i].setLayoutParams(valueParams);
				mGraphDayGraphs[i].setLayoutParams(graphParams);
			}
			
			maxValue = getMaxValue(MAX_OF_MONTH_RX);
			weight = 0;
			
			for (int i = 0; i < 5; i++) {
				weight = getPercent(maxValue, mMonthRxUsage[i]) * 0.8f;
				
				valueParams = (LinearLayout.LayoutParams)mGraphMonthValues[i].getLayoutParams();
				graphParams = (LinearLayout.LayoutParams)mGraphMonthGraphs[i].getLayoutParams();
				
				valueParams.weight = 100 - weight;
				graphParams.weight = weight;
				
				mGraphMonthValues[i].setText(Util.dataFormatSize(mMonthRxUsage[i], false));
				mGraphMonthRemarks[i].setText(Util.getPreviousMonthStr(criteria, (4 - i)));
				
				mGraphMonthValues[i].setLayoutParams(valueParams);
				mGraphMonthGraphs[i].setLayoutParams(graphParams);
			}
			break;
		case USAGE_DIALOG_CALL:
			//SET TITLE VALUE
			mMenuSummaryButton.setText(mContext.getString(R.string.usage_dialog_call_usage_summary));
			mMenuGraphButton.setText(mContext.getString(R.string.usage_dialog_call_usage_graph));
			
			mSummaryTitle.setText(mContext.getString(R.string.usage_dialog_call_usage));
			mSummaryDayTitle.setText(mContext.getString(R.string.usage_dialog_summary_today) + mContext.getString(R.string.usage_dialog_call_usage));
			mSummaryMonthTitle.setText(mContext.getString(R.string.usage_dialog_summary_month) + mContext.getString(R.string.usage_dialog_call_usage));
			
			mSummaryDayTotalTitle.setText(mContext.getString(R.string.usage_dialog_summary_total) + mContext.getString(R.string.usage_dialog_call_usage));
			mSummaryDayRxTitle.setText(mContext.getString(R.string.usage_dialog_call_usage_incoming) + mContext.getString(R.string.usage_dialog_call_usage));
			mSummaryDayTxTitle.setText(mContext.getString(R.string.usage_dialog_call_usage_outgoing) + mContext.getString(R.string.usage_dialog_call_usage));
			
			mSummaryMonthTotalTitle.setText(mContext.getString(R.string.usage_dialog_summary_total) + mContext.getString(R.string.usage_dialog_call_usage));
			mSummaryMonthRxTitle.setText(mContext.getString(R.string.usage_dialog_call_usage_incoming) + mContext.getString(R.string.usage_dialog_call_usage));
			mSummaryMonthTxTitle.setText(mContext.getString(R.string.usage_dialog_call_usage_outgoing) + mContext.getString(R.string.usage_dialog_call_usage));
			mSummaryMonthRemainTitle.setText(mContext.getString(R.string.usage_dialog_summary_remain) + mContext.getString(R.string.usage_dialog_call_usage));
			
			mGraphTitle.setText(mContext.getString(R.string.usage_dialog_call_usage));
			mGraphRemainTitle.setText(mContext.getString(R.string.usage_dialog_graph_remain) + mContext.getString(R.string.usage_dialog_call_usage));
			mGraphDayTitle.setText(mContext.getString(R.string.usage_dialog_graph_today) + mContext.getString(R.string.usage_dialog_call_usage));
			mGraphMonthTitle.setText(mContext.getString(R.string.usage_dialog_graph_month) + mContext.getString(R.string.usage_dialog_call_usage));
			
			//SET USAGE VALUE
			//SUMMARY VALUES
			mSummaryDayTotalValue.setText(Util.callFormatSize(mDayRxUsage[4] + mDayTxUsage[4]));
			mSummaryDayRxValue.setText(Util.callFormatSize(mDayRxUsage[4]));
			mSummaryDayTxValue.setText(Util.callFormatSize(mDayTxUsage[4]));
			
			mSummaryMonthTotalValue.setText(Util.callFormatSize(mMonthRxUsage[4] + mMonthTxUsage[4]));
			mSummaryMonthRxValue.setText(Util.callFormatSize(mMonthRxUsage[4]));
			mSummaryMonthTxValue.setText(Util.callFormatSize(mMonthTxUsage[4]));
			
			if (mLimitUsage[mType] == 0) {	//UNLIMITED.
				mSummaryMonthRemainValue.setText(R.string.usage_dialog_unlimited);
			} else {
				mSummaryMonthRemainValue.setText(Util.callFormatSize(mLimitUsage[mType] - mDayTxUsage[4]));
			}
			
			//GRAPH VALEUS
			if (mLimitUsage[mType] == 0) {
				usedPercent = 50;
				mGraphRemainBar.setProgress((int)usedPercent);
				mGraphRemainUsed.setText(Util.callFormatSize(mMonthTxUsage[4]) + mContext.getString(R.string.usage_dialog_used));
				mGraphRemainFree.setText(mContext.getString(R.string.usage_dialog_unlimited));
			} else {
				usedPercent = getPercent(mLimitUsage[mType], (mMonthTxUsage[4]));
				mGraphRemainBar.setProgress((int)usedPercent);
				mGraphRemainUsed.setText(Util.callFormatSize(mMonthTxUsage[4]) + mContext.getString(R.string.usage_dialog_used));
				mGraphRemainFree.setText(Util.callFormatSize(mLimitUsage[mType] - mMonthTxUsage[4]) + mContext.getString(R.string.usage_dialog_remain));
			}
			
			maxValue = getMaxValue(MAX_OF_DAY_RX);
			weight = 0;
			
			for (int i = 0; i < 5; i++) {
				weight = getPercent(maxValue, mDayRxUsage[i]) * 0.8f;
				
				valueParams = (LinearLayout.LayoutParams)mGraphDayValues[i].getLayoutParams();
				graphParams = (LinearLayout.LayoutParams)mGraphDayGraphs[i].getLayoutParams();
				
				valueParams.weight = 100 - weight;
				graphParams.weight = weight;
				
				mGraphDayValues[i].setText(Util.callFormatSize(mDayRxUsage[i]));
				mGraphDayRemarks[i].setText(Util.getPreviousDateStr(criteria, (4 - i)));
				
				mGraphDayValues[i].setLayoutParams(valueParams);
				mGraphDayGraphs[i].setLayoutParams(graphParams);
			}
			
			maxValue = getMaxValue(MAX_OF_MONTH_RX);
			weight = 0;
			
			for (int i = 0; i < 5; i++) {
				weight = getPercent(maxValue, mMonthRxUsage[i]) * 0.8f;
				
				valueParams = (LinearLayout.LayoutParams)mGraphMonthValues[i].getLayoutParams();
				graphParams = (LinearLayout.LayoutParams)mGraphMonthGraphs[i].getLayoutParams();
				
				valueParams.weight = 100 - weight;
				graphParams.weight = weight;
				
				mGraphMonthValues[i].setText(Util.callFormatSize(mMonthRxUsage[i]));
				mGraphMonthRemarks[i].setText(Util.getPreviousMonthStr(criteria, (4 - i)));
				
				mGraphMonthValues[i].setLayoutParams(valueParams);
				mGraphMonthGraphs[i].setLayoutParams(graphParams);
			}
			break;
		case USAGE_DIALOG_SMS:
			//SET TITLE VALUE
			mMenuSummaryButton.setText(mContext.getString(R.string.usage_dialog_sms_usage_summary));
			mMenuGraphButton.setText(mContext.getString(R.string.usage_dialog_sms_usage_graph));
			
			mSummaryTitle.setText(mContext.getString(R.string.usage_dialog_sms_usage));
			mSummaryDayTitle.setText(mContext.getString(R.string.usage_dialog_summary_today) + mContext.getString(R.string.usage_dialog_sms_usage));
			mSummaryMonthTitle.setText(mContext.getString(R.string.usage_dialog_summary_month) + mContext.getString(R.string.usage_dialog_sms_usage));
			
			mSummaryDayTotalTitle.setText(mContext.getString(R.string.usage_dialog_summary_total) + mContext.getString(R.string.usage_dialog_sms_usage));
			mSummaryDayRxTitle.setText(mContext.getString(R.string.usage_dialog_sms_usage_receive) + mContext.getString(R.string.usage_dialog_sms_usage));
			mSummaryDayTxTitle.setText(mContext.getString(R.string.usage_dialog_sms_usage_send) + mContext.getString(R.string.usage_dialog_sms_usage));
			
			mSummaryMonthTotalTitle.setText(mContext.getString(R.string.usage_dialog_summary_total) + mContext.getString(R.string.usage_dialog_sms_usage));
			mSummaryMonthRxTitle.setText(mContext.getString(R.string.usage_dialog_sms_usage_receive) + mContext.getString(R.string.usage_dialog_sms_usage));
			mSummaryMonthTxTitle.setText(mContext.getString(R.string.usage_dialog_sms_usage_send) + mContext.getString(R.string.usage_dialog_sms_usage));
			mSummaryMonthRemainTitle.setText(mContext.getString(R.string.usage_dialog_summary_remain) + mContext.getString(R.string.usage_dialog_sms_usage));
			
			mGraphTitle.setText(mContext.getString(R.string.usage_dialog_sms_usage));
			mGraphRemainTitle.setText(mContext.getString(R.string.usage_dialog_graph_remain) + mContext.getString(R.string.usage_dialog_sms_usage));
			mGraphDayTitle.setText(mContext.getString(R.string.usage_dialog_graph_today) + mContext.getString(R.string.usage_dialog_sms_usage));
			mGraphMonthTitle.setText(mContext.getString(R.string.usage_dialog_graph_month) + mContext.getString(R.string.usage_dialog_sms_usage));
			
			//SET USAGE VALUE
			//SUMMARY VALUES
			mSummaryDayTotalValue.setText(Util.smsFormatSize(mDayRxUsage[4] + mDayTxUsage[4]));
			mSummaryDayRxValue.setText(Util.smsFormatSize(mDayRxUsage[4]));
			mSummaryDayTxValue.setText(Util.smsFormatSize(mDayTxUsage[4]));
			
			mSummaryMonthTotalValue.setText(Util.smsFormatSize(mMonthRxUsage[4] + mMonthTxUsage[4]));
			mSummaryMonthRxValue.setText(Util.smsFormatSize(mMonthRxUsage[4]));
			mSummaryMonthTxValue.setText(Util.smsFormatSize(mMonthTxUsage[4]));
			
			if (mLimitUsage[mType] == 0) {	//UNLIMITED.
				mSummaryMonthRemainValue.setText(R.string.usage_dialog_unlimited);
			} else {
				mSummaryMonthRemainValue.setText(Util.smsFormatSize(mLimitUsage[mType] - mDayTxUsage[4]));
			}
			
			//GRAPH VALEUS
			if (mLimitUsage[mType] == 0) {
				usedPercent = 50;
				mGraphRemainBar.setProgress((int)usedPercent);
				mGraphRemainUsed.setText(Util.smsFormatSize(mMonthTxUsage[4]) + mContext.getString(R.string.usage_dialog_used));
				mGraphRemainFree.setText(mContext.getString(R.string.usage_dialog_unlimited));
			} else {
				usedPercent = getPercent(mLimitUsage[mType], (mMonthTxUsage[4]));
				mGraphRemainBar.setProgress((int)usedPercent);
				mGraphRemainUsed.setText(Util.smsFormatSize(mMonthTxUsage[4]) + mContext.getString(R.string.usage_dialog_used));
				mGraphRemainFree.setText(Util.smsFormatSize(mLimitUsage[mType] - mMonthTxUsage[4]) + mContext.getString(R.string.usage_dialog_remain));
			}
			
			maxValue = getMaxValue(MAX_OF_DAY_RX);
			weight = 0;
			
			for (int i = 0; i < 5; i++) {
				weight = getPercent(maxValue, mDayRxUsage[i]) * 0.8f;
				
				valueParams = (LinearLayout.LayoutParams)mGraphDayValues[i].getLayoutParams();
				graphParams = (LinearLayout.LayoutParams)mGraphDayGraphs[i].getLayoutParams();
				
				valueParams.weight = 100 - weight;
				graphParams.weight = weight;
				
				mGraphDayValues[i].setText(Util.smsFormatSize(mDayRxUsage[i]));
				mGraphDayRemarks[i].setText(Util.getPreviousDateStr(criteria, (4 - i)));
				
				mGraphDayValues[i].setLayoutParams(valueParams);
				mGraphDayGraphs[i].setLayoutParams(graphParams);
			}
			
			maxValue = getMaxValue(MAX_OF_MONTH_RX);
			weight = 0;
			
			for (int i = 0; i < 5; i++) {
				weight = getPercent(maxValue, mMonthRxUsage[i]) * 0.8f;
				
				valueParams = (LinearLayout.LayoutParams)mGraphMonthValues[i].getLayoutParams();
				graphParams = (LinearLayout.LayoutParams)mGraphMonthGraphs[i].getLayoutParams();
				
				valueParams.weight = 100 - weight;
				graphParams.weight = weight;
				
				mGraphMonthValues[i].setText(Util.smsFormatSize(mMonthRxUsage[i]));
				mGraphMonthRemarks[i].setText(Util.getPreviousMonthStr(criteria, (4 - i)));
				
				mGraphMonthValues[i].setLayoutParams(valueParams);
				mGraphMonthGraphs[i].setLayoutParams(graphParams);
			}
			break;
		}
	}
	
	private static final int MAX_OF_DAY_RX		= 0;
	private static final int MAX_OF_DAY_TX		= 1;
	private static final int MAX_OF_MONTH_RX	= 2;
	private static final int MAX_OF_MONTH_TX	= 3;
	
	private long getMaxValue(int dayOrMonth) {
		long max = 0;
		
		switch (dayOrMonth) {
		case MAX_OF_DAY_RX:
			max = mDayRxUsage[0];
			for (int i = 1; i < 5; i++) {
				if (mDayRxUsage[i] > max) {
					max = mDayRxUsage[i];
				}
			}
			break;
		case MAX_OF_DAY_TX:
			max = mDayTxUsage[0];
			for (int i = 1; i < 5; i++) {
				if (mDayTxUsage[i] > max) {
					max = mDayTxUsage[i];
				}
			}
			break;
		case MAX_OF_MONTH_RX:
			max = mMonthRxUsage[0];
			for (int i = 1; i < 5; i++) {
				if (mMonthRxUsage[i] > max) {
					max = mMonthRxUsage[i];
				}
			}
			break;
		case MAX_OF_MONTH_TX:
			max = mMonthTxUsage[0];
			for (int i = 1; i < 5; i++) {
				if (mMonthTxUsage[i] > max) {
					max = mMonthTxUsage[i];
				}
			}
			break;
		}
		
		return max;
	}
	
	private float getPercent(long criteria, long value) {
		if (criteria == 0) {
			return 0;
		} else {
			return value * 100 / criteria;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		dismiss();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onStop();
	}
	
	private class RadioGroupEventHandler implements RadioGroup.OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			if (group.getId() == R.id.dialog_usage_menu_group) {
				switch (checkedId) {
				case R.id.dialog_usage_menu_summary:
					switch (mApplicationTheme) {
					case Constants.APPLICATION_THEME_PISCES:
						mMenuSummaryButton.setTextColor(mContext.getResources().getColor(R.color.pisces_blue));
						mMenuGraphButton.setTextColor(mContext.getResources().getColor(R.color.white));
						break;
					case Constants.APPLICATION_THEME_ARIES:
						mMenuSummaryButton.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
						mMenuGraphButton.setTextColor(mContext.getResources().getColor(R.color.white));
						break;
					case Constants.APPLICATION_THEME_TAURUS:
						mMenuSummaryButton.setTextColor(mContext.getResources().getColor(R.color.taurus_white_brown));
						mMenuGraphButton.setTextColor(mContext.getResources().getColor(R.color.white));
						break;
					}
					
					mSummaryLayout.setVisibility(View.VISIBLE);
					mGraphLayout.setVisibility(View.GONE);
					break;
				case R.id.dialog_usage_menu_graph:
					switch (mApplicationTheme) {
					case Constants.APPLICATION_THEME_PISCES:
						mMenuSummaryButton.setTextColor(mContext.getResources().getColor(R.color.white));
						mMenuGraphButton.setTextColor(mContext.getResources().getColor(R.color.pisces_blue));
						break;
					case Constants.APPLICATION_THEME_ARIES:
						mMenuSummaryButton.setTextColor(mContext.getResources().getColor(R.color.white));
						mMenuGraphButton.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
						break;
					case Constants.APPLICATION_THEME_TAURUS:
						mMenuSummaryButton.setTextColor(mContext.getResources().getColor(R.color.white));
						mMenuGraphButton.setTextColor(mContext.getResources().getColor(R.color.taurus_white_brown));
						break;
					}
					
					mSummaryLayout.setVisibility(View.GONE);
					mGraphLayout.setVisibility(View.VISIBLE);
					break;
				}
			} else if (group.getId() == R.id.dialog_usage_graph_day_toggle_group ||
					   group.getId() == R.id.dialog_usage_graph_month_toggle_group) {
				long criteria = System.currentTimeMillis();
				
				long maxValue = 0;
				float weight = 0;
				
				LinearLayout.LayoutParams valueParams = null;
				LinearLayout.LayoutParams graphParams = null;
				
				switch (checkedId) {
				case R.id.dialog_usage_graph_day_toggle_rx:
					mGraphDayToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.white));
					switch (mApplicationTheme) {
					case Constants.APPLICATION_THEME_PISCES:
						mGraphDayToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.pisces_white_blue));
						break;
					case Constants.APPLICATION_THEME_ARIES:
						mGraphDayToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
						break;
					case Constants.APPLICATION_THEME_TAURUS:
						mGraphDayToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.taurus_white_brown));
						break;
					}
					
					maxValue = getMaxValue(MAX_OF_DAY_RX);
					weight = 0;
					
					for (int i = 0; i < 5; i++) {
						weight = getPercent(maxValue, mDayRxUsage[i]) * 0.8f;
						
						valueParams = (LinearLayout.LayoutParams)mGraphDayValues[i].getLayoutParams();
						graphParams = (LinearLayout.LayoutParams)mGraphDayGraphs[i].getLayoutParams();
						
						valueParams.weight = 100 - weight;
						graphParams.weight = weight;
						
						switch (mType) {
						case USAGE_DIALOG_NETWORK:
							mGraphDayValues[i].setText(Util.dataFormatSize(mDayRxUsage[i], false));
							break;
						case USAGE_DIALOG_CALL:
							mGraphDayValues[i].setText(Util.callFormatSize(mDayRxUsage[i]));
							break;
						case USAGE_DIALOG_SMS:
							mGraphDayValues[i].setText(Util.smsFormatSize(mDayRxUsage[i]));
							break;
						}
						
						mGraphDayRemarks[i].setText(Util.getPreviousDateStr(criteria, (4 - i)));
						
						mGraphDayValues[i].setLayoutParams(valueParams);
						mGraphDayGraphs[i].setLayoutParams(graphParams);
					}
					break;
				case R.id.dialog_usage_graph_day_toggle_tx:
					mGraphDayToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.white));
					switch (mApplicationTheme) {
					case Constants.APPLICATION_THEME_PISCES:
						mGraphDayToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.pisces_white_blue));
						break;
					case Constants.APPLICATION_THEME_ARIES:
						mGraphDayToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
						break;
					case Constants.APPLICATION_THEME_TAURUS:
						mGraphDayToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.taurus_white_brown));
						break;
					}
					
					maxValue = getMaxValue(MAX_OF_DAY_TX);
					weight = 0;
					
					for (int i = 0; i < 5; i++) {
						weight = getPercent(maxValue, mDayTxUsage[i]) * 0.8f;
						
						valueParams = (LinearLayout.LayoutParams)mGraphDayValues[i].getLayoutParams();
						graphParams = (LinearLayout.LayoutParams)mGraphDayGraphs[i].getLayoutParams();
						
						valueParams.weight = 100 - weight;
						graphParams.weight = weight;
						
						switch (mType) {
						case USAGE_DIALOG_NETWORK:
							mGraphDayValues[i].setText(Util.dataFormatSize(mDayTxUsage[i], false));
							break;
						case USAGE_DIALOG_CALL:
							mGraphDayValues[i].setText(Util.callFormatSize(mDayTxUsage[i]));
							break;
						case USAGE_DIALOG_SMS:
							mGraphDayValues[i].setText(Util.smsFormatSize(mDayTxUsage[i]));
							break;
						}
						
						mGraphDayRemarks[i].setText(Util.getPreviousDateStr(criteria, (4 - i)));
						
						mGraphDayValues[i].setLayoutParams(valueParams);
						mGraphDayGraphs[i].setLayoutParams(graphParams);
					}
					break;
				case R.id.dialog_usage_graph_month_toggle_rx:
					mGraphMonthToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.white));
					switch (mApplicationTheme) {
					case Constants.APPLICATION_THEME_PISCES:
						mGraphMonthToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.pisces_white_blue));
						break;
					case Constants.APPLICATION_THEME_ARIES:
						mGraphMonthToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
						break;
					case Constants.APPLICATION_THEME_TAURUS:
						mGraphMonthToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.taurus_white_brown));
						break;
					}
					
					maxValue = getMaxValue(MAX_OF_MONTH_RX);
					weight = 0;
					
					for (int i = 0; i < 5; i++) {
						weight = getPercent(maxValue, mMonthRxUsage[i]) * 0.8f;
						
						valueParams = (LinearLayout.LayoutParams)mGraphMonthValues[i].getLayoutParams();
						graphParams = (LinearLayout.LayoutParams)mGraphMonthGraphs[i].getLayoutParams();
						
						valueParams.weight = 100 - weight;
						graphParams.weight = weight;
						
						switch (mType) {
						case USAGE_DIALOG_NETWORK:
							mGraphMonthValues[i].setText(Util.dataFormatSize(mMonthRxUsage[i], false));
							break;
						case USAGE_DIALOG_CALL:
							mGraphMonthValues[i].setText(Util.callFormatSize(mMonthRxUsage[i]));
							break;
						case USAGE_DIALOG_SMS:
							mGraphMonthValues[i].setText(Util.smsFormatSize(mMonthRxUsage[i]));
							break;
						}
						
						mGraphMonthRemarks[i].setText(Util.getPreviousMonthStr(criteria, (4 - i)));
						
						mGraphMonthValues[i].setLayoutParams(valueParams);
						mGraphMonthGraphs[i].setLayoutParams(graphParams);
					}
					break;
				case R.id.dialog_usage_graph_month_toggle_tx:
					mGraphMonthToggleRxBtn.setTextColor(mContext.getResources().getColor(R.color.white));
					switch (mApplicationTheme) {
					case Constants.APPLICATION_THEME_PISCES:
						mGraphMonthToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.pisces_white_blue));
						break;
					case Constants.APPLICATION_THEME_ARIES:
						mGraphMonthToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
						break;
					case Constants.APPLICATION_THEME_TAURUS:
						mGraphMonthToggleTxBtn.setTextColor(mContext.getResources().getColor(R.color.taurus_white_brown));
						break;
					}
					
					maxValue = getMaxValue(MAX_OF_MONTH_TX);
					weight = 0;
					
					for (int i = 0; i < 5; i++) {
						weight = getPercent(maxValue, mMonthTxUsage[i]) * 0.8f;
						
						valueParams = (LinearLayout.LayoutParams)mGraphMonthValues[i].getLayoutParams();
						graphParams = (LinearLayout.LayoutParams)mGraphMonthGraphs[i].getLayoutParams();
						
						valueParams.weight = 100 - weight;
						graphParams.weight = weight;
						
						switch (mType) {
						case USAGE_DIALOG_NETWORK:
							mGraphMonthValues[i].setText(Util.dataFormatSize(mMonthTxUsage[i], false));
							break;
						case USAGE_DIALOG_CALL:
							mGraphMonthValues[i].setText(Util.callFormatSize(mMonthTxUsage[i]));
							break;
						case USAGE_DIALOG_SMS:
							mGraphMonthValues[i].setText(Util.smsFormatSize(mMonthTxUsage[i]));
							break;
						}
						
						mGraphMonthRemarks[i].setText(Util.getPreviousMonthStr(criteria, (4 - i)));
						
						mGraphMonthValues[i].setLayoutParams(valueParams);
						mGraphMonthGraphs[i].setLayoutParams(graphParams);
					}
					break;
				}
			}
		}
	}
}
