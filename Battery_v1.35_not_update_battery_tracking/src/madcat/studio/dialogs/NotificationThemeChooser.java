package madcat.studio.dialogs;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.service.IntegrityService;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NotificationThemeChooser extends Dialog {
	private static final int PADDING = 15;
	
	private Context mContext;
	private int mBackgroundTheme = Constants.APPLICATION_THEME_BASIC;
	private int mNotificationTheme = Constants.NOTIFICATION_THEME_BASIC_RECTANGLE;
	
	private LinearLayout mThemeLayout;
	private ImageView mThemeDisplayView;
	private TextView mThemeNameView;
	private Button mPrevBtn, mNextBtn;
	private Button mOkBtn, mCancelBtn;
	
	private String[] mThemeNameArray;
	private int mThemeIndex = 0;
	
	public NotificationThemeChooser(Context context, int styleId, int applicationTheme) {
		// TODO Auto-generated constructor stub
		super(context, styleId);
		
		mContext = context;
		mBackgroundTheme = applicationTheme;
		mThemeNameArray = context.getResources().getStringArray(R.array.notification_theme_array);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_notification_theme_chooser);
		
		mThemeLayout = (LinearLayout)findViewById(R.id.dialog_notification_theme_layout);
		mThemeDisplayView = (ImageView)findViewById(R.id.dialog_notification_theme_display_view);
		mThemeNameView = (TextView)findViewById(R.id.dialog_notification_theme_name_view);
		mPrevBtn = (Button)findViewById(R.id.dialog_notification_theme_btn_prev);
		mNextBtn = (Button)findViewById(R.id.dialog_notification_theme_btn_next);
		mOkBtn = (Button)findViewById(R.id.dialog_notification_theme_ok);
		mCancelBtn = (Button)findViewById(R.id.dialog_notification_theme_cancel);
		
		switch (mBackgroundTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mThemeLayout.setBackgroundResource(R.drawable.dialog_background_pisces);
			mPrevBtn.setBackgroundResource(R.drawable.com_preference_prev_btn_pisces);
			mNextBtn.setBackgroundResource(R.drawable.com_preference_next_btn_pisces);
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mThemeLayout.setBackgroundResource(R.drawable.dialog_background_aries);
			mPrevBtn.setBackgroundResource(R.drawable.com_preference_prev_btn_aries);
			mNextBtn.setBackgroundResource(R.drawable.com_preference_next_btn_aries);
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mThemeLayout.setBackgroundResource(R.drawable.dialog_background_taurus);
			mPrevBtn.setBackgroundResource(R.drawable.com_preference_prev_btn_taurus);
			mNextBtn.setBackgroundResource(R.drawable.com_preference_next_btn_taurus);
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		switch (mNotificationTheme) {
		case Constants.NOTIFICATION_THEME_BASIC_RECTANGLE:
			break;
//		case Constants.NOTIFICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.NOTIFICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.NOTIFICATION_THEME_PISCES:
			mThemeDisplayView.setImageResource(R.drawable.notification_pisces_100);
			break;
		case Constants.NOTIFICATION_THEME_ARIES:
			mThemeDisplayView.setImageResource(R.drawable.notification_aries_100);
			break;
		case Constants.NOTIFICATION_THEME_TAURUS:
			mThemeDisplayView.setImageResource(R.drawable.notification_taurus_100);
			break;
//		case Constants.NOTIFICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.NOTIFICATION_THEME_CANCER:
//			
//			break;
//		case Constants.NOTIFICATION_THEME_LEO:
//			
//			break;
//		case Constants.NOTIFICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.NOTIFICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.NOTIFICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.NOTIFICATION_THEME_ARCHER:
//			
//			break;
		case Constants.NOTIFICATION_THEME_BASIC_CIRCLE:
			mThemeDisplayView.setImageResource(R.drawable.notification_circle_100);
			break;
		}
		
		mThemeLayout.setPadding(PADDING, PADDING, PADDING, PADDING);
		mThemeNameView.setText(mThemeNameArray[mNotificationTheme]);
		
		EvnetHandler eventHandler = new EvnetHandler();
		
		mPrevBtn.setOnClickListener(eventHandler);
		mNextBtn.setOnClickListener(eventHandler);
		mOkBtn.setOnClickListener(eventHandler);
		mCancelBtn.setOnClickListener(eventHandler);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		dismiss();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onStop();
	}
	
	public void setNotificaitonTheme(int theme) {
		mNotificationTheme = theme;
		mThemeIndex = theme;
	}
	
	public int getNotificationTheme() {
		return mNotificationTheme;
	}
	
	private class EvnetHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.dialog_notification_theme_btn_prev:
				if (mThemeIndex == 0) {
					mThemeIndex = mThemeNameArray.length - 1;
				} else {
					mThemeIndex--;
				}
				
				switch (mThemeIndex) {
				case Constants.NOTIFICATION_THEME_BASIC_CIRCLE:
					mThemeDisplayView.setImageResource(R.drawable.notification_circle_100);
					break;
				case Constants.NOTIFICATION_THEME_CAPRICORN:
					mThemeDisplayView.setImageResource(R.drawable.notification_capricorn_100);
					break;
				case Constants.NOTIFICATION_THEME_AQUARIUS:
					mThemeDisplayView.setImageResource(R.drawable.notification_aquarius_100);
					break;
				case Constants.NOTIFICATION_THEME_PISCES:
					mThemeDisplayView.setImageResource(R.drawable.notification_pisces_100);
					break;
				case Constants.NOTIFICATION_THEME_ARIES:
					mThemeDisplayView.setImageResource(R.drawable.notification_aries_100);
					break;
				case Constants.NOTIFICATION_THEME_TAURUS:
					mThemeDisplayView.setImageResource(R.drawable.notification_taurus_100);
					break;
				case Constants.NOTIFICATION_THEME_GEMINI:
					mThemeDisplayView.setImageResource(R.drawable.notification_gemini_100);
					break;
				case Constants.NOTIFICATION_THEME_CANCER:
					mThemeDisplayView.setImageResource(R.drawable.notification_cancer_100);
					break;
				case Constants.NOTIFICATION_THEME_LEO:
					mThemeDisplayView.setImageResource(R.drawable.notification_leo_100);
					break;
				case Constants.NOTIFICATION_THEME_VIRGO:
					mThemeDisplayView.setImageResource(R.drawable.notification_virgo_100);
					break;
				case Constants.NOTIFICATION_THEME_LIBRA:
					mThemeDisplayView.setImageResource(R.drawable.notification_libra_100);
					break;
				case Constants.NOTIFICATION_THEME_SCORPIO:
					mThemeDisplayView.setImageResource(R.drawable.notification_scorpio_100);
					break;
				case Constants.NOTIFICATION_THEME_ARCHER:
					mThemeDisplayView.setImageResource(R.drawable.notification_archer_100);
					break;
				case Constants.NOTIFICATION_THEME_BASIC_RECTANGLE:
				default:
					mThemeDisplayView.setImageResource(R.drawable.notification_rectangle_100);
					break;
				}
				
				mThemeNameView.setText(mThemeNameArray[mThemeIndex]);
				break;
			case R.id.dialog_notification_theme_btn_next:
				if (mThemeIndex == mThemeNameArray.length - 1) {
					mThemeIndex = 0;
				} else {
					mThemeIndex++;
				}
				
				switch (mThemeIndex) {
				case Constants.NOTIFICATION_THEME_BASIC_CIRCLE:
					mThemeDisplayView.setImageResource(R.drawable.notification_circle_100);
					break;
				case Constants.NOTIFICATION_THEME_CAPRICORN:
					mThemeDisplayView.setImageResource(R.drawable.notification_capricorn_100);
					break;
				case Constants.NOTIFICATION_THEME_AQUARIUS:
					mThemeDisplayView.setImageResource(R.drawable.notification_aquarius_100);
					break;
				case Constants.NOTIFICATION_THEME_PISCES:
					mThemeDisplayView.setImageResource(R.drawable.notification_pisces_100);
					break;
				case Constants.NOTIFICATION_THEME_ARIES:
					mThemeDisplayView.setImageResource(R.drawable.notification_aries_100);
					break;
				case Constants.NOTIFICATION_THEME_TAURUS:
					mThemeDisplayView.setImageResource(R.drawable.notification_taurus_100);
					break;
				case Constants.NOTIFICATION_THEME_GEMINI:
					mThemeDisplayView.setImageResource(R.drawable.notification_gemini_100);
					break;
				case Constants.NOTIFICATION_THEME_CANCER:
					mThemeDisplayView.setImageResource(R.drawable.notification_cancer_100);
					break;
				case Constants.NOTIFICATION_THEME_LEO:
					mThemeDisplayView.setImageResource(R.drawable.notification_leo_100);
					break;
				case Constants.NOTIFICATION_THEME_VIRGO:
					mThemeDisplayView.setImageResource(R.drawable.notification_virgo_100);
					break;
				case Constants.NOTIFICATION_THEME_LIBRA:
					mThemeDisplayView.setImageResource(R.drawable.notification_libra_100);
					break;
				case Constants.NOTIFICATION_THEME_SCORPIO:
					mThemeDisplayView.setImageResource(R.drawable.notification_scorpio_100);
					break;
				case Constants.NOTIFICATION_THEME_ARCHER:
					mThemeDisplayView.setImageResource(R.drawable.notification_archer_100);
					break;
				case Constants.NOTIFICATION_THEME_BASIC_RECTANGLE:
				default:
					mThemeDisplayView.setImageResource(R.drawable.notification_rectangle_100);
					break;
				}
				
				mThemeNameView.setText(mThemeNameArray[mThemeIndex]);
				break;
			case R.id.dialog_notification_theme_ok:
				if (mNotificationTheme != mThemeIndex) {
					if (mThemeIndex == Constants.NOTIFICATION_THEME_PISCES ||
						mThemeIndex == Constants.NOTIFICATION_THEME_ARIES ||
						mThemeIndex == Constants.NOTIFICATION_THEME_TAURUS ||
						mThemeIndex == Constants.NOTIFICATION_THEME_BASIC_CIRCLE ||
						mThemeIndex == Constants.NOTIFICATION_THEME_BASIC_RECTANGLE) {
						mNotificationTheme = mThemeIndex;
							
						SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE).edit();
						editor.putInt(Constants.NOTIFICATION_THEME, mThemeIndex);
						editor.commit();
						
						Intent intent = new Intent(IntegrityService.SERVICE_UPDATE_ACTION);
						mContext.stopService(intent);
						mContext.startService(intent);
						dismiss();
					} else {
						Toast.makeText(mContext, mContext.getString(R.string.preference_theme_not_prepare_msg), Toast.LENGTH_SHORT).show();
						new ToastForbiddenAsyncTask().execute();
					}
				} else {
					dismiss();
				}
				break;
			case R.id.dialog_notification_theme_cancel:
				dismiss();
				break;
			}
		}
	}
	
	private class ToastForbiddenAsyncTask extends AsyncTask<Void, Void, Void> {
		boolean flag = false;
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			publishProgress();
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			flag = true;
			publishProgress();
			
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			mOkBtn.setClickable(flag);
		}
	}
}
