package madcat.studio.dialogs;

import java.io.IOException;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

//배터리 완충/부족 알림 다이얼로그
public class BatteryStateNotifyDialog extends Activity {
	public static final String DIALOG_TYPE				= "dialog_type";
	public static final int DIALOG_TYPE_BATTERY_FULL	= 0;
	public static final int DIALOG_TYPE_BATTERY_LOW		= 1;
	
	public static final String DIALOG_THEME				= "application_theme";
	
	private static final int PADDING = 15;

	private LinearLayout mNotifyLayout = null;
	private TextView mNotifyTextView = null;
	
	private int mNotifyType = Constants.BATTERY_NOTIFICATION_ALERT_VIBRATE;
	private Vibrator mVibrator = null;
	private MediaPlayer mSoundPlayer = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_battery_notify);
		
		mNotifyLayout = (LinearLayout)findViewById(R.id.dialog_notify_layout);
		mNotifyTextView = (TextView)findViewById(R.id.dialog_notify_text);
		
		switch (getIntent().getIntExtra(DIALOG_THEME, Constants.APPLICATION_THEME_BASIC)) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mNotifyLayout.setBackgroundResource(R.drawable.dialog_background_pisces);
			mNotifyLayout.setBackgroundResource(R.color.pisces_blue);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mNotifyLayout.setBackgroundResource(R.drawable.dialog_background_aries);
			mNotifyLayout.setBackgroundResource(R.color.aries_pink);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mNotifyLayout.setBackgroundResource(R.drawable.dialog_background_taurus);
			mNotifyLayout.setBackgroundResource(R.color.taurus_white_brown);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		switch (getIntent().getIntExtra(DIALOG_TYPE, DIALOG_TYPE_BATTERY_FULL)) {
		case DIALOG_TYPE_BATTERY_FULL:
			mNotifyTextView.setText(R.string.battery_notify_dialog_battery_full);
			break;
		case DIALOG_TYPE_BATTERY_LOW:
			mNotifyTextView.setText(R.string.battery_notify_dialog_battery_low);
			break;
		}
		
		mNotifyLayout.setPadding(PADDING, PADDING, PADDING, PADDING);
		mNotifyLayout.setClickable(true);
		mNotifyLayout.setOnClickListener(new ClickEventHandler());
		
		switch (mNotifyType) {
		case Constants.BATTERY_NOTIFICATION_ALERT_VIBRATE:
			startVibrate();
			break;
		case Constants.BATTERY_NOTIFICATION_ALERT_SOUND:
			startSound();
			break;
		case Constants.BATTERY_NOTIFICATION_ALERT_BOTH:
			startVibrate();
			startSound();
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finish();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		switch (mNotifyType) {
		case Constants.BATTERY_NOTIFICATION_ALERT_VIBRATE:
			stopVibrate();
			break;
		case Constants.BATTERY_NOTIFICATION_ALERT_SOUND:
			stopSound();
			break;
		case Constants.BATTERY_NOTIFICATION_ALERT_BOTH:
			stopVibrate();
			stopSound();
			break;
		}
		
		super.onStop();
	}
	
	public void setNotifyType(int notifyType) {
		mNotifyType = notifyType;
	}
	
	private static final long VIBRATE_INTERVAL[] = {100, 1000, 500, 1000, 500, 1000};	//1Day, 24Hours.
	
	private void startVibrate() {
		mVibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		mVibrator.vibrate(VIBRATE_INTERVAL, -1);
	}
	
	private void stopVibrate() {
		if (mVibrator != null) {
			mVibrator.cancel();
			mVibrator = null;
		}
	}
	
	private static final String SOUND_PATH = "sound/sound_0.mp3";
	
	private void startSound() {
//		RingtoneManager ringtoneManager = new RingtoneManager(mContext);
//		ringtoneManager = 
		mSoundPlayer = new MediaPlayer();
		try {
			mSoundPlayer.setDataSource(getAssets().openFd(SOUND_PATH).getFileDescriptor());
			mSoundPlayer.setAudioStreamType(AudioManager.STREAM_RING);
			mSoundPlayer.setLooping(true);
			mSoundPlayer.prepare();
			mSoundPlayer.start();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void stopSound() {
		if (mSoundPlayer != null) {
			if (mSoundPlayer.isPlaying()) {
				mSoundPlayer.stop();
			}
			
			mSoundPlayer = null;
		}
	}
	
	private class ClickEventHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.equals(mNotifyLayout)) {
				switch (mNotifyType) {
				case Constants.BATTERY_NOTIFICATION_ALERT_VIBRATE:
					stopVibrate();
					break;
				case Constants.BATTERY_NOTIFICATION_ALERT_SOUND:
					stopSound();
					break;
				case Constants.BATTERY_NOTIFICATION_ALERT_BOTH:
					stopVibrate();
					stopSound();
					break;
				}
				finish();
			}
		}
	}
}