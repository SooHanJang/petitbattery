package madcat.studio.dialogs;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ApplicationThemeChooser extends Dialog {
	private static final int PADDING = 15;
	
	private Context mContext;
	private int mBackgroundTheme = Constants.APPLICATION_THEME_BASIC;
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	private LinearLayout mThemeLayout;
	private ImageView mThemeDisplayView;
	private TextView mThemeNameView;
	private Button mPrevBtn, mNextBtn;
	private Button mOkBtn, mCancelBtn;
	
	private String[] mThemeNameArray;
	private int mThemeIndex = 0;
	
	private boolean mFirstThemeSelect = false;
	
	public ApplicationThemeChooser(Context context, int styleId, int backgroundTheme) {
		// TODO Auto-generated constructor stub
		super(context, styleId);
		mContext = context;
		mBackgroundTheme = backgroundTheme;
		mThemeNameArray = context.getResources().getStringArray(R.array.application_theme_array);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_application_theme_chooser);
		
		mThemeLayout = (LinearLayout)findViewById(R.id.dialog_application_theme_layout);
		mThemeDisplayView = (ImageView)findViewById(R.id.dialog_application_theme_display_view);
		mThemeNameView = (TextView)findViewById(R.id.dialog_application_theme_name_view);
		mPrevBtn = (Button)findViewById(R.id.dialog_application_theme_btn_prev);
		mNextBtn = (Button)findViewById(R.id.dialog_application_theme_btn_next);
		mOkBtn = (Button)findViewById(R.id.dialog_application_theme_ok);
		mCancelBtn = (Button)findViewById(R.id.dialog_application_theme_cancel);
		
		switch (mBackgroundTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mThemeLayout.setBackgroundResource(R.drawable.dialog_background_pisces);
			mThemeDisplayView.setImageResource(R.drawable.preview_pisces);
			mPrevBtn.setBackgroundResource(R.drawable.com_preference_prev_btn_pisces);
			mNextBtn.setBackgroundResource(R.drawable.com_preference_next_btn_pisces);
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mThemeLayout.setBackgroundResource(R.drawable.dialog_background_aries);
			mThemeDisplayView.setImageResource(R.drawable.preview_aries);
			mPrevBtn.setBackgroundResource(R.drawable.com_preference_prev_btn_aries);
			mNextBtn.setBackgroundResource(R.drawable.com_preference_next_btn_aries);
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mThemeLayout.setBackgroundResource(R.drawable.dialog_background_taurus);
			mThemeDisplayView.setImageResource(R.drawable.preview_taurus);
			mPrevBtn.setBackgroundResource(R.drawable.com_preference_prev_btn_taurus);
			mNextBtn.setBackgroundResource(R.drawable.com_preference_next_btn_taurus);
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}

		switch (mApplicationTheme) {
		case Constants.APPLICATION_THEME_BASIC:
			mThemeDisplayView.setImageResource(R.drawable.preview_basic);
			break;
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mThemeDisplayView.setImageResource(R.drawable.preview_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mThemeDisplayView.setImageResource(R.drawable.preview_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mThemeDisplayView.setImageResource(R.drawable.preview_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			break;
		}
		
		mThemeLayout.setPadding(PADDING, PADDING, PADDING, PADDING);
		mThemeNameView.setText(mThemeNameArray[mApplicationTheme]);
		
		EvnetHandler eventHandler = new EvnetHandler();
		
		mPrevBtn.setOnClickListener(eventHandler);
		mNextBtn.setOnClickListener(eventHandler);
		mOkBtn.setOnClickListener(eventHandler);
		mCancelBtn.setOnClickListener(eventHandler);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		dismiss();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onStop();
	}
	
	public void setApplicaitonTheme(int theme) {
		mApplicationTheme = theme;
		mThemeIndex = theme;
	}
	
	public int getApplicationTheme() {
		return mApplicationTheme;
	}
	
	public void setFirstExecutionFlag(boolean first) {
		mFirstThemeSelect = first;
	}
	
	private class EvnetHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.dialog_application_theme_btn_prev:
				if (mThemeIndex == 0) {
					mThemeIndex = mThemeNameArray.length - 1;
				} else {
					mThemeIndex--;
				}
				
				switch (mThemeIndex) {
				case Constants.APPLICATION_THEME_CAPRICORN:
					mThemeDisplayView.setImageResource(R.drawable.preview_capricorn);
					break;
				case Constants.APPLICATION_THEME_AQUARIUS:
					mThemeDisplayView.setImageResource(R.drawable.preview_aquarius);
					break;
				case Constants.APPLICATION_THEME_PISCES:
					mThemeDisplayView.setImageResource(R.drawable.preview_pisces);
					break;
				case Constants.APPLICATION_THEME_ARIES:
					mThemeDisplayView.setImageResource(R.drawable.preview_aries);
					break;
				case Constants.APPLICATION_THEME_TAURUS:
					mThemeDisplayView.setImageResource(R.drawable.preview_taurus);
					break;
				case Constants.APPLICATION_THEME_GEMINI:
					mThemeDisplayView.setImageResource(R.drawable.preview_gemini);
					break;
				case Constants.APPLICATION_THEME_CANCER:
					mThemeDisplayView.setImageResource(R.drawable.preview_cancer);
					break;
				case Constants.APPLICATION_THEME_LEO:
					mThemeDisplayView.setImageResource(R.drawable.preview_leo);
					break;
				case Constants.APPLICATION_THEME_VIRGO:
					mThemeDisplayView.setImageResource(R.drawable.preview_virgo);
					break;
				case Constants.APPLICATION_THEME_LIBRA:
					mThemeDisplayView.setImageResource(R.drawable.preview_libra);
					break;
				case Constants.APPLICATION_THEME_SCORPIO:
					mThemeDisplayView.setImageResource(R.drawable.preview_scorpio);
					break;
				case Constants.APPLICATION_THEME_ARCHER:
					mThemeDisplayView.setImageResource(R.drawable.preview_archer);
					break;
				case Constants.APPLICATION_THEME_BASIC:
				default:
					mThemeDisplayView.setImageResource(R.drawable.preview_basic);
					break;
				}
				
				mThemeNameView.setText(mThemeNameArray[mThemeIndex]);
				break;
			case R.id.dialog_application_theme_btn_next:
				if (mThemeIndex == mThemeNameArray.length - 1) {
					mThemeIndex = 0;
				} else {
					mThemeIndex++;
				}
				
				switch (mThemeIndex) {
				case Constants.APPLICATION_THEME_CAPRICORN:
					mThemeDisplayView.setImageResource(R.drawable.preview_capricorn);
					break;
				case Constants.APPLICATION_THEME_AQUARIUS:
					mThemeDisplayView.setImageResource(R.drawable.preview_aquarius);
					break;
				case Constants.APPLICATION_THEME_PISCES:
					mThemeDisplayView.setImageResource(R.drawable.preview_pisces);
					break;
				case Constants.APPLICATION_THEME_ARIES:
					mThemeDisplayView.setImageResource(R.drawable.preview_aries);
					break;
				case Constants.APPLICATION_THEME_TAURUS:
					mThemeDisplayView.setImageResource(R.drawable.preview_taurus);
					break;
				case Constants.APPLICATION_THEME_GEMINI:
					mThemeDisplayView.setImageResource(R.drawable.preview_gemini);
					break;
				case Constants.APPLICATION_THEME_CANCER:
					mThemeDisplayView.setImageResource(R.drawable.preview_cancer);
					break;
				case Constants.APPLICATION_THEME_LEO:
					mThemeDisplayView.setImageResource(R.drawable.preview_leo);
					break;
				case Constants.APPLICATION_THEME_VIRGO:
					mThemeDisplayView.setImageResource(R.drawable.preview_virgo);
					break;
				case Constants.APPLICATION_THEME_LIBRA:
					mThemeDisplayView.setImageResource(R.drawable.preview_libra);
					break;
				case Constants.APPLICATION_THEME_SCORPIO:
					mThemeDisplayView.setImageResource(R.drawable.preview_scorpio);
					break;
				case Constants.APPLICATION_THEME_ARCHER:
					mThemeDisplayView.setImageResource(R.drawable.preview_archer);
					break;
				case Constants.APPLICATION_THEME_BASIC:
				default:
					mThemeDisplayView.setImageResource(R.drawable.preview_basic);
					break;
				}
				
				mThemeNameView.setText(mThemeNameArray[mThemeIndex]);
				break;
			case R.id.dialog_application_theme_ok:
				if (mApplicationTheme != mThemeIndex) {
					if (mThemeIndex == Constants.APPLICATION_THEME_PISCES ||
						mThemeIndex == Constants.APPLICATION_THEME_ARIES ||
						mThemeIndex == Constants.APPLICATION_THEME_TAURUS ||
						mThemeIndex == Constants.APPLICATION_THEME_BASIC) {
						mApplicationTheme = mThemeIndex;
						
						SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE).edit();
						
						if (mFirstThemeSelect) {
							editor.putInt(Constants.WIDGET_THEME, mThemeIndex);
							editor.putInt(Constants.NOTIFICATION_THEME, mThemeIndex);
						}
						
						editor.putInt(Constants.APPLICATION_THEME, mThemeIndex);
						editor.commit();
						
						Toast.makeText(mContext, mContext.getString(R.string.preference_theme_change_msg), Toast.LENGTH_SHORT).show();
						dismiss();
					} else {
						Toast.makeText(mContext, mContext.getString(R.string.preference_theme_not_prepare_msg), Toast.LENGTH_SHORT).show();
						new ToastForbiddenAsyncTask().execute();
					}
				} else {
					dismiss();
				}
				break;
			case R.id.dialog_application_theme_cancel:
				dismiss();
				break;
			}
		}
	}
	
	private class ToastForbiddenAsyncTask extends AsyncTask<Void, Void, Void> {
		boolean flag = false;
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			publishProgress();
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			flag = true;
			publishProgress();
			
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			mOkBtn.setClickable(flag);
		}
	}
}