package madcat.studio.dialogs;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class BatteryNotifySettingDialog extends Dialog {
	private Context mContext = null;
	
	private LinearLayout mNotifySettingLayout = null;
	private TextView mNotifySettingTitleView = null;
	
	private TextView mNotifySettingFullTitleView = null;
	private CheckBox mNotifySettingFullCheckBox = null;
	private LinearLayout mNotifySettingFullLayout = null, mNotifySettingFullSoundLayout = null;
	private TextView mNotifySettingFullMethodTitleView = null, mNotifySettingFullSoundTitleView = null;
	private Spinner mNotifySettingFullMethodSpinner = null, mNotifySettingFullSoundSpinner = null;
	
	private TextView mNotifySettingLowTitleView = null;
	private CheckBox mNotifySettingLowCheckBox = null;
	private LinearLayout mNotifySettingLowLayout = null, mNotifySettingLowSoundLayout = null;
	private TextView mNotifySettingLowBatteryLevelTitleView, mNotifySettingLowMethodTitleView = null, mNotifySettingLowSoundTitleView = null;
	private EditText mNotifySettingLowBatteryLevelTextView = null;
	private Spinner mNotifySettingLowMethodSpinner = null, mNotifySettingLowSoundSpinner = null;
	
	private Button mNotifySettingOkButton = null, mNotifySettingCancelButton = null;
	
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
//	private boolean mOriginFullChecked = true, mOriginLowChecked = true;
//	private int mOriginLevel = 0;
	
	public BatteryNotifySettingDialog(Context context, int styleId, int applicationTheme) {
		// TODO Auto-generated constructor stub
		super(context, styleId);
		
		mContext = context;
		mApplicationTheme = applicationTheme;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_notify_setting);
		
		mNotifySettingLayout = (LinearLayout)findViewById(R.id.dialog_notify_setting_layout);
		mNotifySettingTitleView = (TextView)findViewById(R.id.dialog_notify_setting_title);
		
		mNotifySettingFullTitleView = (TextView)findViewById(R.id.dialog_notify_setting_full_title);
		mNotifySettingFullCheckBox = (CheckBox)findViewById(R.id.dialog_notify_setting_full_checkbox);
		
		mNotifySettingFullLayout = (LinearLayout)findViewById(R.id.dialog_notify_setting_full_layout);
//		mNotifySettingFullSoundLayout = (LinearLayout)findViewById(R.id.dialog_notify_setting_full_sound_layout);
		
		mNotifySettingFullMethodTitleView = (TextView)findViewById(R.id.dialog_notify_setting_full_method_title);
		mNotifySettingFullMethodSpinner = (Spinner)findViewById(R.id.dialog_notify_setting_full_method_spinner);
//		mNotifySettingFullSoundTitleView = (TextView)findViewById(R.id.dialog_notify_setting_full_sound_title);
//		mNotifySettingFullSoundSpinner = (Spinner)findViewById(R.id.dialog_notify_setting_full_sound_spinner);
		
		mNotifySettingLowTitleView = (TextView)findViewById(R.id.dialog_notify_setting_low_title);
		mNotifySettingLowCheckBox = (CheckBox)findViewById(R.id.dialog_notify_setting_low_checkbox);
		
		mNotifySettingLowLayout = (LinearLayout)findViewById(R.id.dialog_notify_setting_low_layout);
//		mNotifySettingLowSoundLayout = (LinearLayout)findViewById(R.id.dialog_notify_setting_low_sound_layout);
		
		mNotifySettingLowBatteryLevelTitleView = (TextView)findViewById(R.id.dialog_notify_setting_low_battery_level_title);
		mNotifySettingLowMethodTitleView = (TextView)findViewById(R.id.dialog_notify_setting_low_method_title);
		mNotifySettingLowBatteryLevelTextView = (EditText)findViewById(R.id.dialog_notify_setting_low_battery_level_text);
		mNotifySettingLowMethodSpinner = (Spinner)findViewById(R.id.dialog_notify_setting_low_method_spinner);
//		mNotifySettingLowSoundTitleView = (TextView)findViewById(R.id.dialog_notify_setting_low_sound_title);
//		mNotifySettingLowSoundSpinner = (Spinner)findViewById(R.id.dialog_notify_setting_low_sound_spinner);
		
		mNotifySettingOkButton = (Button)findViewById(R.id.dialog_notify_setting_ok_button);
		mNotifySettingCancelButton = (Button)findViewById(R.id.dialog_notify_setting_cancel_button);
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mNotifySettingLayout.setBackgroundResource(R.drawable.dialog_background_pisces);
			mNotifySettingOkButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mNotifySettingCancelButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mNotifySettingLayout.setBackgroundResource(R.drawable.dialog_background_aries);
			mNotifySettingOkButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mNotifySettingCancelButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mNotifySettingLayout.setBackgroundResource(R.drawable.dialog_background_taurus);
			mNotifySettingOkButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mNotifySettingCancelButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			break;
		}
		
		ArrayAdapter fullSpinnerAdapter = ArrayAdapter.createFromResource(mContext, R.array.battery_notify_setting_dialog_spinner_array, android.R.layout.simple_spinner_item);
		fullSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mNotifySettingFullMethodSpinner.setAdapter(fullSpinnerAdapter);
		
		ArrayAdapter lowSpinnerAdapter = ArrayAdapter.createFromResource(mContext, R.array.battery_notify_setting_dialog_spinner_array, android.R.layout.simple_spinner_item);
		lowSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mNotifySettingLowMethodSpinner.setAdapter(lowSpinnerAdapter);
		
		CheckBoxEventHandler checkBoxEventHandler = new CheckBoxEventHandler();
		ClickEventHandler clickEventHandler = new ClickEventHandler();
//		SpinnerEventHandler spinnerEventHandler = new SpinnerEventHandler();
		
		mNotifySettingFullCheckBox.setOnCheckedChangeListener(checkBoxEventHandler);
		mNotifySettingLowCheckBox.setOnCheckedChangeListener(checkBoxEventHandler);

		mNotifySettingOkButton.setOnClickListener(clickEventHandler);
		mNotifySettingCancelButton.setOnClickListener(clickEventHandler);
		
//		mNotifySettingFullMethodSpinner.setOnItemSelectedListener(spinnerEventHandler);
//		mNotifySettingLowMethodSpinner.setOnItemSelectedListener(spinnerEventHandler);
		
		SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mLowAlarmLevel = pref.getInt(Constants.BATTERY_NOTIFICATION_LOW_ALERT_LEVEL, 20);
		
		mNotifySettingFullCheckBox.setChecked(pref.getBoolean(Constants.BATTERY_NOTIFICATION_ALERT_FULL, true));
		mNotifySettingLowCheckBox.setChecked(pref.getBoolean(Constants.BATTERY_NOTIFICATION_ALERT_LOW, true));
		mNotifySettingLowBatteryLevelTextView.setText(mLowAlarmLevel + "");
		
		mNotifySettingFullMethodSpinner.setSelection(pref.getInt(Constants.BATTERY_NOTIFICATION_FULL_ALERT_TYPE, Constants.BATTERY_NOTIFICATION_ALERT_VIBRATE));
		mNotifySettingLowMethodSpinner.setSelection(pref.getInt(Constants.BATTERY_NOTIFICATION_LOW_ALERT_TYPE, Constants.BATTERY_NOTIFICATION_ALERT_VIBRATE));
	}
	
	private boolean mSave = false;
	
	public boolean isSave() {
		return mSave;
	}
	
	public boolean isBatteryFullAlarm() {
		return mNotifySettingFullCheckBox.isChecked();
	}
	
	public boolean isBatteryLowAlarm() {
		return mNotifySettingLowCheckBox.isChecked();
	}
	
	public int getBatteryFullAlarmType() {
		if (isBatteryFullAlarm()) {
			return mNotifySettingLowMethodSpinner.getSelectedItemPosition();
		} else {
			return -1;
		}
	}
	
	public int getBatteryLowAlarmType() {
		if (isBatteryLowAlarm()) {
			return mNotifySettingLowMethodSpinner.getSelectedItemPosition();
		} else {
			return -1;
		}
	}
	
	public int getBatteryLowAlertLevel() {
		if (isBatteryLowAlarm()) {
			return Integer.parseInt(mNotifySettingLowBatteryLevelTextView.getText().toString());
		} else {
			return -1;
		}
	}
	
	private class CheckBoxEventHandler implements CompoundButton.OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			switch (buttonView.getId()) {
			case R.id.dialog_notify_setting_full_checkbox:
				if (isChecked) {
					mNotifySettingFullLayout.setVisibility(View.VISIBLE);
				} else {
					mNotifySettingFullLayout.setVisibility(View.GONE);
				}
				break;
			case R.id.dialog_notify_setting_low_checkbox:
				if (isChecked) {
					mNotifySettingLowLayout.setVisibility(View.VISIBLE);
				} else {
					mNotifySettingLowLayout.setVisibility(View.GONE);
				}
				break;
			}
		}
	}
	
	private int mLowAlarmLevel = 0;
	
	private class ClickEventHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.dialog_notify_setting_ok_button:
				String text = mNotifySettingLowBatteryLevelTextView.getText().toString();
				if (text == null || text.trim().equals("")) {
					Toast.makeText(mContext, "값을 입력해주세요.", Toast.LENGTH_SHORT).show();
					mNotifySettingLowBatteryLevelTextView.setText("");
					mNotifySettingLowBatteryLevelTextView.requestFocus();
				} else {
					try {
						mLowAlarmLevel = Integer.parseInt(text);
						
						if (mLowAlarmLevel <= 0) {
							Toast.makeText(mContext, "0보다 큰 정수를 입력하세요.", Toast.LENGTH_SHORT).show();
							mNotifySettingLowBatteryLevelTextView.setText("");
							mNotifySettingLowBatteryLevelTextView.requestFocus();
							return;
						}
						
						if (mLowAlarmLevel >= 100) {
							Toast.makeText(mContext, "100보다 작은 정수를 입력하세요.", Toast.LENGTH_SHORT).show();
							mNotifySettingLowBatteryLevelTextView.setText("");
							mNotifySettingLowBatteryLevelTextView.requestFocus();
							return;
						}
						
						//SET SharedPreference Value, on CheckBox and Spinner.
						SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE).edit();
						
						editor.putBoolean(Constants.BATTERY_NOTIFICATION_ALERT_FULL, mNotifySettingFullCheckBox.isChecked());
						editor.putInt(Constants.BATTERY_NOTIFICATION_FULL_ALERT_TYPE, mNotifySettingFullMethodSpinner.getSelectedItemPosition());
						editor.putBoolean(Constants.BATTERY_NOTIFICATION_ALERT_LOW, mNotifySettingLowCheckBox.isChecked());
						editor.putInt(Constants.BATTERY_NOTIFICATION_LOW_ALERT_TYPE, mNotifySettingLowMethodSpinner.getSelectedItemPosition());
						editor.putInt(Constants.BATTERY_NOTIFICATION_LOW_ALERT_LEVEL, mLowAlarmLevel);
						editor.commit();
						
						mSave = true;
						//End.
						dismiss();
					} catch (NumberFormatException e) {
						// TODO: handle exception
						Toast.makeText(mContext, "올바르지 않은 입력입니다.", Toast.LENGTH_SHORT).show();
						mNotifySettingLowBatteryLevelTextView.setText("");
						mNotifySettingLowBatteryLevelTextView.requestFocus();
					}
				}
				break;
			case R.id.dialog_notify_setting_cancel_button:
				mSave = false;
				dismiss();
				break;
			}
		}
	}

//	private class SpinnerEventHandler implements OnItemSelectedListener {
//		@Override
//		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//			// TODO Auto-generated method stub
//			switch (parent.getId()) {
//			case R.id.dialog_notify_setting_full_method_spinner:
//				mSelectedMethodFull = position;
//				break;
//			case R.id.dialog_notify_setting_low_method_spinner:
//				mSelectedMethodLow = position;
//				break;
//			}
//		}
//
//		@Override
//		public void onNothingSelected(AdapterView<?> arg0) {
//			// TODO Auto-generated method stub
//		}
//	}
}