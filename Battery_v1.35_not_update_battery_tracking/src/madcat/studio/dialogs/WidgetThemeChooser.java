package madcat.studio.dialogs;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.utils.Util;
import madcat.studio.widget.BatteryWidget;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class WidgetThemeChooser extends Dialog {
	private static final int PADDING = 15;
	
	private Context mContext;
	private int mBackgroundTheme = Constants.APPLICATION_THEME_BASIC;
	private int mWidgetTheme = Constants.WIDGET_THEME_BASIC;
	
	private LinearLayout mThemeLayout;
	private ImageView mThemeDisplayView;
	private TextView mThemeNameView;
	private Button mPrevBtn, mNextBtn;
	private Button mOkBtn, mCancelBtn;
	
	private String[] mThemeNameArray;
	private int mThemeIndex = 0;
	
	public WidgetThemeChooser(Context context, int styleId, int backgroundTheme) {
		// TODO Auto-generated constructor stub
		super(context, styleId);
		
		mContext = context;
		mBackgroundTheme = backgroundTheme;
		mThemeNameArray = context.getResources().getStringArray(R.array.widget_theme_array);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_widget_theme_chooser);
		
		mThemeLayout = (LinearLayout)findViewById(R.id.dialog_widget_theme_layout);
		mThemeDisplayView = (ImageView)findViewById(R.id.dialog_widget_theme_display_view);
		mThemeNameView = (TextView)findViewById(R.id.dialog_widget_theme_name_view);
		mPrevBtn = (Button)findViewById(R.id.dialog_widget_theme_btn_prev);
		mNextBtn = (Button)findViewById(R.id.dialog_widget_theme_btn_next);
		mOkBtn = (Button)findViewById(R.id.dialog_widget_theme_ok);
		mCancelBtn = (Button)findViewById(R.id.dialog_widget_theme_cancel);
		
		switch (mBackgroundTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mThemeLayout.setBackgroundResource(R.drawable.dialog_background_pisces);
			mPrevBtn.setBackgroundResource(R.drawable.com_preference_prev_btn_pisces);
			mNextBtn.setBackgroundResource(R.drawable.com_preference_next_btn_pisces);
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mThemeLayout.setBackgroundResource(R.drawable.dialog_background_aries);
			mPrevBtn.setBackgroundResource(R.drawable.com_preference_prev_btn_aries);
			mNextBtn.setBackgroundResource(R.drawable.com_preference_next_btn_aries);
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mThemeLayout.setBackgroundResource(R.drawable.dialog_background_taurus);
			mPrevBtn.setBackgroundResource(R.drawable.com_preference_prev_btn_taurus);
			mNextBtn.setBackgroundResource(R.drawable.com_preference_next_btn_taurus);
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		switch (mWidgetTheme) {
		case Constants.WIDGET_THEME_BASIC:
			mThemeDisplayView.setImageResource(R.drawable.widget_background_basic_4);
			break;
//		case Constants.WIDGET_THEME_CAPRICORN:
//			
//			break;
//		case Constants.WIDGET_THEME_AQUARIUS:
//			
//			break;
		case Constants.WIDGET_THEME_PISCES:
			mThemeDisplayView.setImageResource(R.drawable.widget_background_pisces_4);
			break;
		case Constants.WIDGET_THEME_ARIES:
			mThemeDisplayView.setImageResource(R.drawable.widget_background_aries_4);
			break;
		case Constants.WIDGET_THEME_TAURUS:
			mThemeDisplayView.setImageResource(R.drawable.widget_background_taurus_4);
			break;
//		case Constants.WIDGET_THEME_GEMINI:
//			
//			break;
//		case Constants.WIDGET_THEME_CANCER:
//			
//			break;
//		case Constants.WIDGET_THEME_LEO:
//			
//			break;
//		case Constants.WIDGET_THEME_VIRGO:
//			
//			break;
//		case Constants.WIDGET_THEME_LIBRA:
//			
//			break;
//		case Constants.WIDGET_THEME_SCORPIO:
//			
//			break;
//		case Constants.WIDGET_THEME_ARCHER:
//			
//			break;
		}
		
		mThemeLayout.setPadding(PADDING, PADDING, PADDING, PADDING);
		mThemeNameView.setText(mThemeNameArray[mWidgetTheme]);
		
		EvnetHandler eventHandler = new EvnetHandler();
		
		mPrevBtn.setOnClickListener(eventHandler);
		mNextBtn.setOnClickListener(eventHandler);
		mOkBtn.setOnClickListener(eventHandler);
		mCancelBtn.setOnClickListener(eventHandler);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		dismiss();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onStop();
	}
	
	public void setWidgetTheme(int theme) {
		mWidgetTheme = theme;
		mThemeIndex = theme;
	}
	
	public int getWidgetTheme() {
		return mWidgetTheme;
	}
	
	private class EvnetHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.dialog_widget_theme_btn_prev:
				if (mThemeIndex == 0) {
					mThemeIndex = mThemeNameArray.length - 1;
				} else {
					mThemeIndex--;
				}
				
				switch (mThemeIndex) {
				case Constants.WIDGET_THEME_CAPRICORN:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_capricorn_4);
					break;
				case Constants.WIDGET_THEME_AQUARIUS:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_aquarius_4);
					break;
				case Constants.WIDGET_THEME_PISCES:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_pisces_4);
					break;
				case Constants.WIDGET_THEME_ARIES:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_aries_4);
					break;
				case Constants.WIDGET_THEME_TAURUS:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_taurus_4);
					break;
				case Constants.WIDGET_THEME_GEMINI:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_gemini_4);
					break;
				case Constants.WIDGET_THEME_CANCER:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_cancer_4);
					break;
				case Constants.WIDGET_THEME_LEO:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_leo_4);
					break;
				case Constants.WIDGET_THEME_VIRGO:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_virgo_4);
					break;
				case Constants.WIDGET_THEME_LIBRA:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_libra_4);
					break;
				case Constants.WIDGET_THEME_SCORPIO:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_scorpio_4);
					break;
				case Constants.WIDGET_THEME_ARCHER:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_archer_4);
					break;
				case Constants.WIDGET_THEME_BASIC:
				default:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_basic_4);
					break;
				}
				
				mThemeNameView.setText(mThemeNameArray[mThemeIndex]);
				break;
			case R.id.dialog_widget_theme_btn_next:
				if (mThemeIndex == mThemeNameArray.length - 1) {
					mThemeIndex = 0;
				} else {
					mThemeIndex++;
				}
				
				switch (mThemeIndex) {
				case Constants.WIDGET_THEME_CAPRICORN:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_capricorn_4);
					break;
				case Constants.WIDGET_THEME_AQUARIUS:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_aquarius_4);
					break;
				case Constants.WIDGET_THEME_PISCES:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_pisces_4);
					break;
				case Constants.WIDGET_THEME_ARIES:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_aries_4);
					break;
				case Constants.WIDGET_THEME_TAURUS:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_taurus_4);
					break;
				case Constants.WIDGET_THEME_GEMINI:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_gemini_4);
					break;
				case Constants.WIDGET_THEME_CANCER:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_cancer_4);
					break;
				case Constants.WIDGET_THEME_LEO:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_leo_4);
					break;
				case Constants.WIDGET_THEME_VIRGO:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_virgo_4);
					break;
				case Constants.WIDGET_THEME_LIBRA:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_libra_4);
					break;
				case Constants.WIDGET_THEME_SCORPIO:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_scorpio_4);
					break;
				case Constants.WIDGET_THEME_ARCHER:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_archer_4);
					break;
				case Constants.WIDGET_THEME_BASIC:
				default:
					mThemeDisplayView.setImageResource(R.drawable.widget_background_basic_4);
					break;
				}
				
				mThemeNameView.setText(mThemeNameArray[mThemeIndex]);
				break;
			case R.id.dialog_widget_theme_ok:
				if (mWidgetTheme != mThemeIndex) {
					if (mThemeIndex == Constants.WIDGET_THEME_PISCES ||
						mThemeIndex == Constants.WIDGET_THEME_ARIES ||
						mThemeIndex == Constants.WIDGET_THEME_TAURUS ||
						mThemeIndex == Constants.WIDGET_THEME_BASIC) {
						mWidgetTheme = mThemeIndex;
						
						SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
						SharedPreferences.Editor editor = pref.edit();
						editor.putInt(Constants.WIDGET_THEME, mThemeIndex);
						editor.commit();
						
						Intent widgetUpdate = new Intent(Constants.WIDGET_THEME_ACTION);
						widgetUpdate.putExtra(BatteryWidget.BATTERY_LEVEL, pref.getInt(Constants.RECORD_BATTERY_LEVEL, 100));
						widgetUpdate.putExtra(BatteryWidget.WIDGET_THEME, mThemeIndex);
						mContext.sendBroadcast(widgetUpdate);
						dismiss();
					} else {
						Toast.makeText(mContext, mContext.getString(R.string.preference_theme_not_prepare_msg), Toast.LENGTH_SHORT).show();
						new ToastForbiddenAsyncTask().execute();
					}
				} else {
					dismiss();
				}
				break;
			case R.id.dialog_widget_theme_cancel:
				dismiss();
				break;
			}
		}
	}
	
	private class ToastForbiddenAsyncTask extends AsyncTask<Void, Void, Void> {
		boolean flag = false;
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			publishProgress();
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			flag = true;
			publishProgress();
			
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			mOkBtn.setClickable(flag);
		}
	}
}