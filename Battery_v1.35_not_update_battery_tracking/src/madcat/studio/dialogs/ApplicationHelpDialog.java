package madcat.studio.dialogs;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.utils.Util;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ApplicationHelpDialog extends Dialog {
	private static final int PADDING = 15;
	
	private static final int PAGE_START = 1;
	private static final int PAGE_END = 8;
	
	private Context mContext = null;
	
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	private LinearLayout mHelpLayout = null;
	private ImageView mHelpImageView = null;
	private ImageView mHelpPageView = null;
	private Button mHelpPrevButton = null, mHelpNextButton = null;
	
	private int mPage = PAGE_START;
	
	public ApplicationHelpDialog(Context context, int styleId, int applicationTheme) {
		// TODO Auto-generated constructor stub
		super(context, styleId);
		mContext = context;
		mApplicationTheme = applicationTheme;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_application_help);
		
		mHelpLayout = (LinearLayout)findViewById(R.id.dialog_help_layout);
		mHelpImageView = (ImageView)findViewById(R.id.dialog_help_page_image);
		mHelpPageView = (ImageView)findViewById(R.id.dialog_help_page_view);
		mHelpPrevButton = (Button)findViewById(R.id.dialog_help_page_prev);
		mHelpNextButton = (Button)findViewById(R.id.dialog_help_page_next);
		
		mHelpImageView.setImageResource(R.drawable.help_page_image_1);
		mHelpPageView.setImageResource(R.drawable.help_page_view_1);
		
		mHelpPrevButton.setText(mContext.getString(R.string.button_prev));
		mHelpNextButton.setText(mContext.getString(R.string.button_next));
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mHelpLayout.setBackgroundResource(R.drawable.dialog_background_pisces);
			mHelpPrevButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mHelpNextButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mHelpLayout.setBackgroundResource(R.drawable.dialog_background_aries);
			mHelpPrevButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mHelpNextButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mHelpLayout.setBackgroundResource(R.drawable.dialog_background_taurus);
			mHelpPrevButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mHelpNextButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//
//			break;
		}
		
		mHelpLayout.setPadding(PADDING, PADDING, PADDING, PADDING);
		
		ClickEventHandler eventHandler = new ClickEventHandler();
		mHelpPrevButton.setOnClickListener(eventHandler);
		mHelpNextButton.setOnClickListener(eventHandler);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		dismiss();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onStop();
	}
	
	private static final String PREFIX_HELP_IMAGE = "help_page_image_";
	private static final String PREFIX_HELP_VIEW = "help_page_view_";
	
	private class ClickEventHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.dialog_help_page_prev:
				if (mPage > PAGE_START) {
					mPage--;
					if (mPage == PAGE_START) {
						mHelpImageView.setImageResource(mContext.getResources().getIdentifier(PREFIX_HELP_IMAGE + mPage, "drawable", mContext.getPackageName()));
						mHelpPageView.setImageResource(mContext.getResources().getIdentifier(PREFIX_HELP_VIEW + mPage, "drawable", mContext.getPackageName()));
						mHelpNextButton.setText(mContext.getString(R.string.button_prev));
						mHelpNextButton.setText(mContext.getString(R.string.button_next));
					} else {
						mHelpImageView.setImageResource(mContext.getResources().getIdentifier(PREFIX_HELP_IMAGE + mPage, "drawable", mContext.getPackageName()));
						mHelpPageView.setImageResource(mContext.getResources().getIdentifier(PREFIX_HELP_VIEW + mPage, "drawable", mContext.getPackageName()));
						mHelpPrevButton.setText(mContext.getString(R.string.button_prev));
						mHelpNextButton.setText(mContext.getString(R.string.button_next));
					}
				}
				break;
			case R.id.dialog_help_page_next:
				if (mPage < PAGE_END) {
					mPage++;
					if (mPage == PAGE_END) {
						mHelpImageView.setImageResource(mContext.getResources().getIdentifier(PREFIX_HELP_IMAGE + mPage, "drawable", mContext.getPackageName()));
						mHelpPageView.setImageResource(mContext.getResources().getIdentifier(PREFIX_HELP_VIEW + mPage, "drawable", mContext.getPackageName()));
						mHelpPrevButton.setText(mContext.getString(R.string.button_prev));
						mHelpNextButton.setText(mContext.getString(R.string.button_close));
					} else {
						mHelpImageView.setImageResource(mContext.getResources().getIdentifier(PREFIX_HELP_IMAGE + mPage, "drawable", mContext.getPackageName()));
						mHelpPageView.setImageResource(mContext.getResources().getIdentifier(PREFIX_HELP_VIEW + mPage, "drawable", mContext.getPackageName()));
						mHelpPrevButton.setText(mContext.getString(R.string.button_prev));
						mHelpNextButton.setText(mContext.getString(R.string.button_next));
					}
				} else {
					dismiss();
				}
				break;
			}
		}
	}
}
