package madcat.studio.service;

public class BatteryServiceConstants {
	public static final int SERVICE_MODE_RECORDING = 0;
	public static final int SERVICE_MODE_OPTIMIZATION = 1;
	public static final int SERVICE_MODE_NOTIFICATION = 2;
	public static final int SERVICE_MODE_ALL = 3;
	
	public static final int USAGE_DATA_UPDATE = 30;	//30min

	public static final int BRIGHTNESS_AUTO_POW = 0;
	public static final int BRIGHTNESS_AUTO_MAX = 1;
	public static final int BRIGHTNESS_AUTO_GEN = 2;
	public static final int BRIGHTNESS_AUTO_MIN = 3;
	
	public static final int BRIGHTNESS_CUST_POW = 4;
	public static final int BRIGHTNESS_CUST_MAX = 5;
	public static final int BRIGHTNESS_CUST_GEN = 6;
	public static final int BRIGHTNESS_CUST_MIN = 7;
	
	public static final int OPTIMIZING_AUTO_POW = 0;
	public static final int OPTIMIZING_AUTO_MAX = 1;
	public static final int OPTIMIZING_AUTO_GEN = 2;
	public static final int OPTIMIZING_AUTO_MIN = 3;
	
	public static final int OPTIMIZING_CUST_POW = 4;
	public static final int OPTIMIZING_CUST_MAX = 5;
	public static final int OPTIMIZING_CUST_GEN = 6;
	public static final int OPTIMIZING_CUST_MIN = 7;
}
