package madcat.studio.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import madcat.studio.adapter.ProcessInfo;
import madcat.studio.constants.Constants;
import madcat.studio.dialogs.BatteryStateNotifyDialog;
import madcat.studio.schedule.constants.ScheduleConstants;
import madcat.studio.utils.ScreenBrightnessController;
import madcat.studio.utils.TrackingData;
import madcat.studio.utils.UsageData;
import madcat.studio.utils.Util;
import madcat.studio.widget.BatteryWidget;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;

public class IntegrityService extends Service {
	public static final String SERVICE_EXCUTE_ACTION	= "madcat.studio.battery.service.pro";
	public static final String SERVICE_UPDATE_ACTION	= "madcat.studio.battery.service.pro.update";
	
	public static final String SERVICE_HARDWARE_WIFI_ACTION			= "madcat.studio.battery.service.pro.hardware.wifi";
	public static final String SERVICE_HARDWARE_BLUETOOTH_ACTION	= "madcat.studio.battery.service.pro.hardware.bluetooth";
	public static final String SERVICE_HARDWARE_PROCESS_ACTION		= "madcat.studio.battery.service.pro.hardware.process";
	
	private Context mContext = null;
	
	private IntegityBroadcastReceiver mBroadcastReceiver = null;
	
	private AlarmManager mAlarmManager = null;
	private PendingIntent mAlarmIntent = null;				//for Service Update.
	
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	private int mWidgetTheme = Constants.WIDGET_THEME_BASIC;
	private int mNotificationTheme = Constants.NOTIFICATION_THEME_BASIC_RECTANGLE;
	
	private boolean mIsManagementFlag = true;
	private boolean mIsNotificationFlag = true;
	private boolean mIsTrackingFlag = true;
	private boolean mIsAlertFullFlag = true;
	private boolean mIsAlertLowFlag = true;

	private boolean mIsPowerConnected = false;
	private boolean mIsScreenOn = true;
	
	private boolean mIsBlueToothConnected = false;
	
	private boolean mManagementType = Constants.OPTIMIZATION_TYPE_AUTO;
	
	//Custom Properties.
	private Integer[] mMaximumRange = null;
	private Integer[] mGeneralRange = null;
	private Integer[] mMinimumRange = null;
	
	private Integer[] mServiceDelay = null;
	private Boolean[] mIsWifiControl = null;
	private Boolean[] mIsBlueToothControl = null;
	private Boolean[] mIsProcessControl = null;
	private Boolean[] mIsScreenControl = null;
	private Integer[] mScreenBrightness = null;
	private Integer[] mScreenTimeout = null;
		
//	private int mCurrentOptimizingMode = -1;
	
	private boolean mBatteryNotificationDuplicationFlag = false;
	
	private static final int DEFAULT_BATTERY_LOW_LEVEL = 20;
	private int mBatteryLowLevel = DEFAULT_BATTERY_LOW_LEVEL;
	
	private static final int DEFAULT_BATTERY_LEVEL = 70;
	private int mBatteryLevel = DEFAULT_BATTERY_LEVEL;
	
	private int mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_AUTO_GEN;
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		
		mContext = this;
		
		registryBroadcastReceiver();
		registryAlarm();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		super.onStartCommand(intent, flags, startId);
		
		String action = intent.getAction();
		
		SharedPreferences pref = getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mApplicationTheme = pref.getInt(Constants.APPLICATION_THEME, Constants.APPLICATION_THEME_BASIC);
		mWidgetTheme = pref.getInt(Constants.WIDGET_THEME, Constants.WIDGET_THEME_BASIC);
		mNotificationTheme = pref.getInt(Constants.NOTIFICATION_THEME, Constants.NOTIFICATION_THEME_BASIC_RECTANGLE);
		
		mIsManagementFlag = pref.getBoolean(Constants.OPTIMIZATION_EXECUTE, true);
		mIsNotificationFlag = pref.getBoolean(Constants.NOTIFICATION_EXECUTE, true);
		mIsTrackingFlag = pref.getBoolean(Constants.BATTERY_TRACKING_EXECUTE, true);
		mIsAlertFullFlag = pref.getBoolean(Constants.BATTERY_NOTIFICATION_ALERT_FULL, true);
		mIsAlertLowFlag = pref.getBoolean(Constants.BATTERY_NOTIFICATION_ALERT_LOW, true);
		
		mManagementType = pref.getBoolean(Constants.OPTIMIZATION_TYPE, Constants.OPTIMIZATION_TYPE_AUTO);
		
		mBatteryNotificationDuplicationFlag = false;
		mBatteryLowLevel = pref.getInt(Constants.BATTERY_NOTIFICATION_LOW_ALERT_LEVEL, DEFAULT_BATTERY_LOW_LEVEL);
		
		mIsPowerConnected = pref.getBoolean(Constants.SERVICE_PLUG_CONNECTED, false);
		
		mIsBlueToothConnected = pref.getBoolean(Constants.SERVICE_BLUETOOTH_CONNECTED, false);
		
		if (action.equals(SERVICE_EXCUTE_ACTION)) {
			PowerManager pm = (PowerManager)mContext.getSystemService(Context.POWER_SERVICE);
			mIsScreenOn = pm.isScreenOn();
			
			Intent batteryLevelChecker = registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
			mBatteryLevel = batteryLevelChecker.getIntExtra(BatteryManager.EXTRA_LEVEL, DEFAULT_BATTERY_LEVEL);
			
			setBatteryManagementProperties();
			
			if (mIsPowerConnected) {
				if (mManagementType) {	//AUTO
					mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_AUTO_POW;
				} else {				//CUSTOM
					mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_CUST_POW;
				}
			} else {
				if (mManagementType) {
					if (ScheduleConstants.MAXIMUM_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.MAXIMUM_RANGE_END) {
						mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_AUTO_MAX;
					} else if (ScheduleConstants.GENERAL_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.GENERAL_RANGE_END) {
						mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_AUTO_GEN;
					} else if (ScheduleConstants.MINIMUM_RANGE_START <= mBatteryLevel && mBatteryLevel <= ScheduleConstants.MINIMUM_RANGE_END) {
						mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_AUTO_MIN;
					}
				} else {
					if (mMaximumRange[0] < mBatteryLevel && mBatteryLevel <= mMaximumRange[1]) {
						mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_CUST_MAX;
					} else if (mGeneralRange[0] < mBatteryLevel && mBatteryLevel <= mGeneralRange[1]) {
						mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_CUST_GEN;
					} else if (mMinimumRange[0] < mBatteryLevel && mBatteryLevel <= mMinimumRange[1]) {
						mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_CUST_MIN;
					}
				}
			}
			widgetUpdate();
			
			if (!mIsScreenOn && mIsManagementFlag) {
				registryAlarm();
			}
			
			if (mIsNotificationFlag) { 
				BatteryNotification notification = BatteryNotification.getInstance();
				notification.setNotification(mContext);
			}
		} else if (action.equals(SERVICE_UPDATE_ACTION)) {
			//Notification Widget Update.
			widgetUpdate();
			if (mIsNotificationFlag) { notificationUpdate(); }
		} else if (action.equals(SERVICE_HARDWARE_WIFI_ACTION)) {
			//Hardware Control
			if (mIsManagementFlag) { wifiControl(); }
		} else if (action.equals(SERVICE_HARDWARE_BLUETOOTH_ACTION)) {
			if (mIsManagementFlag) { blueToothControl(); }
		} else if (action.equals(SERVICE_HARDWARE_PROCESS_ACTION)) {
			if (mIsManagementFlag) { processControl(); }
		}
		
		return START_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	private static final int RESTART_INTERVAL = 5000;
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		unregistryBroadcastReceiver();
		unregistryAlarm();
		unregistryHardwareAlarm();
		
		if (mIsNotificationFlag) {
			BatteryNotification notification = BatteryNotification.getInstance();
			notification.unsetNotification();
		}
		
		Util.setNetworkUsage(this);
		Util.setCallUsage(this);
		Util.setSMSUsage(this);
		
		PendingIntent pendingIntent = PendingIntent.getService(mContext, 0, new Intent(SERVICE_EXCUTE_ACTION), 0);
		mAlarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + RESTART_INTERVAL, pendingIntent);
		
		super.onDestroy();
	}
	
	private void setBatteryManagementProperties() {
		if (!mManagementType) {
			//Load Custom Values from Database.
			mMaximumRange = new Integer[2];
			mGeneralRange = new Integer[2];
			mMinimumRange = new Integer[2];
			
			mServiceDelay = new Integer[4];
			mIsWifiControl = new Boolean[4];
			mIsBlueToothControl = new Boolean[4];
			mIsProcessControl = new Boolean[4];
			mIsScreenControl = new Boolean[4];
			mScreenBrightness = new Integer[4];
			mScreenTimeout = new Integer[4];
			
			SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
			
			mMaximumRange[0] = pref.getInt(ScheduleConstants.SCHEDULE_RANGE_MAXIMUM_START, ScheduleConstants.MAXIMUM_RANGE_START);
			mMaximumRange[1] = pref.getInt(ScheduleConstants.SCHEDULE_RANGE_MAXIMUM_END, ScheduleConstants.MAXIMUM_RANGE_END);
			
			mGeneralRange[0] = pref.getInt(ScheduleConstants.SCHEDULE_RANGE_GENERAL_START, ScheduleConstants.GENERAL_RANGE_START);
			mGeneralRange[1] = pref.getInt(ScheduleConstants.SCHEDULE_RANGE_GENERAL_END, ScheduleConstants.GENERAL_RANGE_END);
			
			mMinimumRange[0] = pref.getInt(ScheduleConstants.SCHEDULE_RANGE_MINIMUM_START, ScheduleConstants.MINIMUM_RANGE_START);
			mMinimumRange[1] = pref.getInt(ScheduleConstants.SCHEDULE_RANGE_MINIMUM_END, ScheduleConstants.MINIMUM_RANGE_END);
			
			mServiceDelay[0] = pref.getInt(ScheduleConstants.SCHEDULE_DELAY_POWER, ScheduleConstants.DELAY_SHORTEST);
			mServiceDelay[1] = pref.getInt(ScheduleConstants.SCHEDULE_DELAY_MAXIMUM, ScheduleConstants.DELAY_SHORTEST);
			mServiceDelay[2] = pref.getInt(ScheduleConstants.SCHEDULE_DELAY_GENERAL, ScheduleConstants.DELAY_SHORT);
			mServiceDelay[3] = pref.getInt(ScheduleConstants.SCHEDULE_DELAY_MINIMUM, ScheduleConstants.DELAY_LONG);
			
			mIsWifiControl[0] = pref.getBoolean(ScheduleConstants.SCHEDULE_WIFI_POWER, false);
			mIsWifiControl[1] = pref.getBoolean(ScheduleConstants.SCHEDULE_WIFI_MAXIMUM, false);
			mIsWifiControl[2] = pref.getBoolean(ScheduleConstants.SCHEDULE_WIFI_GENERAL, true);
			mIsWifiControl[3] = pref.getBoolean(ScheduleConstants.SCHEDULE_WIFI_MINIMUM, true);
			
			mIsBlueToothControl[0] = pref.getBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_POWER, false);
			mIsBlueToothControl[1] = pref.getBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_MAXIMUM, false);
			mIsBlueToothControl[2] = pref.getBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_GENERAL, true);
			mIsBlueToothControl[3] = pref.getBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_MINIMUM, true);
			
			mIsProcessControl[0] = pref.getBoolean(ScheduleConstants.SCHEDULE_PROCESS_POWER, false);
			mIsProcessControl[1] = pref.getBoolean(ScheduleConstants.SCHEDULE_PROCESS_MAXIMUM, false);
			mIsProcessControl[2] = pref.getBoolean(ScheduleConstants.SCHEDULE_PROCESS_GENERAL, false);
			mIsProcessControl[3] = pref.getBoolean(ScheduleConstants.SCHEDULE_PROCESS_MINIMUM, false);
			
			mIsScreenControl[0] = pref.getBoolean(ScheduleConstants.SCHEDULE_SCREEN_POWER, true);
			mIsScreenControl[1] = pref.getBoolean(ScheduleConstants.SCHEDULE_SCREEN_MAXIMUM, true);
			mIsScreenControl[2] = pref.getBoolean(ScheduleConstants.SCHEDULE_SCREEN_GENERAL, true);
			mIsScreenControl[3] = pref.getBoolean(ScheduleConstants.SCHEDULE_SCREEN_MINIMUM, true);
			
			mScreenBrightness[0] = pref.getInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_POWER, ScheduleConstants.SCREEN_BRIGHTNESS_MAX);
			mScreenBrightness[1] = pref.getInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_MAXIMUM, ScheduleConstants.SCREEN_BRIGHTNESS_MAX);
			mScreenBrightness[2] = pref.getInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_GENERAL, ScheduleConstants.SCREEN_BRIGHTNESS_MID);
			mScreenBrightness[3] = pref.getInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_MINIMUM, ScheduleConstants.SCREEN_BRIGHTNESS_MIN);
			
			mScreenTimeout[0] = pref.getInt(ScheduleConstants.SCHEDULE_TIMEOUT_POWER, ScheduleConstants.SCREEN_TIMEOUT_LONGEST);
			mScreenTimeout[1] = pref.getInt(ScheduleConstants.SCHEDULE_TIMEOUT_MAXIMUM, ScheduleConstants.SCREEN_TIMEOUT_LONG);
			mScreenTimeout[2] = pref.getInt(ScheduleConstants.SCHEDULE_TIMEOUT_GENERAL, ScheduleConstants.SCREEN_TIMEOUT_NORMAL);
			mScreenTimeout[3] = pref.getInt(ScheduleConstants.SCHEDULE_TIMEOUT_MINIMUM, ScheduleConstants.SCREEN_TIMEOUT_SHORT);
		}
		
		Settings.System.putInt(getContentResolver(), Settings.System.WIFI_SLEEP_POLICY, Settings.System.WIFI_SLEEP_POLICY_NEVER);
		
		if (mIsManagementFlag && !mIsScreenOn) {
			if (mIsPowerConnected) {
				if (mManagementType) {
					if (ScheduleConstants.POWER_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_POW); }
				} else {
					if (mIsScreenControl[0]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_POW); }
				}
			} else {
				if (mManagementType) {
					if (ScheduleConstants.MAXIMUM_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.MAXIMUM_RANGE_END) {
						if (ScheduleConstants.MAXIMUM_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_MAX); }
					} else if (ScheduleConstants.GENERAL_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.GENERAL_RANGE_END) {
						if (ScheduleConstants.GENERAL_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_GEN); }
					} else if (ScheduleConstants.MINIMUM_RANGE_START <= mBatteryLevel && mBatteryLevel <= ScheduleConstants.MINIMUM_RANGE_END) {
						if (ScheduleConstants.MINIMUM_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_MIN); }
					}
				} else {
					if (mMaximumRange[0] < mBatteryLevel && mBatteryLevel <= mMaximumRange[1]) {
						if (mIsScreenControl[1]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_MAX); }
					} else if (mGeneralRange[0] < mBatteryLevel && mBatteryLevel <= mGeneralRange[1]) {
						if (mIsScreenControl[2]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_GEN); }
					} else if (mMinimumRange[0] <= mBatteryLevel && mBatteryLevel <= mMinimumRange[1]) {
						if (mIsScreenControl[3]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_MIN); }
					}
				}
			}
		}
	}
	
	//It's work. Dev Complete. 2011. 12. 02. Confirmed By Daniel.
		private boolean brightnessControl(int flag) {
			Intent brightnessControl = new Intent(this, ScreenBrightnessController.class);
			
			switch (flag) {
			case BatteryServiceConstants.BRIGHTNESS_AUTO_POW:
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_BRIGHTNESS, ScheduleConstants.POWER_BRIGHTNESS);
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_TIMEOUT, ScheduleConstants.POWER_TIMEOUT);
				break;
			case BatteryServiceConstants.BRIGHTNESS_AUTO_MAX:
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_BRIGHTNESS, ScheduleConstants.MAXIMUM_BRIGHTNESS);
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_TIMEOUT, ScheduleConstants.MAXIMUM_TIMEOUT);
				break;
			case BatteryServiceConstants.BRIGHTNESS_AUTO_GEN:
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_BRIGHTNESS, ScheduleConstants.GENERAL_BRIGHTNESS);
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_TIMEOUT, ScheduleConstants.GENERAL_TIMEOUT);
				break;
			case BatteryServiceConstants.BRIGHTNESS_AUTO_MIN:
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_BRIGHTNESS, ScheduleConstants.MINIMUM_BRIGHTNESS);
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_TIMEOUT, ScheduleConstants.MINIMUM_TIMEOUT);
				break;
			case BatteryServiceConstants.BRIGHTNESS_CUST_POW:
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_BRIGHTNESS, mScreenBrightness[0]);
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_TIMEOUT, mScreenTimeout[0]);
				break;
			case BatteryServiceConstants.BRIGHTNESS_CUST_MAX:
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_BRIGHTNESS, mScreenBrightness[1]);
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_TIMEOUT, mScreenTimeout[1]);
				break;
			case BatteryServiceConstants.BRIGHTNESS_CUST_GEN:
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_BRIGHTNESS, mScreenBrightness[2]);
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_TIMEOUT, mScreenTimeout[2]);
				break;
			case BatteryServiceConstants.BRIGHTNESS_CUST_MIN:
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_BRIGHTNESS, mScreenBrightness[3]);
				brightnessControl.putExtra(ScreenBrightnessController.SCREEN_TIMEOUT, mScreenTimeout[3]);
				break;
			default:
				return false;
			}
			
			brightnessControl.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(brightnessControl);
			
			return true;
		}
	
		//It's work. Dev Complete. 2012. 04. 10. Confirmed By Daniel.
		private boolean wifiControl() {
			ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
			WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
			
			if (connectivityManager != null && wifiManager != null) {
				if (wifiManager.isWifiEnabled()) {
					if (connectivityManager.getActiveNetworkInfo().getType() != ConnectivityManager.TYPE_WIFI) {
						wifiManager.setWifiEnabled(false);
						return true;
					}
				}
			}
			
			return false;
		}
		
		//It's work. Dev Complete. 2012. 04. 10. Confirmed By Daniel.
		private boolean blueToothControl() {
			BluetoothAdapter blueToothAdapter = BluetoothAdapter.getDefaultAdapter();
			
			if (blueToothAdapter != null) {
				if (blueToothAdapter.isEnabled()) {
					if (!mIsBlueToothConnected) {
						if (blueToothAdapter.isDiscovering()) {
							blueToothAdapter.cancelDiscovery();
						}
						blueToothAdapter.disable();
						return true;
					}
				}
			}
			
			return false;
		}
		
		//It's work. Dev Complete. 2012. 04. 10. Confirmed By Daniel.
		private boolean processControl() {
			ArrayList<ProcessInfo> processInfoList = new ArrayList<ProcessInfo>();
			
			ActivityManager activityManager = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
			PackageManager packageManager = getPackageManager();
			
			List<RunningAppProcessInfo> runProcessInfoList = activityManager.getRunningAppProcesses();
			ApplicationInfo applicationInfo = null;

			//Get Launcher Application's package name.
			Intent intent = new Intent(Intent.ACTION_MAIN, null);
			intent.addCategory(Intent.CATEGORY_HOME);
			List<ResolveInfo> launcherInfoList = packageManager.queryIntentActivities(intent, 0);
			HashSet<String> launcherSet = new HashSet<String>();
			
			for (ResolveInfo resolveInfo : launcherInfoList) {
				launcherSet.add(resolveInfo.activityInfo.packageName);
			}

			launcherInfoList = null;
			
			boolean isLauncher = false;
			
			for (RunningAppProcessInfo runProcessInfo : runProcessInfoList) {
				ProcessInfo processInfo = new ProcessInfo();
				isLauncher = false;
				
				try {
					applicationInfo = packageManager.getApplicationInfo(runProcessInfo.pkgList[0], PackageManager.GET_META_DATA);
					
					if (launcherSet.contains(applicationInfo.packageName)) {
						processInfo.setCategory(ProcessInfo.LAUNCHER);
						isLauncher = true;
					}
					
					if (!isLauncher) {
						if ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
							processInfo.setCategory(ProcessInfo.SYSTEM);
						} else {
							if (runProcessInfo.importance == RunningAppProcessInfo.IMPORTANCE_VISIBLE || runProcessInfo.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
								processInfo.setCategory(ProcessInfo.IMPORTANT);
							} else if (runProcessInfo.importance == RunningAppProcessInfo.IMPORTANCE_SERVICE) {
								processInfo.setCategory(ProcessInfo.SERVICE);
							} else if (runProcessInfo.importance == RunningAppProcessInfo.IMPORTANCE_BACKGROUND || runProcessInfo.importance == RunningAppProcessInfo.IMPORTANCE_EMPTY) {
								processInfo.setCategory(ProcessInfo.NOT_IMPORTANT);
							} else if (runProcessInfo.importance == 130) {	//130 : IMPORTANCE_PERCEPTIBLE, added API Version 9.
								processInfo.setCategory(ProcessInfo.PERCEPTIBLE);
							} else {
								processInfo.setCategory(ProcessInfo.UNKNOWN);
							}
						}
					}
					
					processInfo.setPackageName(runProcessInfo.pkgList[0]);
					processInfo.setPid(runProcessInfo.pid);
					processInfo.setLru(runProcessInfo.lru);
					
					int[] pids = {runProcessInfo.pid};
					processInfo.setMemUsage(activityManager.getProcessMemoryInfo(pids)[0].getTotalPss());
					
					processInfoList.add(processInfo);
				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				applicationInfo = null;
			}
			
			for (ProcessInfo processInfo : processInfoList) {
				if (processInfo.getCategory() == ProcessInfo.NOT_IMPORTANT || processInfo.getCategory() == ProcessInfo.UNKNOWN || processInfo.getLru() > 4) {
					if (!processInfo.getPackageName().equals(Constants.MY_LITE_PACKAGE_NAME) && !processInfo.getPackageName().equals(Constants.MY_PRO_PACKAGE_NAME)) {
						activityManager.restartPackage(processInfo.getPackageName());
					}
				}
			}
			
			return true;
		}
	
		private void registryBroadcastReceiver() {
		if (mBroadcastReceiver == null) {
			mBroadcastReceiver = new IntegityBroadcastReceiver();
			IntentFilter filter = new IntentFilter();
			
			filter.addAction(Intent.ACTION_SCREEN_ON);
			filter.addAction(Intent.ACTION_SCREEN_OFF);
			
			filter.addAction(Intent.ACTION_BATTERY_CHANGED);
			filter.addAction(Intent.ACTION_POWER_CONNECTED);
			filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
			
			filter.addAction(Intent.ACTION_DATE_CHANGED);
			filter.addAction(Intent.ACTION_SHUTDOWN);
			
			filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
			filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
			
			registerReceiver(mBroadcastReceiver, filter);
		}
	}
	
	private void unregistryBroadcastReceiver() {
		if (mBroadcastReceiver != null) {
			unregisterReceiver(mBroadcastReceiver);
		}
	}
	
	private static final int ALARM_INTERVAL = 1000 * 60;		//1 Min.
	
	private void registryAlarm() {
		if (mAlarmManager == null)
			mAlarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
	
		if (mAlarmIntent == null)
			mAlarmIntent = PendingIntent.getService(mContext, 0, new Intent(SERVICE_UPDATE_ACTION), 0);
		
		mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), ALARM_INTERVAL, mAlarmIntent);
	}
	
	private void unregistryAlarm() {
		if (mAlarmManager != null)
			mAlarmManager.cancel(mAlarmIntent);
		
		if (mAlarmIntent != null)
			mAlarmIntent.cancel();
	}
	
	private PendingIntent mAlarmWifiControlIntent = null;
	private PendingIntent mAlarmBlueToothControlIntent = null;
	private PendingIntent mAlarmProcessControlIntent = null;
	
	private void registryHardwareAlarm(int delay) {
		if (mAlarmManager == null)
			mAlarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		
		switch (mCurrentOptimizingMode) {
		case BatteryServiceConstants.OPTIMIZING_AUTO_POW:
			if (ScheduleConstants.POWER_WIFI) { registryWifiControlAlarm(delay); }
			if (ScheduleConstants.POWER_BLUETOOTH) { registryBlueToothControlAlarm(delay); }
			if (ScheduleConstants.POWER_PROCESS) { registryProcessControlAlarm(delay); }
			break;
		case BatteryServiceConstants.OPTIMIZING_AUTO_MAX:
			if (ScheduleConstants.MAXIMUM_WIFI) { registryWifiControlAlarm(delay); }
			if (ScheduleConstants.MAXIMUM_BLUETOOTH) { registryBlueToothControlAlarm(delay); }
			if (ScheduleConstants.MAXIMUM_PROCESS) { registryProcessControlAlarm(delay); }
			break;
		case BatteryServiceConstants.OPTIMIZING_AUTO_GEN:
			if (ScheduleConstants.GENERAL_WIFI) { registryWifiControlAlarm(delay); }
			if (ScheduleConstants.GENERAL_BLUETOOTH) { registryBlueToothControlAlarm(delay); }
			if (ScheduleConstants.GENERAL_PROCESS) { registryProcessControlAlarm(delay); }
			break;
		case BatteryServiceConstants.OPTIMIZING_AUTO_MIN:
			if (ScheduleConstants.MINIMUM_WIFI) { registryWifiControlAlarm(delay); }
			if (ScheduleConstants.MINIMUM_BLUETOOTH) { registryBlueToothControlAlarm(delay); }
			if (ScheduleConstants.MINIMUM_PROCESS) { registryProcessControlAlarm(delay); }
			break;
		case BatteryServiceConstants.OPTIMIZING_CUST_POW:
			if (mIsWifiControl[0]) { registryWifiControlAlarm(delay); }
			if (mIsBlueToothControl[0]) { registryBlueToothControlAlarm(delay); }
			if (mIsProcessControl[0]) { registryProcessControlAlarm(delay); }
			break;
		case BatteryServiceConstants.OPTIMIZING_CUST_MAX:
			if (mIsWifiControl[1]) { registryWifiControlAlarm(delay); }
			if (mIsBlueToothControl[1]) { registryBlueToothControlAlarm(delay); }
			if (mIsProcessControl[1]) { registryProcessControlAlarm(delay); }
			break;
		case BatteryServiceConstants.OPTIMIZING_CUST_GEN:
			if (mIsWifiControl[2]) { registryWifiControlAlarm(delay); }
			if (mIsBlueToothControl[2]) { registryBlueToothControlAlarm(delay); }
			if (mIsProcessControl[2]) { registryProcessControlAlarm(delay); }
			break;
		case BatteryServiceConstants.OPTIMIZING_CUST_MIN:
			if (mIsWifiControl[3]) { registryWifiControlAlarm(delay); }
			if (mIsBlueToothControl[3]) { registryBlueToothControlAlarm(delay); }
			if (mIsProcessControl[3]) { registryProcessControlAlarm(delay); }
			break;
		}
	}
	
	private void registryWifiControlAlarm(int delay) {
		if (mAlarmManager == null)
			mAlarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
		
		if (mAlarmWifiControlIntent == null)
			mAlarmWifiControlIntent = PendingIntent.getService(mContext, 0, new Intent(SERVICE_HARDWARE_WIFI_ACTION), 0);
		
		mAlarmManager.set(AlarmManager.RTC_WAKEUP, delay * 60 * 1000, mAlarmWifiControlIntent);
	}
	
	private void registryBlueToothControlAlarm(int delay) {
		if (mAlarmManager == null)
			mAlarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
		
		if (mAlarmBlueToothControlIntent == null)
			mAlarmBlueToothControlIntent = PendingIntent.getService(mContext, 0, new Intent(SERVICE_HARDWARE_BLUETOOTH_ACTION), 0);
		
		mAlarmManager.set(AlarmManager.RTC_WAKEUP, delay * 60 * 1000, mAlarmBlueToothControlIntent);
	}
	
	private void registryProcessControlAlarm(int delay) {
		if (mAlarmManager == null)
			mAlarmManager = (AlarmManager)mContext.getSystemService(Context.ALARM_SERVICE);
		
		if (mAlarmProcessControlIntent == null)
			mAlarmProcessControlIntent = PendingIntent.getService(mContext, 0, new Intent(SERVICE_HARDWARE_PROCESS_ACTION), 0);
		
		mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + delay * 60 * 1000, delay * 60 * 1000, mAlarmBlueToothControlIntent);
	}
	
	//unregistry all control alarm
	private void unregistryHardwareAlarm() {
		if (mAlarmManager != null) {
			if (mAlarmWifiControlIntent != null) {
				mAlarmManager.cancel(mAlarmWifiControlIntent);
				mAlarmWifiControlIntent.cancel();
			}
			
			if (mAlarmBlueToothControlIntent != null) {
				mAlarmManager.cancel(mAlarmBlueToothControlIntent);
				mAlarmBlueToothControlIntent.cancel();
			}
			
			if (mAlarmProcessControlIntent != null) {
				mAlarmManager.cancel(mAlarmProcessControlIntent);
				mAlarmProcessControlIntent.cancel();
			}
		}
	}
	
	private void widgetUpdate() {
		//Widget Update.
		Intent widgetUpdate = new Intent(Constants.WIDGET_UPDATE_ACTION);
		widgetUpdate.putExtra(BatteryWidget.BATTERY_LEVEL, mBatteryLevel);
		widgetUpdate.putExtra(BatteryWidget.WIDGET_THEME, mWidgetTheme);
		sendBroadcast(widgetUpdate);
	}
	
	private void notificationUpdate() {
		BatteryNotification notification = BatteryNotification.getInstance();
		
		try {
			notification.updateNotification(mContext, null, mNotificationTheme, mBatteryLevel);
		} catch (NullPointerException e) {
			notification.setNotification(mContext);
		} finally {
			notification.updateNotification(mContext, null, mNotificationTheme, mBatteryLevel);
		}
	}
	
	private class IntegityBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			
			if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
				mBatteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 100);
				
				switch (intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, BatteryManager.BATTERY_PLUGGED_USB)) {
				case BatteryManager.BATTERY_PLUGGED_AC:
				case BatteryManager.BATTERY_PLUGGED_USB:
					mIsPowerConnected = true;
					break;
				default:
					mIsPowerConnected = false;
				}
				
				SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE).edit();
				editor.putBoolean(Constants.SERVICE_PLUG_CONNECTED, mIsPowerConnected);
				editor.commit();
				
				if (mIsScreenOn) {
					//Widget & Notification Update.
					widgetUpdate();
					if (mIsNotificationFlag) { notificationUpdate(); }
				} else {
					if (mIsManagementFlag) {
						if (!mIsPowerConnected) {
							if (mManagementType) {
								if (ScheduleConstants.MAXIMUM_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.MAXIMUM_RANGE_END) {
									if (mCurrentOptimizingMode != BatteryServiceConstants.OPTIMIZING_AUTO_MAX) {
										unregistryHardwareAlarm();
										mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_AUTO_MAX;
										registryHardwareAlarm(ScheduleConstants.MAXIMUM_DELAY);
									}
								} else if (ScheduleConstants.GENERAL_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.GENERAL_RANGE_END) {
									if (mCurrentOptimizingMode != BatteryServiceConstants.OPTIMIZING_AUTO_GEN) {
										unregistryHardwareAlarm();
										mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_AUTO_GEN;
										registryHardwareAlarm(ScheduleConstants.GENERAL_DELAY);
									}
								} else if (ScheduleConstants.MINIMUM_RANGE_START <= mBatteryLevel && mBatteryLevel <= ScheduleConstants.MINIMUM_RANGE_END) {
									if (mCurrentOptimizingMode != BatteryServiceConstants.OPTIMIZING_AUTO_MIN) {
										unregistryHardwareAlarm();
										mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_AUTO_MIN;
										registryHardwareAlarm(ScheduleConstants.MINIMUM_DELAY);
									}
								}
							} else {
								if (mMaximumRange[0] < mBatteryLevel && mBatteryLevel <= mMaximumRange[1]) {
									if (mCurrentOptimizingMode != BatteryServiceConstants.OPTIMIZING_CUST_MAX) {
										unregistryHardwareAlarm();
										mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_CUST_MAX;
										registryHardwareAlarm(ScheduleConstants.MAXIMUM_DELAY);
									}
								} else if (mGeneralRange[0] < mBatteryLevel && mBatteryLevel <= mGeneralRange[1]) {
									if (mCurrentOptimizingMode != BatteryServiceConstants.OPTIMIZING_CUST_MAX) {
										unregistryHardwareAlarm();
										mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_CUST_GEN;
										registryHardwareAlarm(ScheduleConstants.MAXIMUM_DELAY);
									}
								} else if (mMinimumRange[0] < mBatteryLevel && mBatteryLevel <= mMinimumRange[1]) {
									if (mCurrentOptimizingMode != BatteryServiceConstants.OPTIMIZING_CUST_MAX) {
										unregistryHardwareAlarm();
										mCurrentOptimizingMode = BatteryServiceConstants.OPTIMIZING_CUST_MIN;
										registryHardwareAlarm(ScheduleConstants.MAXIMUM_DELAY);
									}
								}
							}
						}
					}
				}
				
				if (mIsTrackingFlag) {
					//Record Battery Information into DB.
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.TRACKING_DATE_FORMAT);
//					int extras = TrackingData.EXTRAS_POWER_DISCONNECTED;
//					
//					if (mIsPowerConnected) {
//						extras = TrackingData.EXTRAS_POWER_CONNECTED;
//					} else {
//						extras = TrackingData.EXTRAS_POWER_DISCONNECTED;
//					}
					
					Util.saveTrackingData(mContext, new TrackingData(simpleDateFormat.format(System.currentTimeMillis()), mBatteryLevel, intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_UNKNOWN)));
				}
				
				int batteryStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_CHARGING);
				
				if (mIsAlertFullFlag && !mBatteryNotificationDuplicationFlag) {
					if (mBatteryLevel >= 99 && (batteryStatus == BatteryManager.BATTERY_STATUS_CHARGING || batteryStatus == BatteryManager.BATTERY_STATUS_FULL)) {
						//완충알림
						mBatteryNotificationDuplicationFlag = true;
						
						Intent batteryStateNotifyIntent = new Intent(mContext, BatteryStateNotifyDialog.class);
						batteryStateNotifyIntent.putExtra(BatteryStateNotifyDialog.DIALOG_TYPE, BatteryStateNotifyDialog.DIALOG_TYPE_BATTERY_FULL);
						batteryStateNotifyIntent.putExtra(BatteryStateNotifyDialog.DIALOG_THEME, mApplicationTheme);
						
						PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, batteryStateNotifyIntent, PendingIntent.FLAG_ONE_SHOT);
						try {
							pendingIntent.send();
						} catch (CanceledException e) {
							// TODO Auto-generated catch block
						}
					}
				}
				
				if (mIsAlertLowFlag && !mBatteryNotificationDuplicationFlag) {
					if (mBatteryLevel <= mBatteryLowLevel && batteryStatus == BatteryManager.BATTERY_STATUS_DISCHARGING) {
						//부족알림
						mBatteryNotificationDuplicationFlag = true;
						
						Intent batteryStateNotifyIntent = new Intent(mContext, BatteryStateNotifyDialog.class);
						batteryStateNotifyIntent.putExtra(BatteryStateNotifyDialog.DIALOG_TYPE, BatteryStateNotifyDialog.DIALOG_TYPE_BATTERY_LOW);
						batteryStateNotifyIntent.putExtra(BatteryStateNotifyDialog.DIALOG_THEME, mApplicationTheme);
						
						PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, batteryStateNotifyIntent, PendingIntent.FLAG_ONE_SHOT);
						try {
							pendingIntent.send();
						} catch (CanceledException e) {
							// TODO Auto-generated catch block
						}
					}
				}
			} else if (action.equals(Intent.ACTION_POWER_CONNECTED)) {
				mIsPowerConnected = true;
				mBatteryNotificationDuplicationFlag = false;
				
				if (mIsScreenOn) {
					if (mManagementType) {	//AUTO
						if (ScheduleConstants.POWER_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_POW); }
					} else {				//CUSTOM
						if (mIsScreenControl[0]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_POW); }
					}
				}
				
				if (mIsTrackingFlag) {
					//Record Battery Information into DB.
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.TRACKING_DATE_FORMAT);
					Util.saveTrackingData(mContext, new TrackingData(simpleDateFormat.format(System.currentTimeMillis()), mBatteryLevel, intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_UNKNOWN)));
				}
			} else if (action.equals(Intent.ACTION_POWER_DISCONNECTED)) {
				mIsPowerConnected = false;
				mBatteryNotificationDuplicationFlag = false;
				
				if (mIsScreenOn) {
					if (mManagementType) {
						if (ScheduleConstants.MAXIMUM_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.MAXIMUM_RANGE_END) {
							if (ScheduleConstants.MAXIMUM_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_MAX); }
						} else if (ScheduleConstants.GENERAL_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.GENERAL_RANGE_END) {
							if (ScheduleConstants.GENERAL_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_GEN); }
						} else if (ScheduleConstants.MINIMUM_RANGE_START <= mBatteryLevel && mBatteryLevel <= ScheduleConstants.MINIMUM_RANGE_END) {
							if (ScheduleConstants.MINIMUM_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_MIN); }
						}
					} else {
						if (mMaximumRange[0] < mBatteryLevel && mBatteryLevel <= mMaximumRange[1]) {
							if (mIsScreenControl[1]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_MAX); }
						} else if (mGeneralRange[0] < mBatteryLevel && mBatteryLevel <= mGeneralRange[1]) {
							if (mIsScreenControl[2]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_GEN); }
						} else if (mMinimumRange[0] <= mBatteryLevel && mBatteryLevel <= mMinimumRange[1]) {
							if (mIsScreenControl[3]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_MIN); }
						}
					}
				}
				
				if (mIsTrackingFlag) {
					//Record Battery Information into DB.
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.TRACKING_DATE_FORMAT);
					Util.saveTrackingData(mContext, new TrackingData(simpleDateFormat.format(System.currentTimeMillis()), mBatteryLevel, intent.getIntExtra(BatteryManager.EXTRA_STATUS, BatteryManager.BATTERY_STATUS_UNKNOWN)));
				}
			} else if (action.equals(Intent.ACTION_SCREEN_ON)) {
				mIsScreenOn = true;
				//Widget & Notification Update
				widgetUpdate();
				if (mIsNotificationFlag) { notificationUpdate(); }
				
				//Screen Brightness Control
				if (mIsManagementFlag) {
					if (mIsPowerConnected) {
						if (mManagementType) {	//AUTO
							if (ScheduleConstants.POWER_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_POW); }
						} else {				//CUSTOM
							if (mIsScreenControl[0]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_POW); }
						}
					} else {
						if (mManagementType) {
							if (ScheduleConstants.MAXIMUM_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.MAXIMUM_RANGE_END) {
								if (ScheduleConstants.MAXIMUM_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_MAX); }
							} else if (ScheduleConstants.GENERAL_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.GENERAL_RANGE_END) {
								if (ScheduleConstants.GENERAL_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_GEN); }
							} else if (ScheduleConstants.MINIMUM_RANGE_START <= mBatteryLevel && mBatteryLevel <= ScheduleConstants.MINIMUM_RANGE_END) {
								if (ScheduleConstants.MINIMUM_SCREEN) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_AUTO_MIN); }
							}
						} else {
							if (mMaximumRange[0] < mBatteryLevel && mBatteryLevel <= mMaximumRange[1]) {
								if (mIsScreenControl[1]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_MAX); }
							} else if (mGeneralRange[0] < mBatteryLevel && mBatteryLevel <= mGeneralRange[1]) {
								if (mIsScreenControl[2]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_GEN); }
							} else if (mMinimumRange[0] <= mBatteryLevel && mBatteryLevel <= mMinimumRange[1]) {
								if (mIsScreenControl[3]) { brightnessControl(BatteryServiceConstants.BRIGHTNESS_CUST_MIN); }
							}
						}
					}
				}
				
				//Unregistry Hardware Control Alarm
				unregistryHardwareAlarm();
			} else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
				mIsScreenOn = false;
				//Registry Hardware Control Alarm
				if (mIsManagementFlag) {
					if (mIsPowerConnected) {
						if (mManagementType) {	//AUTO
							registryHardwareAlarm(ScheduleConstants.POWER_DELAY);
						} else {				//CUSTOM
							registryHardwareAlarm(mServiceDelay[0]);
						}
					} else {
						if (mManagementType) {
							if (ScheduleConstants.MAXIMUM_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.MAXIMUM_RANGE_END) {
								registryHardwareAlarm(ScheduleConstants.MAXIMUM_DELAY);
							} else if (ScheduleConstants.GENERAL_RANGE_START < mBatteryLevel && mBatteryLevel <= ScheduleConstants.GENERAL_RANGE_END) {
								registryHardwareAlarm(ScheduleConstants.GENERAL_DELAY);
							} else if (ScheduleConstants.MINIMUM_RANGE_START <= mBatteryLevel && mBatteryLevel <= ScheduleConstants.MINIMUM_RANGE_END) {
								registryHardwareAlarm(ScheduleConstants.MINIMUM_DELAY);
							}
						} else {
							if (mMaximumRange[0] < mBatteryLevel && mBatteryLevel <= mMaximumRange[1]) {
								registryHardwareAlarm(mServiceDelay[1]);
							} else if (mGeneralRange[0] < mBatteryLevel && mBatteryLevel <= mGeneralRange[1]) {
								registryHardwareAlarm(mServiceDelay[2]);
							} else if (mMinimumRange[0] <= mBatteryLevel && mBatteryLevel <= mMinimumRange[1]) {
								registryHardwareAlarm(mServiceDelay[3]);
							}
						}
					}
				}
			} else if (action.equals(Intent.ACTION_DATE_CHANGED)) {
				//Record Each Usage into DB
				SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
				
				long networkRxUsage = pref.getLong(Constants.RECORD_NETWORK_RX_USAGE, 0);
				long networkTxUsage = pref.getLong(Constants.RECORD_NETWORK_TX_USAGE, 0);
				long callRxUsage = pref.getLong(Constants.RECORD_CALL_RX_USAGE, 0);
				long callTxUsage = pref.getLong(Constants.RECORD_CALL_TX_USAGE, 0);
				long smsRxUsage = pref.getLong(Constants.RECORD_SMS_RX_USAGE, 0);
				long smsTxUsage = pref.getLong(Constants.RECORD_SMS_TX_USAGE, 0);
				
				SharedPreferences.Editor editor = pref.edit();
				
				if (pref.getBoolean(Constants.DEVICE_TERMINATED_FLAG, false)) {
					networkRxUsage += pref.getLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, 0);
					networkTxUsage += pref.getLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, 0);
					
					editor.putBoolean(Constants.DEVICE_TERMINATED_FLAG, false);
					editor.putLong(Constants.RECORD_NETWORK_RX_BEFORE_USAGE, pref.getLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, 0));
					editor.putLong(Constants.RECORD_NETWORK_TX_BEFORE_USAGE, pref.getLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, 0));
				} else {
					long beforeRx = pref.getLong(Constants.RECORD_NETWORK_RX_BEFORE_USAGE, 0);
					long beforeTx = pref.getLong(Constants.RECORD_NETWORK_TX_BEFORE_USAGE, 0);
					
					editor.putLong(Constants.RECORD_NETWORK_RX_BEFORE_USAGE, beforeRx + networkRxUsage);
					editor.putLong(Constants.RECORD_NETWORK_TX_BEFORE_USAGE, beforeTx + networkTxUsage);
				}
				
				editor.putLong(Constants.RECORD_NETWORK_RX_USAGE, 0);
				editor.putLong(Constants.RECORD_NETWORK_TX_USAGE, 0);
				editor.putLong(Constants.RECORD_CALL_RX_USAGE, 0);
				editor.putLong(Constants.RECORD_CALL_TX_USAGE, 0);
				editor.putLong(Constants.RECORD_SMS_RX_USAGE, 0);
				editor.putLong(Constants.RECORD_SMS_TX_USAGE, 0);
				
				editor.putLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, 0);
				editor.putLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, 0);
				
				editor.commit();
				
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.AMOUNT_DATE_FORMAT);
				String date = simpleDateFormat.format(System.currentTimeMillis() - ((((23 * 60) + 59) * 60) + 59) * 1000);
				
				UsageData[] data = new UsageData[3];
				data[0] = new UsageData(date, UsageData.DATA_TYPE_NETWORK, networkRxUsage, networkTxUsage);
				data[1] = new UsageData(date, UsageData.DATA_TYPE_CALL, callRxUsage, callTxUsage);
				data[2] = new UsageData(date, UsageData.DATA_TYPE_SMS, smsRxUsage, smsTxUsage);
				
				Util.saveUsageData(context, data);
			} else if (action.equals(BluetoothDevice.ACTION_ACL_CONNECTED)) {
				//BlueTooth Flag Control
				mIsBlueToothConnected = true;
				SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE).edit();
				editor.putBoolean(Constants.SERVICE_BLUETOOTH_CONNECTED, mIsBlueToothConnected);
				editor.commit();
			} else if (action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)) {
				//BlueTooth Flag Control
				mIsBlueToothConnected = false;
				SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE).edit();
				editor.putBoolean(Constants.SERVICE_BLUETOOTH_CONNECTED, mIsBlueToothConnected);
				editor.commit();
			} else if (action.equals(Intent.ACTION_SHUTDOWN)) {
				//Record Each Usage into DB
				Util.setNetworkUsage(context);
				Util.setCallUsage(context);
				Util.setSMSUsage(context);
				
				SharedPreferences.Editor editor = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE).edit();
				editor.putBoolean(Constants.DEVICE_TERMINATED_FLAG, true);
				editor.putLong(Constants.DEVICE_TERMINATED_TIME, System.currentTimeMillis());
				editor.commit();
			}
		}
	}
}
