package madcat.studio.service;

import madcat.studio.battery.pro.MainActivity;
import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class BatteryNotification {
	private static BatteryNotification mBatteryNotification = new BatteryNotification();
	
	private static NotificationManager mNotificationManager = null;
	private static Notification mNotification = null;
	private static Intent mIntent = null;
	private static PendingIntent mPendingIntent = null;
	
	private BatteryNotification() { }
	
	public static BatteryNotification getInstance() {
		return mBatteryNotification;
	}
	
	public void setNotification(Context context) {
		if (mNotificationManager == null) {
//			mNotificationManager.cancel(Constants.NOTIFICATION_ID);
			mNotificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
			mIntent = new Intent(context, MainActivity.class);
			mIntent.putExtra(MainActivity.ACTIVITY_MENU_FLAG, MainActivity.ACTIVITY_MENU_BATTERY);
			mPendingIntent = PendingIntent.getActivity(context, 0, mIntent, 0);
		}
	}

	private static final String PREFIX_NOTIFICATION = "notification_";
	private static final String[] MIDFIX_THEME_ARRAY = {"rectangle_", "pisces_" ,"aries_", "taurus_", "capricorn_", "aquarius_", "gemini_", "cancer_", "leo_", "virgo_", "libra_", "scorpio_", "archer_", "circle_"};
	
	public void updateNotification(Context context, String msg, int notificationTheme, int batteryPercent) {
		int notificationDrawableId = 0;
		
		if (batteryPercent == 0) {
			batteryPercent = 1;
		}
		
		notificationDrawableId = context.getResources().getIdentifier(PREFIX_NOTIFICATION + MIDFIX_THEME_ARRAY[notificationTheme] + batteryPercent, "drawable", context.getPackageName());
		
		mNotification = new Notification(notificationDrawableId, null, System.currentTimeMillis());
		mNotification.flags = Notification.FLAG_ONGOING_EVENT;
		
		mNotification.setLatestEventInfo(context, context.getString(R.string.app_name) + context.getString(R.string.prefix_notification_title), context.getString(R.string.prefix_notification_content) + batteryPercent + "%", mPendingIntent);
		mNotificationManager.notify(Constants.NOTIFICATION_ID, mNotification);
	}
	
	
	public void unsetNotification() {
		if (mNotificationManager != null) {
			mNotificationManager.cancel(Constants.NOTIFICATION_ID);
		}
	}
}
