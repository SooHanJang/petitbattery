package madcat.studio.widget;

import madcat.studio.battery.pro.MainActivity;
import madcat.studio.constants.Constants;
import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;

public class BatteryWidget extends AppWidgetProvider {
	public static final String BATTERY_LEVEL = "battery_level";
	public static final String WIDGET_THEME  = "widget_theme";
	
	private static int mBatteryPercent = 0;
	private static int mWidgetTheme = Constants.WIDGET_THEME_BASIC;
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		super.onReceive(context, intent);
		
		String action = intent.getAction();
		
		if (action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
			SharedPreferences pref = context.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
			mBatteryPercent = pref.getInt(Constants.RECORD_BATTERY_LEVEL, 100);
			mWidgetTheme = pref.getInt(Constants.WIDGET_THEME, Constants.WIDGET_THEME_BASIC);
			updateWidget(context);
		} else if (action.equals(Constants.WIDGET_UPDATE_ACTION)) {
			mBatteryPercent = intent.getIntExtra(BATTERY_LEVEL, 100);
			mWidgetTheme = intent.getIntExtra(WIDGET_THEME, Constants.WIDGET_THEME_BASIC);
			updateWidget(context);
		} else if (action.equals(Constants.WIDGET_THEME_ACTION)) {
			mBatteryPercent = intent.getIntExtra(BATTERY_LEVEL, 100);
			mWidgetTheme = intent.getIntExtra(WIDGET_THEME, Constants.WIDGET_THEME_BASIC);
			updateWidget(context);
		}
	}
	
	private void updateWidget(Context context) {
		ComponentName thisAppWidget = new ComponentName(context.getPackageName(), BatteryWidget.class.getName());
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
		
		onUpdate(context, appWidgetManager, appWidgetIds);
	}
	
	@Override	//widget 등록
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		for(int i=0; i < appWidgetIds.length; i++) {
			int appwidgetId = appWidgetIds[i];
			updateAppWidget(context, appWidgetManager, appwidgetId);		// 위젯을 업데이트 하는 메소드
		}
	}
	
	private static final String PREFIX_WIDGET	= "widget_";
	private static final String POSTFIX_LAYOUT	= "_layout";
	private static final String POSTFIX_TEXT	= "_text";
	
	//THEME RESOURCE POSTFIX
	private static final String[] THEME_ARRAY = {"basic_", "pisces_" ,"aries_", "taurus_", "capricorn_", "aquarius_", "gemini_", "cancer_", "leo_", "virgo_", "libra_", "scorpio_", "archer_"};
	
	public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
		int identifierInteger = 0;
		switch (mBatteryPercent) {
		case 0:
			identifierInteger = 1;
			break;
		case 100:
			identifierInteger = 99;
			break;
		default:
			identifierInteger = mBatteryPercent;
		}
		identifierInteger /= 20;
		
		String identifierString = PREFIX_WIDGET + THEME_ARRAY[mWidgetTheme] + identifierInteger;
		
		String packageName = context.getPackageName();
		
		int updateLayoutId = context.getResources().getIdentifier(identifierString, "layout", packageName);
		int updateWidgetLayoutId = context.getResources().getIdentifier(identifierString + POSTFIX_LAYOUT, "id", packageName);
		int updateWidgetTextId = context.getResources().getIdentifier(identifierString + POSTFIX_TEXT, "id", packageName);

		RemoteViews updateViews = new RemoteViews(packageName, updateLayoutId);
		updateViews.setTextViewText(updateWidgetTextId, mBatteryPercent + "%");
		
		Intent intent = new Intent(context, MainActivity.class);
//		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
		
		updateViews.setOnClickPendingIntent(updateWidgetLayoutId, pendingIntent);
		updateViews.setOnClickPendingIntent(updateWidgetTextId, pendingIntent);
		
		appWidgetManager.updateAppWidget(appWidgetId, updateViews);		// 위젯 업데이트 매니저 호출
	}
}
