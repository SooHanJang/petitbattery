package madcat.studio.adapter;

import android.graphics.drawable.Drawable;

public class ProcessInfo implements Comparable<ProcessInfo> {
	public static final int SYSTEM	= 0;
	public static final int LAUNCHER = 1;
	public static final int IMPORTANT = 2;
	public static final int SERVICE = 3;
	public static final int NOT_IMPORTANT = 4;
	public static final int UNKNOWN = 5;
	public static final int PERCEPTIBLE = 6;
	
	private String mLabel = null;
	private Drawable mIcon = null;
	
	private String mPackageName = null;
	private int mPid = 0;
	private int mLru = 0;
	private int mCategory = 0;
	private long mMemUsage = 0;

	private boolean mKill = false;
	
	public ProcessInfo() {
		// TODO Auto-generated constructor stub
	}
	
//	public ProcessItem(String label, Drawable icon, int pid, int lru, int category, int cpuUsage, int memUsage) {
//		mLabel = label;
//		mIcon = icon;
//		mPid = pid;
//		mLru = lru;
//		mCategory = category;
//		mCpuUsage = cpuUsage;
//		mMemUsage = memUsage;
//	}
	
	public void setLabel(String label) {
		mLabel = label;
	}
	
	public void setIcon(Drawable icon) {
		mIcon = icon;
	}
	
	public void setPackageName(String packageName) {
		mPackageName = packageName;
	}
	
	public void setPid(int pid) {
		mPid = pid;
	}
	
	public void setLru(int lru) {
		mLru = lru;
	}
	
	public void setCategory(int category) {
		mCategory = category;
	}
	
	public void setMemUsage(int memUsage) {
		mMemUsage = memUsage * 1024;
	}
	
	public String getLabel() {
		return mLabel;
	}
	
	public Drawable getIcon() {
		return mIcon;
	}
	
	public String getPackageName() {
		return mPackageName;
	}
	
	public int getPid() {
		return mPid;
	}
	
	public int getLru() {
		return mLru;
	}
	
	public int getCategory() {
		return mCategory;
	}
	
	public long getMemUsage() {
		return mMemUsage;
	}
	
	public void setKill(boolean kill) {
		mKill = kill;
	}
	
	public boolean isKill() {
		return mKill;
	}
	
	@Override
	public int compareTo(ProcessInfo another) {
		// TODO Auto-generated method stub
		if (mMemUsage > another.getMemUsage()) {
			return 1;
		} else if (mMemUsage < another.getMemUsage()) {
			return -1;
		} else {
			if (mPid > another.getPid()) {
				return 1;
			} else if (mPid < another.getPid()) {
				return -1;
			}
		}
		
		return 0;
	}
}
