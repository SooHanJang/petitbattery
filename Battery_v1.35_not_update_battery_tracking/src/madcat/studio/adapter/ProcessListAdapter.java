package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.utils.Util;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ProcessListAdapter extends ArrayAdapter<ProcessInfo> {
	private Context mContext = null;
	private int mLayout = 0;
	private ArrayList<ProcessInfo> mProcessInfoList = null;
	
	private ImageView mIconView = null;
	private TextView mLabelView = null;
	private TextView mCategoryView = null;
	private TextView mUsageView = null;
	private CheckBox mCheckBox = null;
	
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	public ProcessListAdapter(Context context, int layout, int applicationTheme, ArrayList<ProcessInfo> items) {
		// TODO Auto-generated constructor stub
		super(context, layout, items);
		
		mContext = context;
		mLayout = layout;
		mApplicationTheme = applicationTheme;
		mProcessInfoList = new ArrayList<ProcessInfo>(items);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		final int selected = position;
		
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(mLayout, null);
		}
		
		ProcessInfo mProcessItem = mProcessInfoList.get(position);
		
		if (mProcessItem != null) {
			mIconView = (ImageView)view.findViewById(R.id.layout_process_list_row_icon);
			mLabelView = (TextView)view.findViewById(R.id.layout_process_list_row_label);
			mCategoryView = (TextView)view.findViewById(R.id.layout_process_list_row_category);
			mUsageView = (TextView)view.findViewById(R.id.layout_process_list_row_memory_usage);
			mCheckBox = (CheckBox)view.findViewById(R.id.layout_process_list_row_check);
			
			switch (mApplicationTheme) {
//			case Constants.APPLICATION_THEME_CAPRICORN:
//				
//				break;
//			case Constants.APPLICATION_THEME_AQUARIUS:
//				
//				break;
			case Constants.APPLICATION_THEME_PISCES:
				mLabelView.setTextColor(mContext.getResources().getColor(R.color.pisces_blue));
				mUsageView.setTextColor(mContext.getResources().getColor(R.color.pisces_blue));
				mCheckBox.setBackgroundResource(R.drawable.com_checkbox_selector_pisces);
				break;
			case Constants.APPLICATION_THEME_ARIES:
				mLabelView.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
				mUsageView.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
				mCheckBox.setBackgroundResource(R.drawable.com_checkbox_selector_aries);
				break;
			case Constants.APPLICATION_THEME_TAURUS:
				mLabelView.setTextColor(mContext.getResources().getColor(R.color.taurus_brown));
				mUsageView.setTextColor(mContext.getResources().getColor(R.color.taurus_brown));
				mCheckBox.setBackgroundResource(R.drawable.com_checkbox_selector_taurus);
				break;
//			case Constants.APPLICATION_THEME_GEMINI:
//				
//				break;
//			case Constants.APPLICATION_THEME_CANCER:
//				
//				break;
//			case Constants.APPLICATION_THEME_LEO:
//				
//				break;
//			case Constants.APPLICATION_THEME_VIRGO:
//				
//				break;
//			case Constants.APPLICATION_THEME_LIBRA:
//				
//				break;
//			case Constants.APPLICATION_THEME_SCORPIO:
//				
//				break;
//			case Constants.APPLICATION_THEME_ARCHER:
//				
//				break;
			}
			
			mCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					// TODO Auto-generated method stub
					mProcessInfoList.get(selected).setKill(isChecked);
				}
			});
			
			if (mIconView != null)
				mIconView.setBackgroundDrawable(mProcessItem.getIcon());
			
			if (mLabelView != null)
				mLabelView.setText(mProcessItem.getLabel());
			
			mCategoryView.setText(mContext.getString(R.string.prefix_category));
			if (mCategoryView != null) {
				switch (mProcessItem.getCategory()) {
				case ProcessInfo.SYSTEM:
					mCategoryView.setTextColor(Color.RED);
					mCategoryView.append(mContext.getString(R.string.postfix_category_system));
					break;
				case ProcessInfo.LAUNCHER:
					mCategoryView.setTextColor(Color.RED);
					mCategoryView.append(mContext.getString(R.string.postfix_category_launcher));
					break;
				case ProcessInfo.IMPORTANT:
					mCategoryView.setTextColor(Color.RED);
					mCategoryView.append(mContext.getString(R.string.postfix_category_important));
					break;
				case ProcessInfo.SERVICE:
					mCategoryView.setTextColor(Color.BLUE);
					mCategoryView.append(mContext.getString(R.string.postfix_category_service));
					break;
				case ProcessInfo.PERCEPTIBLE:
					mCategoryView.setTextColor(Color.BLUE);
					mCategoryView.append(mContext.getString(R.string.postfix_category_perceptible));
					break;
				case ProcessInfo.NOT_IMPORTANT:
					mCategoryView.setTextColor(Color.GREEN);
					mCategoryView.append(mContext.getString(R.string.postfix_category_not_important));
					break;
				case ProcessInfo.UNKNOWN:
				default:
					mCategoryView.setTextColor(Color.BLACK);
					mCategoryView.append(mContext.getString(R.string.postfix_category_unknown));
					break;
				}
			}
			
			if (mUsageView != null)
				mUsageView.setText(mContext.getString(R.string.prefix_memory) + Util.dataFormatSize(mProcessItem.getMemUsage(), true));
			
			mCheckBox.setChecked(mProcessInfoList.get(position).isKill());
			
			return view;
		}
		
		return null;
	}
	
	public void setProcessItemList(ArrayList<ProcessInfo> processInfoList) {
		if (mProcessInfoList != null) {
			mProcessInfoList.clear();
			mProcessInfoList = null;
		}
		
		mProcessInfoList = new ArrayList<ProcessInfo>(processInfoList);
	}
	
	public boolean selectedProcessKill(ActivityManager am) {
		boolean isKill = false;
		long deallocatedMemorySize = 0;
		
		for (ProcessInfo processInfo : mProcessInfoList) {
			if (processInfo.isKill()) {
				if (processInfo.getPackageName().equals(Constants.MY_LITE_PACKAGE_NAME) || processInfo.getPackageName().equals(Constants.MY_PRO_PACKAGE_NAME)) {
					processInfo.setKill(false);
				} else {
					deallocatedMemorySize += processInfo.getMemUsage();
					am.restartPackage(processInfo.getPackageName());
					isKill = true;
				}
			}
		}
		
		if (isKill)
			showToast(mContext.getString(R.string.prefix_deallocated) + Util.dataFormatSize(deallocatedMemorySize, true));
		else
			showToast(mContext.getString(R.string.process_no_checked_process));
		
		return isKill;
	}
	
	public boolean autoProcessKill(ActivityManager am) {
		boolean isKill = false;
		long deallocatedMemorySize = 0;
		
		for (ProcessInfo processInfo : mProcessInfoList) {
			if (processInfo.getCategory() == ProcessInfo.NOT_IMPORTANT || processInfo.getCategory() == ProcessInfo.UNKNOWN || processInfo.getLru() > 4) {
				if (!processInfo.getPackageName().equals(Constants.MY_LITE_PACKAGE_NAME) && !processInfo.getPackageName().equals(Constants.MY_PRO_PACKAGE_NAME)) {
					deallocatedMemorySize += processInfo.getMemUsage();
					am.restartPackage(processInfo.getPackageName());
					isKill = true;
				}
			}
		}
		
		if (isKill)
			showToast(mContext.getString(R.string.prefix_deallocated) + Util.dataFormatSize(deallocatedMemorySize, true));
		else
			showToast(mContext.getString(R.string.process_no_detected_useless_process));
		
		return isKill;
	}
	
	private void showToast(String msg) {
		Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
	}
}
