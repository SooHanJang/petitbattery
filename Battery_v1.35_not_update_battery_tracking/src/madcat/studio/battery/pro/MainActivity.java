package madcat.studio.battery.pro;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import madcat.studio.constants.Constants;
import madcat.studio.dialogs.ApplicationHelpDialog;
import madcat.studio.dialogs.ApplicationThemeChooser;
import madcat.studio.layouts.BatteryLayout;
import madcat.studio.layouts.ProcessLayout;
import madcat.studio.layouts.SettingLayout;
import madcat.studio.layouts.SystemLayout;
import madcat.studio.service.IntegrityService;
import madcat.studio.utils.LoadDatabase;
import madcat.studio.utils.UsageData;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

public class MainActivity extends Activity {
	public static final String	ACTIVITY_MENU_FLAG		= "activity_menu_flag";
	public static final int		ACTIVITY_MENU_BATTERY	= 0;
	public static final int		ACTIVITY_MENU_PROCESS	= 1;
	public static final int		ACTIVITY_MENU_SYSTEM	= 2;
	public static final int		ACTIVITY_MENU_SETTING	= 3;
	
	private Context mContext = null;
	
	private int mCurrentMenu = ACTIVITY_MENU_BATTERY;
	
	private LinearLayout mMainLayout = null;
	
	private RadioGroup mMenuGroup = null;
	private RadioButton mBatteryMenuGroup = null;
	private RadioButton mProcessMenuGroup = null;
	private RadioButton mSystemMenuGroup = null;
	private RadioButton mSettingMenuGroup = null;
	
	private LinearLayout mMenuDisplayLayout = null;
	
	private LinearLayout.LayoutParams mLayoutParams;
	
	private BatteryLayout mBatteryLayout = null;
	private ProcessLayout mProcessLayout = null;
	private SystemLayout mSystemLayout = null;
	private SettingLayout mSettingLayout = null;
	
	private SharedPreferences mPref = null;
	
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	private PhoneStatusBroadcastReceiver broadcastReceiver = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		mContext = this;
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		mMainLayout = (LinearLayout)findViewById(R.id.activity_main_layout);
		
		mMenuGroup = (RadioGroup)findViewById(R.id.activity_main_menu_group);
		mBatteryMenuGroup = (RadioButton)findViewById(R.id.activity_main_menu_battery);
		mProcessMenuGroup = (RadioButton)findViewById(R.id.activity_main_menu_process);
		mSystemMenuGroup = (RadioButton)findViewById(R.id.activity_main_menu_system);
		mSettingMenuGroup = (RadioButton)findViewById(R.id.activity_main_menu_setting);
		
		mMenuDisplayLayout = (LinearLayout)findViewById(R.id.activity_main_menu_display_layout);

		mPref = getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mApplicationTheme = mPref.getInt(Constants.APPLICATION_THEME, Constants.APPLICATION_THEME_BASIC);
		
		mLayoutParams = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1);
		
		mBatteryLayout = new BatteryLayout(this, mApplicationTheme);
		mProcessLayout = new ProcessLayout(this, mApplicationTheme);
		mSystemLayout = new SystemLayout(this, mApplicationTheme);
		mSettingLayout = new SettingLayout(this, mApplicationTheme);
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mMainLayout.setBackgroundResource(R.drawable.background_pisces);
			mBatteryMenuGroup.setBackgroundResource(R.drawable.com_menu_battery_pisces);
			mProcessMenuGroup.setBackgroundResource(R.drawable.com_menu_process_pisces);
			mSystemMenuGroup.setBackgroundResource(R.drawable.com_menu_system_pisces);
			mSettingMenuGroup.setBackgroundResource(R.drawable.com_menu_setting_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mMainLayout.setBackgroundResource(R.drawable.background_aries);
			mBatteryMenuGroup.setBackgroundResource(R.drawable.com_menu_battery_aries);
			mProcessMenuGroup.setBackgroundResource(R.drawable.com_menu_process_aries);
			mSystemMenuGroup.setBackgroundResource(R.drawable.com_menu_system_aries);
			mSettingMenuGroup.setBackgroundResource(R.drawable.com_menu_setting_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mMainLayout.setBackgroundResource(R.drawable.background_taurus);
			mBatteryMenuGroup.setBackgroundResource(R.drawable.com_menu_battery_taurus);
			mProcessMenuGroup.setBackgroundResource(R.drawable.com_menu_process_taurus);
			mSystemMenuGroup.setBackgroundResource(R.drawable.com_menu_system_taurus);
			mSettingMenuGroup.setBackgroundResource(R.drawable.com_menu_setting_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		if (mPref.getBoolean(Constants.FIRST_EXECUTION, true) || mPref.getBoolean(Constants.UPDATE_EXECUTON_2012_06_27, true)) {
			//First Execution
			SharedPreferences.Editor editor = mPref.edit();
			
			//IDENTIFING DEVICE MANUFACTURER.
			String manufacturer = Build.MANUFACTURER;
			String model = Build.MODEL;
			
			manufacturer = manufacturer.toLowerCase();
			
			if (manufacturer.equals("samsung")) {
				if (model.toLowerCase().equals("shw-m100s")) {
					editor.putInt(Constants.DEVICE_MANUFACTURER_TYPE, Constants.DEVICE_MANUFACTURER_SAMSUNG_A);
				} else {
					editor.putInt(Constants.DEVICE_MANUFACTURER_TYPE, Constants.DEVICE_MANUFACTURER_SAMSUNG);
				}
			} else if (manufacturer.equals("lg")) {
//				editor.putInt(Constants.DEVICE_MANUFACTURER_TYPE, Constants.DEVICE_MANUFACTURER_LG);
				editor.putInt(Constants.DEVICE_MANUFACTURER_TYPE, Constants.DEVICE_MANUFACTURER_COMMON);
			} else {
				editor.putInt(Constants.DEVICE_MANUFACTURER_TYPE, Constants.DEVICE_MANUFACTURER_COMMON);
			}
			
			editor.putLong(Constants.RECORD_NETWORK_RX_USAGE, 0);
    		editor.putLong(Constants.RECORD_NETWORK_TX_USAGE, 0);
    		editor.putLong(Constants.RECORD_CALL_RX_USAGE, 0);
    		editor.putLong(Constants.RECORD_CALL_TX_USAGE, 0);
    		editor.putLong(Constants.RECORD_SMS_RX_USAGE, 0);
    		editor.putLong(Constants.RECORD_SMS_TX_USAGE, 0);
    		
    		editor.putBoolean(Constants.OPTIMIZATION_EXECUTE, true);
    		editor.putBoolean(Constants.NOTIFICATION_EXECUTE, true);
    		editor.putBoolean(Constants.OPTIMIZATION_TYPE, Constants.OPTIMIZATION_TYPE_AUTO);
    		
    		editor.putBoolean(Constants.BATTERY_NOTIFICATION_ALERT_FULL, true);
    		editor.putBoolean(Constants.BATTERY_NOTIFICATION_ALERT_LOW, true);
    		
    		editor.putInt(Constants.BATTERY_NOTIFICATION_FULL_ALERT_TYPE, Constants.BATTERY_NOTIFICATION_ALERT_VIBRATE);
    		editor.putInt(Constants.BATTERY_NOTIFICATION_LOW_ALERT_TYPE, Constants.BATTERY_NOTIFICATION_ALERT_VIBRATE);
    		
    		editor.putBoolean(Constants.BATTERY_TRACKING_EXECUTE, true);
    		
    		editor.putInt(Constants.APPLICATION_THEME, Constants.APPLICATION_THEME_BASIC);
    		editor.putInt(Constants.WIDGET_THEME, Constants.WIDGET_THEME_BASIC);
    		editor.putInt(Constants.NOTIFICATION_THEME, Constants.NOTIFICATION_THEME_BASIC_RECTANGLE);
    		
			editor.putLong(Constants.RECORD_NETWORK_RX_BEFORE_USAGE, TrafficStats.getMobileRxBytes());
			editor.putLong(Constants.RECORD_NETWORK_TX_BEFORE_USAGE, TrafficStats.getMobileTxBytes());
    		
			editor.putInt(Constants.USAGE_LIMIT_RESET_DATE, 1);
			editor.putLong(Constants.USAGE_LIMIT_NETWOKR, Constants.USAGE_LIMIT_DEFAULT_VALUE);
			editor.putLong(Constants.USAGE_LIMIT_CALL, Constants.USAGE_LIMIT_DEFAULT_VALUE);
			editor.putLong(Constants.USAGE_LIMIT_SMS, Constants.USAGE_LIMIT_DEFAULT_VALUE);
			
    		editor.commit();
			
			new MakeDatabaseTask().execute();
		} else {
			switch (getIntent().getIntExtra(ACTIVITY_MENU_FLAG, ACTIVITY_MENU_BATTERY)) {
			case ACTIVITY_MENU_BATTERY:
				mBatteryLayout.setPhoneUsage();
				mMenuDisplayLayout.addView(mBatteryLayout, mLayoutParams);
				mCurrentMenu = ACTIVITY_MENU_BATTERY;
				break;
			case ACTIVITY_MENU_PROCESS:
				mProcessLayout.startProcessLoadTask();
				mMenuDisplayLayout.addView(mProcessLayout, mLayoutParams);
				mCurrentMenu = ACTIVITY_MENU_PROCESS;
				break;
			case ACTIVITY_MENU_SYSTEM:
				mSystemLayout.initialize();
				mMenuDisplayLayout.addView(mSystemLayout, mLayoutParams);
				mCurrentMenu = ACTIVITY_MENU_SYSTEM;
				break;
			case ACTIVITY_MENU_SETTING:
				mMenuDisplayLayout.addView(mSettingLayout, mLayoutParams);
				mCurrentMenu = ACTIVITY_MENU_SETTING;
				break;
			}
		}
		
		mMenuGroup.setOnCheckedChangeListener(new RadioButtonEventHandler());
		
		if (broadcastReceiver == null) {
			broadcastReceiver = new PhoneStatusBroadcastReceiver();
			registerReceiver(broadcastReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		}
		
		startService(new Intent(IntegrityService.SERVICE_EXCUTE_ACTION));
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		if (broadcastReceiver != null)
			unregisterReceiver(broadcastReceiver);
		
		super.onDestroy();
		finish();
	}
	
	private class RadioButtonEventHandler implements OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			switch (mCurrentMenu) {
			case ACTIVITY_MENU_BATTERY:
				mMenuDisplayLayout.removeView(mBatteryLayout);
				break;
			case ACTIVITY_MENU_PROCESS:
				mProcessLayout.stopProcessLoadTask();
				mMenuDisplayLayout.removeView(mProcessLayout);
				break;
			case ACTIVITY_MENU_SYSTEM:
				mSystemLayout.stopResourceWatcher();
				mMenuDisplayLayout.removeView(mSystemLayout);
				break;
			case ACTIVITY_MENU_SETTING:
				mMenuDisplayLayout.removeView(mSettingLayout);
				break;
			}
			
			switch (checkedId) {
			case R.id.activity_main_menu_battery:
				mBatteryLayout.setPhoneUsage();
				mMenuDisplayLayout.addView(mBatteryLayout, mLayoutParams);
				mCurrentMenu = ACTIVITY_MENU_BATTERY;
				break;
			case R.id.activity_main_menu_process:
				mProcessLayout.startProcessLoadTask();
				mMenuDisplayLayout.addView(mProcessLayout, mLayoutParams);
				mCurrentMenu = ACTIVITY_MENU_PROCESS;
				break;
			case R.id.activity_main_menu_system:
				mSystemLayout.initialize();
				mMenuDisplayLayout.addView(mSystemLayout, mLayoutParams);
				mCurrentMenu = ACTIVITY_MENU_SYSTEM;
				break;
			case R.id.activity_main_menu_setting:
				mMenuDisplayLayout.addView(mSettingLayout, mLayoutParams);
				mCurrentMenu = ACTIVITY_MENU_SETTING;
				break;
			}
		}
	}
	
	private class PhoneStatusBroadcastReceiver extends BroadcastReceiver {
		private int batteryLevel = 0;
		private int batteryHealth = 0;
		private int batteryStatus = 0;
		private int batteryPlugged = 0;
		private int batteryVoltage = 0;
		private int batteryTemperature = 0;
		private String batteryTechnology = null;
		
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			
			if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
				batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, - 1) * 100 / intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
				batteryHealth = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, -1);
				batteryStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
				batteryPlugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
				batteryVoltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1);
				batteryTemperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1);
				batteryTechnology = intent.getStringExtra(BatteryManager.EXTRA_TECHNOLOGY);
				
				SharedPreferences.Editor editor = mPref.edit();
				editor.putInt(Constants.RECORD_BATTERY_LEVEL, batteryLevel);
				editor.commit();
				
				mBatteryLayout.setIndicatorLevel(batteryLevel);
				
				if (batteryStatus == BatteryManager.BATTERY_STATUS_DISCHARGING) {
					mBatteryLayout.setChargeState(BatteryLayout.STATE_DISCHARGE);
				} else if (batteryStatus == BatteryManager.BATTERY_STATUS_CHARGING) {
					if (batteryPlugged == BatteryManager.BATTERY_PLUGGED_AC) {
						mBatteryLayout.setChargeState(BatteryLayout.STATE_CHARGE_AC);
					} else if (batteryPlugged == BatteryManager.BATTERY_PLUGGED_USB) {
						mBatteryLayout.setChargeState(BatteryLayout.STATE_CHARGE_USB);
					}
				} else {
					mBatteryLayout.setChargeState(BatteryLayout.STATE_DISCHARGE);
				}
				
				mSystemLayout.setBatteryInfo(batteryLevel, batteryHealth, batteryStatus, batteryPlugged, batteryVoltage, batteryTemperature, batteryTechnology);
			}
		}
	}
	
	private static final String CALL_RECEIVED = "1";
	private static final String CALL_TRANSMITED = "2";
	private static final String[] CALL_PROJECTION = {CallLog.Calls.TYPE, CallLog.Calls.DATE, CallLog.Calls.DURATION};
	
	private static final Uri SMS_RECEIVED_URL = Uri.parse("content://sms/inbox");
	private static final Uri SMS_TRANSMITED_URL = Uri.parse("content://sms/sent");
	private static final Uri SMS_URI_SAMSUNG_GALAXY_A = Uri.parse("content://com.btb.sec.mms.provider/message");
	private static final Uri SMS_URI_SAMSUNG = Uri.parse("content://com.sec.mms.provider/message");
//	private static final Uri SMS_URI_LG = Uri.parse("content://com.lge.messageprovider/msg");
	
//	private static final Uri SMS_RECEIVED_URI_LG = Uri.parse("content://com.lge.messageprovider/msg/inbox");
//	private static final Uri SMS_TRANSMITED_URI_LG = Uri.parse("content://com.lge.messageprovider/msg/outbox");
	
	private static final String[] SMS_PROJECTION = {"date"};
	private static final String[] SMS_PROJECTION_SAMSUNG = {"RegTime","MainType"};
//	private static final String[] SMS_PROJECTION_LG = {};
	
	private class MakeDatabaseTask extends AsyncTask<Void, Void, Void> {
		boolean first = true;
		boolean update = true;
		
		private boolean dbFlag = false;
		private boolean trackDbFlag = false;
		private ProgressDialog progressDialog = null;
		
		@Override
		protected void onPreExecute() {
			first = mPref.getBoolean(Constants.FIRST_EXECUTION, true);
			update = mPref.getBoolean(Constants.UPDATE_EXECUTON_2012_06_27, true);
			
			if (first && update) {
				progressDialog = ProgressDialog.show(mContext, "", mContext.getString(R.string.database_progress_msg));
			} else {
				if (first) {
					progressDialog = ProgressDialog.show(mContext, "", mContext.getString(R.string.database_progress_msg));
				}
				
				if (update) {
					progressDialog = ProgressDialog.show(mContext, "", mContext.getString(R.string.database_update_msg));
				}
			}
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
//			Log.d(TAG, "DB Create Start");
			
			if (first && update) {
				dbFlag = LoadDatabase.loadDataBase(getResources().getAssets());
				if (dbFlag) {
					saveDataBeforeApplicationInstall();
				} else {
//					Log.d(TAG, "DB Create Fail");
				}
			} else {
				if (first) {
					dbFlag = LoadDatabase.loadDataBase(getResources().getAssets());
					if (dbFlag) {
						saveDataBeforeApplicationInstall();
					} else {
//						Log.d(TAG, "DB Create Fail");
					}
				}
				
				if (update) {
					//Load All Data.
					ArrayList<UsageData> previousDatas = Util.loadAllDataUsage(mContext);
					
					//Delete Exist Database
					Util.deletePreviousDatabase(mContext);
					
					//Create New Database
					trackDbFlag = LoadDatabase.loadDataBase(getResources().getAssets());
					
					//Insert Load Data into New DB
					Util.saveUsageData(mContext, previousDatas);
					
					if (trackDbFlag) {
//						Log.d(TAG, "DB Create Success");
					} else {
//						Log.d(TAG, "DB Create Fail");					
					}
				}
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			
			SharedPreferences.Editor editor = mPref.edit();
			
			if (dbFlag) {
				editor.putBoolean(Constants.FIRST_EXECUTION, false);
				editor.putBoolean(Constants.UPDATE_EXECUTON_2012_06_27, false);
				
				ApplicationThemeChooser themeChooser = new ApplicationThemeChooser(mContext, R.style.PopupDialog, mApplicationTheme);
				themeChooser.setFirstExecutionFlag(true);
				themeChooser.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						Intent service = new Intent(IntegrityService.SERVICE_UPDATE_ACTION);
						stopService(service);
						startService(service);
						ApplicationHelpDialog applicationHelpDialog = new ApplicationHelpDialog(mContext, R.style.PopupDialog, mApplicationTheme);
						applicationHelpDialog.show();
					}
				});
				themeChooser.show();
				Toast.makeText(mContext, mContext.getString(R.string.initialize_theme_select_msg), Toast.LENGTH_SHORT).show();
			}
			
			if (trackDbFlag) {
				editor.putBoolean(Constants.UPDATE_EXECUTON_2012_06_27, false);
			}
			
			if (dbFlag || trackDbFlag) {
				editor.commit();
			}
			
			publishProgress();
		}
		
		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			mBatteryLayout.setPhoneUsage();
			mMenuDisplayLayout.addView(mBatteryLayout, mLayoutParams);
			mCurrentMenu = ACTIVITY_MENU_BATTERY;
		}
		
		//어플리케이션 설치 전의 Call/SMS 데이터 불러와서 DB에 저장.
		private void saveDataBeforeApplicationInstall() {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.AMOUNT_DATE_FORMAT);
			LinkedHashMap<String, UsageData> dataHashMap = new LinkedHashMap<String, UsageData>();
			UsageData data = null;

			String today = simpleDateFormat.format(new Date(System.currentTimeMillis()));
			
			//Get Call Data
			Cursor cursor = mContext.getContentResolver().query(CallLog.Calls.CONTENT_URI, CALL_PROJECTION, null, null, CallLog.Calls.DEFAULT_SORT_ORDER);
			
			String type = null;
			String date = null;
			long duration = 0;
			
			if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
				do {
					type = cursor.getString(0);
					date = simpleDateFormat.format(new Date(cursor.getLong(1)));
					duration = cursor.getLong(2);
					
					if (!today.equals(date)) {
						if (dataHashMap.containsKey(date)) {
							data = new UsageData(dataHashMap.get(date));
							if (type.equals(CALL_RECEIVED)) {
								data.setRxData(data.getRxData() + duration);
							} else if (type.equals(CALL_TRANSMITED)) {
								data.setTxData(data.getTxData() + duration);
							}
							dataHashMap.remove(date);
						} else {
							data = new UsageData();
							data.setType(UsageData.DATA_TYPE_CALL);
							data.setDate(date);
							if (type.equals(CALL_RECEIVED)) {
								data.setRxData(duration);
							} else if (type.equals(CALL_TRANSMITED)) {
								data.setTxData(duration);
							}
						}
						
						dataHashMap.put(date, new UsageData(data));
						data = null;
					}
				} while (cursor.moveToNext());
			}
			
			UsageData[] callDataArray = new UsageData[dataHashMap.values().size()];
			callDataArray = dataHashMap.values().toArray(callDataArray);
			
			cursor.close();
			dataHashMap.clear();
			
			//Get SMS Data
			
			switch (mPref.getInt(Constants.DEVICE_MANUFACTURER_TYPE, 0)) {
			case Constants.DEVICE_MANUFACTURER_COMMON:
				cursor = mContext.getContentResolver().query(SMS_RECEIVED_URL, SMS_PROJECTION, null, null, null);
				
				if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
					do {
						date = simpleDateFormat.format(new Date(cursor.getLong(0)));
						
						if (!today.equals(date)) {
							if (dataHashMap.containsKey(date)) {
								data = new UsageData(dataHashMap.get(date));
								data.setRxData(data.getRxData() + 1);
								dataHashMap.remove(date);
							} else {
								data = new UsageData();
								data.setType(UsageData.DATA_TYPE_SMS);
								data.setDate(date);
								data.setRxData(data.getRxData() + 1);
							}
							
							dataHashMap.put(date, new UsageData(data));
							data = null;
						}
					} while (cursor.moveToNext());
				}
				
				cursor.close();
				
				cursor = mContext.getContentResolver().query(SMS_TRANSMITED_URL, SMS_PROJECTION, null, null, null);
				
				if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
					do {
						date = simpleDateFormat.format(new Date(cursor.getLong(0)));
						
						if (!today.equals(date)) {
							if (dataHashMap.containsKey(date)) {
								data = new UsageData(dataHashMap.get(date));
								data.setTxData(data.getTxData() + 1);
								dataHashMap.remove(date);
							} else {
								data = new UsageData();
								data.setType(UsageData.DATA_TYPE_SMS);
								data.setDate(date);
								data.setTxData(data.getTxData() + 1);
							}
							
							dataHashMap.put(date, new UsageData(data));
							data = null;
						}
					} while (cursor.moveToNext());
				}
				
				cursor.close();
				break;
			case Constants.DEVICE_MANUFACTURER_SAMSUNG_A:
				cursor = mContext.getContentResolver().query(SMS_URI_SAMSUNG_GALAXY_A, SMS_PROJECTION_SAMSUNG, null, null, null);
				
				if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
					do {
						date = simpleDateFormat.format(new Date(cursor.getLong(0)));
						
						if (!today.equals(date)) {
							switch (cursor.getInt(1)) {
							case 0:
								if (dataHashMap.containsKey(date)) {
									data = new UsageData(dataHashMap.get(date));
									data.setRxData(data.getRxData() + 1);
									dataHashMap.remove(date);
								} else {
									data = new UsageData();
									data.setType(UsageData.DATA_TYPE_SMS);
									data.setDate(date);
									data.setRxData(data.getRxData() + 1);
								}
								break;
							case 1:
								if (dataHashMap.containsKey(date)) {
									data = new UsageData(dataHashMap.get(date));
									data.setTxData(data.getTxData() + 1);
									dataHashMap.remove(date);
								} else {
									data = new UsageData();
									data.setType(UsageData.DATA_TYPE_SMS);
									data.setDate(date);
									data.setTxData(data.getTxData() + 1);
								}
								break;
							}
							
							dataHashMap.put(date, new UsageData(data));
							data = null;
						}
					} while (cursor.moveToNext());
				}
				
				cursor.close();
				break;
			case Constants.DEVICE_MANUFACTURER_SAMSUNG:
				cursor = mContext.getContentResolver().query(SMS_URI_SAMSUNG, SMS_PROJECTION_SAMSUNG, null, null, null);
				
				if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
					do {
						date = simpleDateFormat.format(new Date(cursor.getLong(0)));
						
						if (!today.equals(date)) {
							switch (cursor.getInt(1)) {
							case 0:
								if (dataHashMap.containsKey(date)) {
									data = new UsageData(dataHashMap.get(date));
									data.setRxData(data.getRxData() + 1);
									dataHashMap.remove(date);
								} else {
									data = new UsageData();
									data.setType(UsageData.DATA_TYPE_SMS);
									data.setDate(date);
									data.setRxData(data.getRxData() + 1);
								}
								break;
							case 1:
								if (dataHashMap.containsKey(date)) {
									data = new UsageData(dataHashMap.get(date));
									data.setTxData(data.getTxData() + 1);
									dataHashMap.remove(date);
								} else {
									data = new UsageData();
									data.setType(UsageData.DATA_TYPE_SMS);
									data.setDate(date);
									data.setTxData(data.getTxData() + 1);
								}
								break;
							}
							
							dataHashMap.put(date, new UsageData(data));
							data = null;
						}
					} while (cursor.moveToNext());
				}
				
				cursor.close();
				break;
			case Constants.DEVICE_MANUFACTURER_LG:
				//For LG
				break;
			}
			
			UsageData[] smsData = new UsageData[dataHashMap.values().size()];
			smsData = dataHashMap.values().toArray(smsData);

			Util.saveUsageData(mContext, callDataArray);
			Util.saveUsageData(mContext, smsData);
			
			dataHashMap.clear();
		}
	}
}
