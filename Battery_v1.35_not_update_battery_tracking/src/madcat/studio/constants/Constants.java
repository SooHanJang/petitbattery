package madcat.studio.constants;

public class Constants {
	//AD CONSTANTS.
	public static final String 	AD_UNIT_ID = "a14f338de792fd5";
	
	//PREFERENCE CONSTANTS.
	public static final String	PREFERENCE_NAME								= "manager_preference";
	
	public static final String	HELP_DIALOG_SHOW							= "true";
	
	//SERVICE PREFERENCE CONSTANTS.
	public static final String	FIRST_EXECUTION								= "first_execution";
	public static final String	UPDATE_EXECUTON_2012_06_27					= "update_20120627";
	
	public static final String	OPTIMIZATION_EXECUTE						= "optimization_excute";
	public static final String	OPTIMIZATION_TYPE							= "optimization_type";
	public static final boolean	OPTIMIZATION_TYPE_AUTO						= true;
	public static final boolean	OPTIMIZATION_TYPE_CUSTOM					= false;
	
	public static final String	NOTIFICATION_EXECUTE						= "notification_excute";
	
	public static final String	BATTERY_NOTIFICATION_ALERT_FULL					= "battery_notification_full";
	public static final String	BATTERY_NOTIFICATION_ALERT_LOW					= "battery_notificaiton_low";
	
	public static final String	BATTERY_NOTIFICATION_FULL_ALERT_TYPE		= "battery_alert_full_type";
	public static final String	BATTERY_NOTIFICATION_LOW_ALERT_TYPE			= "battery_alert_low_type";
	public static final String	BATTERY_NOTIFICATION_LOW_ALERT_LEVEL		= "battery_alert_low_level";
	
//	public static final String	CHARGE_ALERT_FULL_SOUND						= "battery_alert_full_sound_kind";
//	public static final String	CHARGE_ALERT_LOW_SOUND						= "battery_alert_low_sound_kind";
	
	public static final int		BATTERY_NOTIFICATION_ALERT_VIBRATE			= 0;
	public static final int		BATTERY_NOTIFICATION_ALERT_SOUND			= 1;
	public static final int		BATTERY_NOTIFICATION_ALERT_BOTH				= 2;
	
	public static final String	BATTERY_TRACKING_EXECUTE					= "battery_tracking_execute";
	
	public static final String	APPLICATION_THEME							= "application_theme";
	public static final int		APPLICATION_THEME_BASIC						= 0;
	public static final int		APPLICATION_THEME_PISCES					= 1;
	public static final int		APPLICATION_THEME_ARIES						= 2;
	public static final int		APPLICATION_THEME_TAURUS					= 3;
	public static final int		APPLICATION_THEME_CAPRICORN					= 4;
	public static final int		APPLICATION_THEME_AQUARIUS					= 5;
	public static final int		APPLICATION_THEME_GEMINI					= 6;
	public static final int		APPLICATION_THEME_CANCER					= 7;
	public static final int		APPLICATION_THEME_LEO						= 8;
	public static final int		APPLICATION_THEME_VIRGO						= 9;
	public static final int		APPLICATION_THEME_LIBRA						= 10;
	public static final int		APPLICATION_THEME_SCORPIO					= 11;
	public static final int		APPLICATION_THEME_ARCHER					= 12;

	public static final String	WIDGET_THEME								= "widget_theme";
	public static final int		WIDGET_THEME_BASIC							= 0;
	public static final int		WIDGET_THEME_PISCES							= 1;
	public static final int		WIDGET_THEME_ARIES							= 2;
	public static final int		WIDGET_THEME_TAURUS							= 3;
	public static final int		WIDGET_THEME_CAPRICORN						= 4;
	public static final int		WIDGET_THEME_AQUARIUS						= 5;
	public static final int		WIDGET_THEME_GEMINI							= 6;
	public static final int		WIDGET_THEME_CANCER							= 7;
	public static final int		WIDGET_THEME_LEO							= 8;
	public static final int		WIDGET_THEME_VIRGO							= 9;
	public static final int		WIDGET_THEME_LIBRA							= 10;
	public static final int		WIDGET_THEME_SCORPIO						= 11;
	public static final int		WIDGET_THEME_ARCHER							= 12;
	
	public static final String	NOTIFICATION_THEME							= "notification_theme";
	public static final int		NOTIFICATION_THEME_BASIC_RECTANGLE			= 0;
	public static final int		NOTIFICATION_THEME_PISCES					= 1;
	public static final int		NOTIFICATION_THEME_ARIES					= 2;
	public static final int		NOTIFICATION_THEME_TAURUS					= 3;
	public static final int		NOTIFICATION_THEME_CAPRICORN				= 4;
	public static final int		NOTIFICATION_THEME_AQUARIUS					= 5;
	public static final int		NOTIFICATION_THEME_GEMINI					= 6;
	public static final int		NOTIFICATION_THEME_CANCER					= 7;
	public static final int		NOTIFICATION_THEME_LEO						= 8;
	public static final int		NOTIFICATION_THEME_VIRGO					= 9;
	public static final int		NOTIFICATION_THEME_LIBRA					= 10;
	public static final int		NOTIFICATION_THEME_SCORPIO					= 11;
	public static final int		NOTIFICATION_THEME_ARCHER					= 12;
	public static final int		NOTIFICATION_THEME_BASIC_CIRCLE				= 13;
	
	public static final String	DEVICE_TERMINATED_FLAG						= "device_terminated_flag";
	public static final String	DEVICE_TERMINATED_TIME						= "device_terminated_time";
	
//	public static final int		CHARGE_ALERT_SOUND_NONE						= 0;
//	public static final int		CHARGE_ALERT_SOUND_1						= 1;
//	public static final int		CHARGE_ALERT_SOUND_2						= 2;
//	public static final int		CHARGE_ALERT_SOUND_3						= 3;
//	public static final int		CHARGE_ALERT_SOUND_4						= 4;
	
	
	//DATA VALUES.
	public static final String	RECORD_CALL_RX_USAGE						= "record_call_rx";
	public static final String	RECORD_CALL_TX_USAGE						= "record_call_tx";
	public static final String	RECORD_SMS_RX_USAGE							= "record_sms_rx";
	public static final String	RECORD_SMS_TX_USAGE							= "record_sms_tx";
	
	public static final String	RECORD_NETWORK_RX_BEFORE_USAGE				= "record_network_rx_before";					
	public static final String	RECORD_NETWORK_TX_BEFORE_USAGE				= "record_network_tx_before";
	public static final String	RECORD_NETWORK_RX_USAGE						= "record_network_rx";
	public static final String	RECORD_NETWORK_TX_USAGE						= "record_network_tx";
	public static final String	RECORD_TEMP_NETWORK_RX_USAGE				= "record_temp_network_rx";
	public static final String	RECORD_TEMP_NETWORK_TX_USAGE				= "record_temp_network_tx";
	
	public static final String	RECORD_BATTERY_LEVEL						= "record_battery_level";
	
	public static final String	SERVICE_PLUG_CONNECTED						= "service_plug_connected";
	public static final String	SERVICE_BLUETOOTH_CONNECTED					= "service_bluetooth_connected";
	
	public static final String	USAGE_LIMIT_RESET_DATE						= "usage_limit_reset_date";
	public static final String	USAGE_LIMIT_NETWOKR							= "usage_limit_network";
	public static final String	USAGE_LIMIT_CALL							= "usage_limit_call";
	public static final String	USAGE_LIMIT_SMS								= "usage_limit_sms";
	
	//UNLIMITED VALUE.
	public static final int		USAGE_LIMIT_DEFAULT_DATE					= 1;
	public static final int		USAGE_LIMIT_DEFAULT_VALUE					= 0;
	
	//SERVICE ACTION.
	public static final int		NOTIFICATION_ID								= 0x800088;
	
	//DATABASE CONSTANTS
	public static final String ROOT_DIR										= "/data/data/madcat.studio.battery.pro/databases/";
	
	public static final String DATABASE_NAME								= "BatteryDb.db";
	public static final String DATABASE_TABLE_NAME_AMOUNT					= "amount";
	public static final String DATABASE_ATTR_DATE_AMOUNT					= "_date";
	public static final String DATABASE_ATTR_TYPE_AMOUNT					= "_type";
	public static final String DATABASE_ATTR_RX_AMOUNT						= "_r_amt";
	public static final String DATABASE_ATTR_TX_AMOUNT						= "_s_amt";
	public static final String DATABASE_ATTR_RX_SUM_AMOUNT					= "_sum_r_amt";
	public static final String DATABASE_ATTR_TX_SUM_AMOUNT					= "_sum_s_amt";
	
	public static final String DATABASE_TABLE_NAME_TRACKER					= "tracker";
	public static final String DATABASE_ATTR_DATE_TRACKER					= "_date";
	public static final String DATABASE_ATTR_LEVEL_TRACKER					= "_levels";
	public static final String DATABASE_ATTR_STATUS_TRACKER					= "_status";
	public static final String DATABASE_ATTR_EXTRAS_TRACKER					= "_extras";
	
	//WIDGET CONSTANTS
	public static final String WIDGET_UPDATE_ACTION = "android.battery.widget.pro.update";
	public static final String WIDGET_THEME_ACTION = "android.battery.widget.pro.theme";
	
	//UTIL CONSTANTS
	public static final String DATA_SIZE_BYTE = "Byte";
	public static final String DATA_SIZE_KB = "KB";
	public static final String DATA_SIZE_MB = "MB";
	public static final String DATA_SIZE_GB = "GB";
	
	public static final String CALL_SIZE_SECOND_KO = "초";
	public static final String CALL_SIZE_MINUTE_KO = "분";
	public static final String CALL_SIZE_HOUR_KO = "시간";
	public static final String CALL_SIZE_SECOND_EN = "s";
	public static final String CALL_SIZE_MINUTE_EN = "m";
	public static final String CALL_SIZE_HOUR_EN = "h";
	
	public static final String SMS_SIZE_KO = "건";
	public static final String SMS_SIZE_EN = "ea";
	
	public static final String AMOUNT_DATE_FORMAT	= "yyyyMMdd";
	public static final String TRACKING_DATE_FORMAT = "yyyyMMddHHmmss";
	
	public static final String	DEVICE_MANUFACTURER_TYPE		= "device_manufacturer_type";
	public static final int		DEVICE_MANUFACTURER_COMMON		= 0;
	public static final int		DEVICE_MANUFACTURER_SAMSUNG_A	= 1;
	public static final int		DEVICE_MANUFACTURER_SAMSUNG		= 2;
	public static final int		DEVICE_MANUFACTURER_LG			= 3;
	
	//PACKAGE NAME
	public static final String MY_LITE_PACKAGE_NAME = "madcat.studio.battery.free";
	public static final String MY_PRO_PACKAGE_NAME = "madcat.studio.battery.pro";
}
