package madcat.studio.boot;

import java.sql.Date;
import java.text.SimpleDateFormat;

import madcat.studio.constants.Constants;
import madcat.studio.service.IntegrityService;
import madcat.studio.utils.UsageData;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class ServiceAutoStarter extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			SharedPreferences pref = context.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
			SharedPreferences.Editor editor = pref.edit();
			
			Date terminatedDate = new Date(pref.getLong(Constants.DEVICE_TERMINATED_TIME, 0));
			Date currentDate = new Date(System.currentTimeMillis());
			
			if (terminatedDate.getYear() == currentDate.getYear() && terminatedDate.getMonth() == currentDate.getMonth() && terminatedDate.getDate() == currentDate.getDate()) {
				long networkRxUsage = pref.getLong(Constants.RECORD_NETWORK_RX_USAGE, 0);
				long networkTxUsage = pref.getLong(Constants.RECORD_NETWORK_TX_USAGE, 0);
				
				if (pref.getBoolean(Constants.DEVICE_TERMINATED_FLAG, false)) {
					networkRxUsage += pref.getLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, 0);
					networkTxUsage += pref.getLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, 0);
				} else {
					editor.putBoolean(Constants.DEVICE_TERMINATED_FLAG, true);
				}
				
				editor.putLong(Constants.RECORD_NETWORK_RX_USAGE, networkRxUsage);
				editor.putLong(Constants.RECORD_NETWORK_TX_USAGE, networkTxUsage);
				editor.putLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, 0);
				editor.putLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, 0);
				editor.commit();
				
				context.startService(new Intent(IntegrityService.SERVICE_EXCUTE_ACTION));
			} else {
				long networkRxUsage = pref.getLong(Constants.RECORD_NETWORK_RX_USAGE, 0);
				long networkTxUsage = pref.getLong(Constants.RECORD_NETWORK_TX_USAGE, 0);
				long callRxUsage = pref.getLong(Constants.RECORD_CALL_RX_USAGE, 0);
				long callTxUsage = pref.getLong(Constants.RECORD_CALL_TX_USAGE, 0);
				long smsRxUsage = pref.getLong(Constants.RECORD_SMS_RX_USAGE, 0);
				long smsTxUsage = pref.getLong(Constants.RECORD_SMS_TX_USAGE, 0);
				
				networkRxUsage += pref.getLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, 0);
				networkTxUsage += pref.getLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, 0);
				
				editor.putBoolean(Constants.DEVICE_TERMINATED_FLAG, false);
				editor.putLong(Constants.RECORD_NETWORK_RX_BEFORE_USAGE, pref.getLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, 0));
				editor.putLong(Constants.RECORD_NETWORK_TX_BEFORE_USAGE, pref.getLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, 0));
				
				editor.putLong(Constants.RECORD_NETWORK_RX_USAGE, 0);
				editor.putLong(Constants.RECORD_NETWORK_TX_USAGE, 0);
				editor.putLong(Constants.RECORD_CALL_RX_USAGE, 0);
				editor.putLong(Constants.RECORD_CALL_TX_USAGE, 0);
				editor.putLong(Constants.RECORD_SMS_RX_USAGE, 0);
				editor.putLong(Constants.RECORD_SMS_TX_USAGE, 0);
				
				editor.putLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, 0);
				editor.putLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, 0);
				
				editor.commit();
				
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.AMOUNT_DATE_FORMAT);
				String date = simpleDateFormat.format(terminatedDate);
				
				UsageData[] data = new UsageData[3];
				data[0] = new UsageData(date, UsageData.DATA_TYPE_NETWORK, networkRxUsage, networkTxUsage);
				data[1] = new UsageData(date, UsageData.DATA_TYPE_CALL, callRxUsage, callTxUsage);
				data[2] = new UsageData(date, UsageData.DATA_TYPE_SMS, smsRxUsage, smsTxUsage);
				
				Util.saveUsageData(context, data);
				context.startService(new Intent(IntegrityService.SERVICE_EXCUTE_ACTION));
			}
		}
	}
}
