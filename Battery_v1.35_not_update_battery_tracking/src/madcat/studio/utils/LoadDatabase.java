package madcat.studio.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import madcat.studio.constants.Constants;
import android.content.res.AssetManager;

/**
 * 
 * @author Dane 2011.05.26
 * 
 * 
 *
 */
public class LoadDatabase {
//	private static final String TAG = "LOAD DATABASE";
	public static boolean loadDataBase(AssetManager assetManager) {
		boolean loadFlag = true;
		
		BufferedInputStream bis = null;
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		
		try {
			File location = new File(Constants.ROOT_DIR);
			location.mkdirs();
			File f = new File(Constants.ROOT_DIR + Constants.DATABASE_NAME);
			if(f.exists()) {
				f.delete();
				f.createNewFile();
			}
			
			bis = new BufferedInputStream(assetManager.open(Constants.DATABASE_NAME));
			fos = new FileOutputStream(f);
			bos = new BufferedOutputStream(fos);
			
			int read = -1;
			byte[] buffer = new byte[1024];
			
			while((read = bis.read(buffer, 0, 1024)) != -1)
				bos.write(buffer, 0, read);

			bos.flush();
		} catch (Exception e) {
			loadFlag = false;
		} finally {
			try {
				if (bis != null) {
					bis.close();
				}
			} catch(Exception e) {}
			try {
				if(fos != null) {
					fos.close();
				}
			} catch(Exception e) {}
			try {
				if(bos != null) {
					bos.close();
				}
			} catch(Exception e) {}
			
			bis = null;
			fos = null;
			bos = null;
		}
		
		return loadFlag;
    }
	
	public static boolean deleteDatabase() {
		boolean deleteFlag = false;
		
		return deleteFlag;
	}
}
