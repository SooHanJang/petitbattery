package madcat.studio.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;

import madcat.studio.adapter.ProcessInfo;
import madcat.studio.constants.Constants;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.TrafficStats;
import android.net.Uri;
import android.provider.CallLog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

public class Util {
	private static boolean isToday(Date date) {
		Date today = new Date(new Date().getTime() - 60 * 1000);
		
		if (today.getYear() == date.getYear() && today.getMonth() == date.getMonth() && today.getDate() == date.getDate())
			return true;
		else
			return false;
	}
	
	public static void setNetworkUsage(Context context) {
		SharedPreferences pref = context.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		
		if (pref.getBoolean(Constants.DEVICE_TERMINATED_FLAG, false)) {
			editor.putLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, TrafficStats.getMobileRxBytes());
			editor.putLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, TrafficStats.getMobileTxBytes());
		} else {
			long beforeNetworkRx = pref.getLong(Constants.RECORD_NETWORK_RX_BEFORE_USAGE, 0);
			long beforeNetworkTx = pref.getLong(Constants.RECORD_NETWORK_TX_BEFORE_USAGE, 0);
			
			editor.putLong(Constants.RECORD_NETWORK_RX_USAGE, TrafficStats.getMobileRxBytes() - beforeNetworkRx);
			editor.putLong(Constants.RECORD_NETWORK_TX_USAGE, TrafficStats.getMobileTxBytes() - beforeNetworkTx);
		}
		
		editor.commit();
	}

	private static final String CALL_RECEIVED = "1";
	private static final String CALL_TRANSMITED = "2";
	private static final String[] CALL_PROJECTION = {CallLog.Calls.TYPE, CallLog.Calls.DATE, CallLog.Calls.DURATION};
	
	public static void setCallUsage(Context context) {
		SharedPreferences pref = context.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		
		long callRxUsage = 0;
		long callTxUsage = 0;
		
		Cursor cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, CALL_PROJECTION, null, null, CallLog.Calls.DEFAULT_SORT_ORDER);
		
		String type = null;
		Date date = null;
		long duration = 0;
		
		if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
			do {
				type = cursor.getString(0);
				date = new Date(cursor.getLong(1));
				duration = cursor.getLong(2);
				
				if (isToday(date)) {
					if (type.equals(CALL_RECEIVED)) {
						callRxUsage += duration;
					} else if (type.equals(CALL_TRANSMITED)) {
						callTxUsage += duration;
					}
				}
			} while (cursor.moveToNext());
		}
		
		editor.putLong(Constants.RECORD_CALL_RX_USAGE, callRxUsage);
		editor.putLong(Constants.RECORD_CALL_TX_USAGE, callTxUsage);
		
		editor.commit();
	}
	
	private static final Uri SMS_RECEIVED_URI = Uri.parse("content://sms/inbox");
	private static final Uri SMS_TRANSMITED_URI = Uri.parse("content://sms/sent");
	private static final Uri SMS_URI_SAMSUNG_GALAXY_A = Uri.parse("content://com.btb.sec.mms.provider/message");
	private static final Uri SMS_URI_SAMSUNG = Uri.parse("content://com.sec.mms.provider/message");
//	private static final Uri SMS_URI_LG = Uri.parse("content://com.lge.messageprovider/msg");
//	private static final Uri SMS_RECEIVED_URI_LG = Uri.parse("content://com.lge.messageprovider/msg/inbox");
//	private static final Uri SMS_TRANSMITED_URI_LG = Uri.parse("content://com.lge.messageprovider/msg/outbox");
	
	private static final String[] SMS_PROJECTION = {"date"};
	private static final String[] SMS_PROJECTION_SAMSUNG = {"RegTime","MainType"};
//	private static final String[] SMS_PROJECTION_LG = {};
	
	public static void setSMSUsage(Context context) {
		SharedPreferences pref = context.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = pref.edit();
		
		int smsRxUsage = 0;
		int smsTxUsage = 0;
		
		Cursor cursor = null;
		
		switch (pref.getInt(Constants.DEVICE_MANUFACTURER_TYPE, 0)) {
		case 0:	//Common Device
			cursor = context.getContentResolver().query(SMS_RECEIVED_URI, SMS_PROJECTION, null, null, null);
			if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
				do {
					if (isToday(new Date(cursor.getLong(0))))
						smsRxUsage++;
				} while (cursor.moveToNext());
			}
			
			cursor = context.getContentResolver().query(SMS_TRANSMITED_URI, SMS_PROJECTION, null, null, null);
			
			if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
				do {
					if (isToday(new Date(cursor.getLong(0))))
						smsTxUsage++;
				} while (cursor.moveToNext());
			}
			break;
		case 1:	//Samsung Galaxy A
			cursor = context.getContentResolver().query(SMS_URI_SAMSUNG_GALAXY_A, SMS_PROJECTION_SAMSUNG, null, null, null);
			if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
				do {
					if (isToday(new Date(cursor.getLong(0)))) {
						switch (cursor.getInt(1)) {
						case 0:	//RECEIVED SMS
							smsRxUsage++;
							break;
						case 1:	//TRANSMITED SMS
							smsTxUsage++;
							break;
						}
					}
				} while (cursor.moveToNext());
			}
			break;
		case 2:	//Samsung Device
			cursor = context.getContentResolver().query(SMS_URI_SAMSUNG, SMS_PROJECTION_SAMSUNG, null, null, null);
			if (cursor.moveToFirst() && cursor.getColumnCount() > 0) {
				do {
					if (isToday(new Date(cursor.getLong(0)))) {
						switch (cursor.getInt(1)) {
						case 0:	//RECEIVED SMS
							smsRxUsage++;
							break;
						case 1:	//TRANSMITED SMS
							smsTxUsage++;
							break;
						}
					}
				} while (cursor.moveToNext());
			}
			break;
		case 3:	//Lg Device
			
			break;
		}
		
		editor.putLong(Constants.RECORD_SMS_RX_USAGE, smsRxUsage);
		editor.putLong(Constants.RECORD_SMS_TX_USAGE, smsTxUsage);
		
		editor.commit();
	}
	
	public static String dataFormatSize(long size, boolean point) {
        double casted_size = (double)size;
		String suffix = null;
		
		if (size > 0) {
			if (casted_size >= 1024) {
                suffix = Constants.DATA_SIZE_KB;
                casted_size /= 1024;
                if (casted_size >= 1024) {
                	suffix = Constants.DATA_SIZE_MB;
                	casted_size /= 1024;
                	if (casted_size >= 1024) {
                		suffix = Constants.DATA_SIZE_GB;
                		casted_size /= 1024;
                	}
                }
			} else {
				suffix = Constants.DATA_SIZE_BYTE;
			}
			
			StringBuilder resultBuffer = null;
			if (point)
				resultBuffer = new StringBuilder(String.format("%.2f", casted_size));
			else
				resultBuffer = new StringBuilder(Long.toString((long)casted_size));
			
			if (suffix != null)
				resultBuffer.append(suffix);
			
			return resultBuffer.toString();
		} else {
			return size + Constants.DATA_SIZE_KB;
		}
	}
	
	//divider
	private static final int CALL_DIVIDER = 60;
	
	public static String callFormatSize(long size) {
		int seconds = 0;
		int minutes = 0;
		int hours = 0;
		
		if (size > 0) {
			seconds = (int)(size % CALL_DIVIDER);
			minutes = (int)(size / CALL_DIVIDER);
			
			if (minutes > 0) {
				hours = (int)(minutes / 60);
				minutes = (int)(minutes % 60);
			}
			
			if (hours > 0) {
				if (minutes > 0) {
					return hours + Constants.CALL_SIZE_HOUR_KO + " " + minutes + Constants.CALL_SIZE_MINUTE_KO;
				} else {
					if (seconds > 0) {
						return hours + Constants.CALL_SIZE_HOUR_KO + " " + seconds + Constants.CALL_SIZE_SECOND_KO;
					} else {
						return hours + Constants.CALL_SIZE_HOUR_KO;
					}
				}
			} else {
				if (minutes > 0) {
					if (seconds > 0) {
						return minutes + Constants.CALL_SIZE_MINUTE_KO + " " + seconds + Constants.CALL_SIZE_SECOND_KO;
					} else {
						return minutes + Constants.CALL_SIZE_MINUTE_KO;
					}
				} else {
					if (seconds > 0) {
						return seconds + Constants.CALL_SIZE_SECOND_KO;
					} else {
						return size + Constants.CALL_SIZE_SECOND_KO;
					}
				}
			}
		} else {
			return size + Constants.CALL_SIZE_SECOND_KO;
		}
	}
	
	public static String smsFormatSize(long size) {
		return size + Constants.SMS_SIZE_KO;
	}
	
	public static void saveUsageData(Context context, UsageData data) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		ContentValues values = new ContentValues();
		
		values.clear();
		values.put(Constants.DATABASE_ATTR_DATE_AMOUNT, data.getDate());
		values.put(Constants.DATABASE_ATTR_TYPE_AMOUNT, data.getType());
		values.put(Constants.DATABASE_ATTR_RX_AMOUNT, data.getRxData());
		values.put(Constants.DATABASE_ATTR_TX_AMOUNT, data.getTxData());
			
		sdb.insert(Constants.DATABASE_TABLE_NAME_AMOUNT, null, values);
		
		sdb.close();
	}
	
	public static void saveUsageData(Context context, UsageData[] data) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		ContentValues values = new ContentValues();
		
		for (int i = 0; i < data.length; i++) {
			values.clear();
			values.put(Constants.DATABASE_ATTR_DATE_AMOUNT, data[i].getDate());
			values.put(Constants.DATABASE_ATTR_TYPE_AMOUNT, data[i].getType());
			values.put(Constants.DATABASE_ATTR_RX_AMOUNT, data[i].getRxData());
			values.put(Constants.DATABASE_ATTR_TX_AMOUNT, data[i].getTxData());
			
			sdb.insert(Constants.DATABASE_TABLE_NAME_AMOUNT, null, values);
		}
		
		sdb.close();
	}
	
	public static final void saveUsageData(Context context, ArrayList<UsageData> datas) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		ContentValues values = new ContentValues();

		UsageData temp = new UsageData();
		
		for (int i = 0; i < datas.size(); i++) {
			temp.clear();
			temp.setData(datas.get(i));
			
			values.clear();
			values.put(Constants.DATABASE_ATTR_DATE_AMOUNT, temp.getDate());
			values.put(Constants.DATABASE_ATTR_TYPE_AMOUNT, temp.getType());
			values.put(Constants.DATABASE_ATTR_RX_AMOUNT, temp.getRxData());
			values.put(Constants.DATABASE_ATTR_TX_AMOUNT, temp.getTxData());
			
			sdb.insert(Constants.DATABASE_TABLE_NAME_AMOUNT, null, values);
		}
		
		sdb.close();
	}
	
	public static final void saveTrackingData(Context context, TrackingData data) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		ContentValues values = new ContentValues();
		
		values.clear();
		values.put(Constants.DATABASE_ATTR_DATE_TRACKER, data.getDate());
		values.put(Constants.DATABASE_ATTR_LEVEL_TRACKER, data.getLevel());
		values.put(Constants.DATABASE_ATTR_STATUS_TRACKER, data.getStatus());
		values.put(Constants.DATABASE_ATTR_EXTRAS_TRACKER, data.getExtras());
			
		sdb.insert(Constants.DATABASE_TABLE_NAME_TRACKER, null, values);
		
		sdb.close();
	}
	
	public static UsageData loadDateUsage(Context context, long date, int type) {
		UsageData data = new UsageData();
		SimpleDateFormat simpleDataFormat = new SimpleDateFormat(Constants.AMOUNT_DATE_FORMAT);
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String dataQuery = "SELECT " + Constants.DATABASE_ATTR_TYPE_AMOUNT + ", " + Constants.DATABASE_ATTR_RX_AMOUNT + ", " + Constants.DATABASE_ATTR_TX_AMOUNT +
						   " FROM " + Constants.DATABASE_TABLE_NAME_AMOUNT +
						   " WHERE " + Constants.DATABASE_ATTR_DATE_AMOUNT + "='" + simpleDataFormat.format(date) + "'" +
						   " AND " + Constants.DATABASE_ATTR_TYPE_AMOUNT + "='" + type + "'";
		Cursor dataCursor = sdb.rawQuery(dataQuery, null);
		
		while(dataCursor.moveToNext()) {
			data.setType(dataCursor.getInt(0));
			data.setRxData(dataCursor.getLong(1));
			data.setTxData(dataCursor.getLong(2));
		}
		
		dataCursor.close();
		sdb.close();
		
		return data;
	}
	
//	public static ArrayList<UsageData> loadMonthDataUsage(Context context, long date) {
//		ArrayList<UsageData> results = new ArrayList<UsageData>();
//		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.AMOUNT_DATE_FORMAT);
//
//		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
//
//		int endDate = Integer.parseInt(simpleDateFormat.format(date).substring(0, 6));
//		
//		String sumQuery = "SELECT " + Constants.DATABASE_ATTR_TYPE_AMOUNT + " , SUM(" + Constants.DATABASE_ATTR_RX_AMOUNT + "), SUM(" + Constants.DATABASE_ATTR_TX_AMOUNT + ")" +
//						  " FROM " + Constants.DATABASE_TABLE_NAME_AMOUNT +
//						  " WHERE SUBSTR(" + Constants.DATABASE_ATTR_DATE_AMOUNT + ", 1, 6)='" + endDate + "'" +
//						  " GROUP BY " + Constants.DATABASE_ATTR_TYPE_AMOUNT;
//		
//		Cursor sumCursor = sdb.rawQuery(sumQuery, null);
//		
//		while (sumCursor.moveToNext()) {
//			UsageData data = new UsageData();
//			data.setType(sumCursor.getInt(0));
//			data.setRxData(sumCursor.getLong(1));
//			data.setTxData(sumCursor.getLong(2));
//			
//			results.add(data);
//		}
//		
//		sumCursor.close();
//		sdb.close();
//		
//		return results;
//	}
	
	public static ArrayList<UsageData> loadMonthDataUsage(Context context, long date, int criteria) {
		if (criteria > 0) {
			ArrayList<UsageData> results = new ArrayList<UsageData>();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.AMOUNT_DATE_FORMAT);
			
			SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			
			int currentYearAndMonth = Integer.parseInt(simpleDateFormat.format(date).substring(0, 6));
			int nextYearAndMonth = Integer.parseInt(getNextYearAndMonth(date));
			
			String sumQuery = null;
			Cursor sumCursor = null;
			
			if (criteria == 1) {
				sumQuery = "SELECT " + Constants.DATABASE_ATTR_TYPE_AMOUNT + " , SUM(" + Constants.DATABASE_ATTR_RX_AMOUNT + "), SUM(" + Constants.DATABASE_ATTR_TX_AMOUNT + ")" +
						  " FROM " + Constants.DATABASE_TABLE_NAME_AMOUNT +
						  " WHERE SUBSTR(" + Constants.DATABASE_ATTR_DATE_AMOUNT + ", 1, 6)='" + currentYearAndMonth + "'" +
						  " GROUP BY " + Constants.DATABASE_ATTR_TYPE_AMOUNT;
				
				sumCursor = sdb.rawQuery(sumQuery, null);
				
				while (sumCursor.moveToNext()) {
					UsageData data = new UsageData();
					data.setType(sumCursor.getInt(0));
					data.setRxData(sumCursor.getLong(1));
					data.setTxData(sumCursor.getLong(2));
					
					results.add(data);
				}
			} else {
				sumQuery = "SELECT " + Constants.DATABASE_ATTR_TYPE_AMOUNT + ", " + Constants.DATABASE_ATTR_DATE_AMOUNT + " , SUM(" + Constants.DATABASE_ATTR_RX_AMOUNT + "), SUM(" + Constants.DATABASE_ATTR_TX_AMOUNT + ")" +
						  " FROM " + Constants.DATABASE_TABLE_NAME_AMOUNT +
						  " WHERE SUBSTR(" + Constants.DATABASE_ATTR_DATE_AMOUNT + ", 1, 6)='" + currentYearAndMonth + "'" +
						  " GROUP BY " + Constants.DATABASE_ATTR_TYPE_AMOUNT;
				
				sumCursor = sdb.rawQuery(sumQuery, null);
				
				while (sumCursor.moveToNext()) {
					if (Integer.parseInt(sumCursor.getString(1).substring(7, 8)) >= criteria) {
						UsageData data = new UsageData();
						data.setType(sumCursor.getInt(0));
						data.setRxData(sumCursor.getLong(2));
						data.setTxData(sumCursor.getLong(3));
						
						results.add(data);
					}
				}
				
				sumCursor.close();
				
				sumQuery = "SELECT " + Constants.DATABASE_ATTR_TYPE_AMOUNT + ", " + Constants.DATABASE_ATTR_DATE_AMOUNT + " , SUM(" + Constants.DATABASE_ATTR_RX_AMOUNT + "), SUM(" + Constants.DATABASE_ATTR_TX_AMOUNT + ")" +
						  " FROM " + Constants.DATABASE_TABLE_NAME_AMOUNT +
						  " WHERE SUBSTR(" + Constants.DATABASE_ATTR_DATE_AMOUNT + ", 1, 6)='" + nextYearAndMonth + "'" +
						  " GROUP BY " + Constants.DATABASE_ATTR_TYPE_AMOUNT;
				
				sumCursor = sdb.rawQuery(sumQuery, null);
				
				while (sumCursor.moveToNext()) {
					if (Integer.parseInt(sumCursor.getString(1).substring(7, 8)) < criteria) {
						UsageData data = new UsageData();
						data.setType(sumCursor.getInt(0));
						data.setRxData(sumCursor.getLong(1));
						data.setTxData(sumCursor.getLong(2));
						
						results.add(data);
					}
				}
			}
			
			sumCursor.close();
			sdb.close();
			
			return results;
		} else {
			return null;
		}
	}
	
	public static ArrayList<UsageData> loadAllDataUsage(Context context) {
		ArrayList<UsageData> results = new ArrayList<UsageData>();
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String query = "SELECT " + Constants.DATABASE_ATTR_TYPE_AMOUNT + ", " + Constants.DATABASE_ATTR_DATE_AMOUNT + ", " + Constants.DATABASE_ATTR_RX_AMOUNT + ", " + Constants.DATABASE_ATTR_TX_AMOUNT +
					   " FROM " + Constants.DATABASE_TABLE_NAME_AMOUNT;
		
		Cursor cursor = sdb.rawQuery(query, null);
		
		while (cursor.moveToNext()) {
			UsageData data = new UsageData();
			data.setType(cursor.getInt(0));
			data.setDate(cursor.getString(1));
			data.setRxData(cursor.getLong(2));
			data.setTxData(cursor.getLong(3));
			
			results.add(data);
		}
		
		cursor.close();
		sdb.close();
		
		return results;
	}
	
	public static ArrayList<TrackingData> loadDailyTrackingData(Context context, long date) {
		ArrayList<TrackingData> results = new ArrayList<TrackingData>();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.TRACKING_DATE_FORMAT);

		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

		String parsedDate = simpleDateFormat.format(date).substring(0, 8);
		
		String query = "SELECT " + Constants.DATABASE_ATTR_DATE_TRACKER + ", " + Constants.DATABASE_ATTR_LEVEL_TRACKER + ", " + Constants.DATABASE_ATTR_STATUS_TRACKER + ", " + Constants.DATABASE_ATTR_EXTRAS_TRACKER +
					   " FROM " + Constants.DATABASE_TABLE_NAME_TRACKER +
					   " WHERE SUBSTR(" + Constants.DATABASE_ATTR_DATE_AMOUNT + ", 1, 8)='" + parsedDate + "'";
		
		Cursor cursor = sdb.rawQuery(query, null);
		
		while (cursor.moveToNext()) {
			TrackingData data = new TrackingData();
			data.setDate(cursor.getString(0));
			data.setLevel(cursor.getInt(1));
			data.setStatus(cursor.getInt(2));
			data.setExtras(cursor.getInt(3));
			
			results.add(data);
		}
		
		cursor.close();
		sdb.close();
		
		return results;
	}
	
	public static boolean deletePreviousDatabase(Context context) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		boolean deleteFlag = false;
		
		try {
			String query = "DROP TABLE " + Constants.DATABASE_TABLE_NAME_AMOUNT;
			sdb.execSQL(query);
			deleteFlag = true;
		} catch(Exception e) {
			deleteFlag = false;
		} finally {
			sdb.close();
		}
		
		return deleteFlag;
	}
	
//	public static int getPreviousDate(long criteria, int offset) {
//		if (offset > 28) {
//			return -1;
//		} else {
//			Calendar calendar = Calendar.getInstance();
//			calendar.setTimeInMillis(criteria);
//			
//			int date = calendar.get(Calendar.DATE);
//			date = date - offset;
//			
//			if (date > 0) {
//				return date;
//			} else {
//				if (calendar.get(Calendar.MONTH) == 0) {
//					calendar.set(calendar.get(Calendar.YEAR) - 1, 11, 1);
//				} else {
//					calendar.set(calendar.get(Calendar.YEAR), Calendar.MONTH - 1, 1);
//					int temp = calendar.getActualMaximum(Calendar.DATE);
//					
//					if (date == 0) {
//						return temp;
//					} else {
//						return temp - offset;
//					}
//				}
//			}
//		}
//	}
	
	public static long getPreviousDateLong(long criteria, int offset) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(criteria);
		calendar.add(Calendar.DATE, offset * -1);
		return calendar.getTimeInMillis();
	}
	
	public static String getPreviousDateStr(long criteria, int offset) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(criteria);
		calendar.add(Calendar.DATE, offset * -1);
		return calendar.get(Calendar.DATE) + "";
	}
	
	public static long getPreviousMonthLong(long criteria, int offset) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(criteria);
		calendar.add(Calendar.MONTH, offset * -1);
		return calendar.getTimeInMillis();
	}
	
	public static String getPreviousMonthStr(long criteria, int offset) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(criteria);
		calendar.add(Calendar.MONTH, offset * -1);
		return (calendar.get(Calendar.MONTH) + 1) + "";
	}
	
	public static String getPreviousYearAndMonth(long criteria) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(criteria);
		calendar.add(Calendar.MONTH, -1);
		
		StringBuilder strBuilder = new StringBuilder(calendar.get(Calendar.YEAR));
		strBuilder.append(calendar.get(Calendar.MONTH) + 1);
		
		return strBuilder.toString();
	}
	public static String getNextYearAndMonth(long criteria) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(criteria);
		calendar.add(Calendar.MONTH, 1);
		
		StringBuilder strBuilder = new StringBuilder(calendar.get(Calendar.YEAR));
		strBuilder.append(calendar.get(Calendar.MONTH) + 1);
		
		return strBuilder.toString();
	}
	
		
	/**
	 * 부모 View를 기준으로 자식 View를 재귀 호출하면서 할당된 이미지를 해제한다.
	 * @param root
	 */
	
	public static void recursiveRecycle(View root) {
		if(root == null) {
			return;
		}
		
		if(root instanceof ViewGroup) {
			ViewGroup group = (ViewGroup)root;
			int count = group.getChildCount();
			
			for(int i=0; i < count; i++) {
				recursiveRecycle(group.getChildAt(i));
			}
			
			if(!(root instanceof AdapterView)) {
				group.removeAllViews();
			}
		}
		
		if(root instanceof ImageView) {
			((ImageView)root).setImageDrawable(null);
		}
		
		root.setBackgroundDrawable(null);
		
		root = null;
		
		return;
	}
	
	/**
	 * ProcessInfo를 Element로 가진 ArrayList를 입력받아, 현재 사용중인 Memory 수치를 기준으로 정렬한다.
	 * 정렬순서는 가장 높은 메모리 사용 어플리케이션 순(내림차순)으로 한다.
	 * @param arrayList
	 * @return sortedArrayList
	 */
	public static ArrayList<ProcessInfo> sortProcessInfoArrayList(ArrayList<ProcessInfo> arrayList) {
		ArrayList<ProcessInfo> sortedArrayList = new ArrayList<ProcessInfo>(arrayList);
		Collections.sort(sortedArrayList);
		Collections.reverse(sortedArrayList);
		
		return sortedArrayList;
	}
}
