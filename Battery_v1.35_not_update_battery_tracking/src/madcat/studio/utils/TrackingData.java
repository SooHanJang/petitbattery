package madcat.studio.utils;

public class TrackingData {
	public static final int STATUS_UNKNOWN			= 1;
	public static final int STATUS_CHARGE			= 2;
	public static final int STATUS_DISCHARGE		= 3;
	public static final int STATUS_NOT_CHARGE		= 4;
	public static final int STATUS_FULL				= 5;
	
	public static final int EXTRAS_BATTERY			= 0;

	private String	date					= "";
	private int		level					= 0;
	private int		status					= STATUS_DISCHARGE;
	private int		extras					= EXTRAS_BATTERY;
	
	public TrackingData() {}
	
	public TrackingData(TrackingData data) {
		this.date = data.getDate();
		this.level = data.getLevel();
		this.status = data.getStatus();
		this.extras = data.getExtras();
	}
	
	public TrackingData(String date, int level, int status) {
		// TODO Auto-generated constructor stub
		this.date = date;
		this.level = level;
		this.status = status;
	}
	
	public TrackingData(String date, int level, int status, int extras) {
		this(date, level, status);
		this.extras = extras;
	}
	
	public void clear() {
		this.date = "";
		this.level = 0;
		this.status = STATUS_CHARGE;
		this.extras = EXTRAS_BATTERY;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public void setExtras(int extras) {
		this.extras = extras;
	}
	
	public String getDate() {
		return date;
	}
	
	public int getLevel() {
		return level;
	}
	
	public int getStatus() {
		return status;
	}
	
	public int getExtras() {
		return extras;
	}
}
