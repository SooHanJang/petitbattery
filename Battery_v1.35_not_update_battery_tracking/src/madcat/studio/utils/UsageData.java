package madcat.studio.utils;

public class UsageData {
	public static final int DATA_TYPE_NETWORK		=	0;
	public static final int DATA_TYPE_CALL			=	1;
	public static final int DATA_TYPE_SMS			=	2;
	
	private int		type							=	DATA_TYPE_NETWORK;
	private String	date							=	"";
	private long	rxData							=	0;
	private long	txData							=	0;
	
	public UsageData() {}
	
	public UsageData(UsageData data) {
		this.date = data.getDate();
		this.type = data.getType();
		this.rxData = data.getRxData();
		this.txData = data.getTxData();
	}
	
	public UsageData(String date, int type, long rxData, long txData) {
		// TODO Auto-generated constructor stub
		this.date = date;
		this.type = type;
		this.rxData = rxData;
		this.txData = txData;
	}
	
	public void clear() {
		date = "";
		type = 0;
		rxData = 0;
		txData = 0;
	}
	
	public void setData(UsageData data) {
		this.date = data.getDate();
		this.type = data.getType();
		this.rxData = data.getRxData();
		this.txData = data.getTxData();
	}
	
	public int getType() {	return type;	}
	public void setType(int type) {	this.type = type;	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public void setRxData(long rxData) {
		this.rxData = rxData;
	}
	
	public void setTxData(long txData) {
		this.txData = txData;
	}
	
	public String getDate() {
		return date;
	}
	
	public long getRxData() {
		return rxData;
	}
	
	public long getTxData() {
		return txData;
	}
}
