package madcat.studio.utils;

import madcat.studio.battery.pro.R;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;

public class ScreenBrightnessController extends Activity {
	public static final String SCREEN_BRIGHTNESS = "screen_brightness";
	public static final int SCREEN_BRIGHTNESS_AUTO = 0;				//auto control
	public static final int SCREEN_BRIGHTNESS_MAXIMUM = 255;		//100%
	public static final int SCREEN_BRIGHTNESS_GENERAL = 180;		//70%
	public static final int SCREEN_BRIGHTNESS_MINIMUM = 105;		//40%
	
	public static final String SCREEN_TIMEOUT = "screen_timeout";
	public static final int SCREEN_TIMEOUT_MAXIMUM = 60 * 1000;		//60sec
	public static final int SCREEN_TIMEOUT_GENERAL = 30 * 1000;		//30sec
	public static final int SCREEN_TIMEOUT_MINIMUM = 15 * 1000;		//15sec
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.transparent_layout);
		
		int screen_brightness = getIntent().getIntExtra(SCREEN_BRIGHTNESS, SCREEN_BRIGHTNESS_GENERAL);
		int screen_timeout = getIntent().getIntExtra(SCREEN_TIMEOUT, SCREEN_TIMEOUT_GENERAL) * 1000;
		
		if (screen_brightness == SCREEN_BRIGHTNESS_AUTO) {
			Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);
		} else {
			Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
			Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, screen_brightness);
			
			Window window = getWindow();
			WindowManager.LayoutParams lp = window.getAttributes();
			lp.screenBrightness = (float)screen_brightness / SCREEN_BRIGHTNESS_MAXIMUM;
			window.setAttributes(lp);
		}
		
		Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, screen_timeout);
		
		new ScreenBrightnessControlTask().execute();
	}
	
	@Override
	public void onBackPressed() {	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
		finish();
	}
	
	private static final int SLEEP_TIME = 500;
	
	private class ScreenBrightnessControlTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(SLEEP_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
			} finally {
				finish();
			}
			
			return null;
		}
	}
}
