package madcat.studio.layouts;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import madcat.studio.adapter.ProcessInfo;
import madcat.studio.adapter.ProcessListAdapter;
import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.utils.Util;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

public class ProcessLayout extends LinearLayout {
	private Context mContext = null;
	
	private ActivityManager mActivityManager = null;
	private PackageManager mPackageManager = null;
	
	private ListView mProcessList = null;
	private ProcessListAdapter mProcessListAdapter = null;
	
	private ImageView mSplitImage = null;
	private LinearLayout mBtnLayout = null;
	
	private Button mAutoKillBtn = null;
	private Button mSelectKillBtn = null;
	
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	public ProcessLayout(Context context, int applicationTheme) {
		// TODO Auto-generated constructor stub
		super(context);
		
		mContext = context;
		
		mActivityManager = (ActivityManager)mContext.getSystemService(Context.ACTIVITY_SERVICE);
		mPackageManager = mContext.getPackageManager();
		
		mApplicationTheme = applicationTheme;
		
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.layout_process, this, true);
		
		mProcessList = (ListView)findViewById(R.id.layout_process_list);
		mSplitImage = (ImageView)findViewById(R.id.layout_process_splitbar_image);
		mBtnLayout = (LinearLayout)findViewById(R.id.layout_process_kill_layout);
		mAutoKillBtn = (Button)findViewById(R.id.layout_process_auto_kill_button);
		mSelectKillBtn = (Button)findViewById(R.id.layout_process_slt_kill_button);
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mProcessList.setDivider(new ColorDrawable(getResources().getColor(R.color.pisces_blue)));
			mProcessList.setDividerHeight(2);
			mSplitImage.setVisibility(GONE);
			mBtnLayout.setBackgroundResource(R.color.transprent);
			mAutoKillBtn.setBackgroundResource(R.drawable.com_autokill_selector_pisces);
			mSelectKillBtn.setBackgroundResource(R.drawable.com_selectkill_selector_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mProcessList.setDivider(new ColorDrawable(getResources().getColor(R.color.aries_pink)));
			mProcessList.setDividerHeight(2);
			mSplitImage.setVisibility(GONE);
			mBtnLayout.setBackgroundResource(R.color.transprent);
			mAutoKillBtn.setBackgroundResource(R.drawable.com_autokill_selector_aries);
			mSelectKillBtn.setBackgroundResource(R.drawable.com_selectkill_selector_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mProcessList.setDivider(new ColorDrawable(getResources().getColor(R.color.taurus_brown)));
			mProcessList.setDividerHeight(2);
			mSplitImage.setVisibility(GONE);
			mBtnLayout.setBackgroundResource(R.color.transprent);
			mAutoKillBtn.setBackgroundResource(R.drawable.com_autokill_selector_taurus);
			mSelectKillBtn.setBackgroundResource(R.drawable.com_selectkill_selector_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		ClickEventHandler clickEventHandler = new ClickEventHandler();
		mSelectKillBtn.setOnClickListener(clickEventHandler);
		mAutoKillBtn.setOnClickListener(clickEventHandler);
		
		setProcessList();
	}
	
	private ProcessLoadAsyncTask mProcessLoadTask = null;
	
	public void startProcessLoadTask() {
		if (mProcessLoadTask != null) {
			mProcessLoadTask.cancel(true);
			mProcessLoadTask = null;
		}
		
		mProcessLoadTask = new ProcessLoadAsyncTask();
		mProcessLoadTask.execute();
	}
	
	public void stopProcessLoadTask() {
		if (mProcessLoadTask != null) {
			mProcessLoadTask.cancel(true);
			mProcessLoadTask = null;
		}
	}
	
	private void setProcessList() {
		ArrayList<ProcessInfo> processInfoList = new ArrayList<ProcessInfo>();
		
		if (mProcessListAdapter != null)
			mProcessListAdapter = null;
		
		List<RunningAppProcessInfo> runProcessInfoList = mActivityManager.getRunningAppProcesses();
		ApplicationInfo applicationInfo = null;

		//Get Launcher Application's package name.
		Intent intent = new Intent(Intent.ACTION_MAIN, null);
		intent.addCategory(Intent.CATEGORY_HOME);
		List<ResolveInfo> launcherInfoList = mPackageManager.queryIntentActivities(intent, 0);
		HashSet<String> launcherSet = new HashSet<String>();
		
		for (ResolveInfo resolveInfo : launcherInfoList) {
			launcherSet.add(resolveInfo.activityInfo.packageName);
		}

		launcherInfoList = null;
		
		boolean isLauncher = false;
		
		for (RunningAppProcessInfo runProcessInfo : runProcessInfoList) {
			isLauncher = false;
			
			try {
				applicationInfo = mPackageManager.getApplicationInfo(runProcessInfo.pkgList[0], PackageManager.GET_META_DATA);
				
				if (!applicationInfo.packageName.equals(mContext.getPackageName())) {
					ProcessInfo processInfo = new ProcessInfo();
					processInfo.setIcon(applicationInfo.loadIcon(mPackageManager));
					processInfo.setLabel(applicationInfo.loadLabel(mPackageManager).toString());
					
					if (launcherSet.contains(applicationInfo.packageName)) {
						processInfo.setCategory(ProcessInfo.LAUNCHER);
						isLauncher = true;
					}
					
					if (!isLauncher) {
						if ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 1) {
							processInfo.setCategory(ProcessInfo.SYSTEM);
						} else {
							if (runProcessInfo.importance == RunningAppProcessInfo.IMPORTANCE_VISIBLE || runProcessInfo.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
								processInfo.setCategory(ProcessInfo.IMPORTANT);
							} else if (runProcessInfo.importance == RunningAppProcessInfo.IMPORTANCE_SERVICE) {
								processInfo.setCategory(ProcessInfo.SERVICE);
							} else if (runProcessInfo.importance == RunningAppProcessInfo.IMPORTANCE_BACKGROUND || runProcessInfo.importance == RunningAppProcessInfo.IMPORTANCE_EMPTY) {
								processInfo.setCategory(ProcessInfo.NOT_IMPORTANT);
							} else if (runProcessInfo.importance == 130) {	//130 : IMPORTANCE_PERCEPTIBLE, added API Version 9.
								processInfo.setCategory(ProcessInfo.PERCEPTIBLE);
							} else {
								processInfo.setCategory(ProcessInfo.UNKNOWN);
							}
						}
					}
					
					processInfo.setPackageName(runProcessInfo.pkgList[0]);
					processInfo.setPid(runProcessInfo.pid);
					processInfo.setLru(runProcessInfo.lru);
					
					int[] pids = {runProcessInfo.pid};
					processInfo.setMemUsage(mActivityManager.getProcessMemoryInfo(pids)[0].getTotalPss());
					
					processInfoList.add(processInfo);
				}
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			applicationInfo = null;
		}
		
		processInfoList = Util.sortProcessInfoArrayList(processInfoList);
		
		mProcessListAdapter = new ProcessListAdapter(mContext, R.layout.layout_process_row, mApplicationTheme, processInfoList);
		mProcessList.setAdapter(mProcessListAdapter);
	}
	
	private class ClickEventHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.equals(mSelectKillBtn)) {
				mProcessListAdapter.selectedProcessKill(mActivityManager);
				setProcessList();
			} else if (v.equals(mAutoKillBtn)) {
				mProcessListAdapter.autoProcessKill(mActivityManager);
				setProcessList();
			}
		}
	}
	
	private class ProcessLoadAsyncTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			publishProgress();
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			setProcessList();
		}
	}
}
