package madcat.studio.layouts;

import java.util.ArrayList;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.dialogs.UsageDialog;
import madcat.studio.utils.UsageData;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BatteryLayout extends LinearLayout {
	private Context mContext = null;
	
	private ImageView mIndicatorImage = null;
	private TextView mIndicatorText = null;
	
//	private RelativeLayout mChargeLayout = null;
	private RelativeLayout mNetworkLayout = null;
	private RelativeLayout mCallLayout = null;
	private RelativeLayout mSmsLayout = null;
	
	private ImageView mChargeImage = null;
	private TextView mNetworkText = null;
	private TextView mCallText = null;
	private TextView mSmsText = null;
	
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	private String mApplicationThemeStr = BASIC;
	private String mPackageName = "";
	
	private long mMonthNetworkRxUsage = 0;
	private long mMonthNetworkTxUsage = 0;
	private long mMonthCallRxUsage = 0;
	private long mMonthCallTxUsage = 0;
	private long mMonthSmsRxUsage = 0;
	private long mMonthSmsTxUsage = 0;
	
	private long mTodayNetworkRxUsage = 0;
	private long mTodayNetworkTxUsage = 0;
	private long mTodayCallRxUsage = 0;
	private long mTodayCallTxUsage = 0;
	private long mTodaySmsRxUsage = 0;
	private long mTodaySmsTxUsage = 0;
	
	public BatteryLayout(Context context, int applicationTheme) {
		// TODO Auto-generated constructor stub
		super(context);
		
		mContext = context;
		
		mApplicationTheme = applicationTheme;
		mPackageName = context.getPackageName();
		
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		switch (applicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mApplicationThemeStr = PISCES;
			
			layoutInflater.inflate(R.layout.layout_battery_pisces, this, true);
			mIndicatorImage = (ImageView)findViewById(R.id.layout_battery_indicator_image_pisces);
			mIndicatorText = (TextView)findViewById(R.id.layout_battery_indicator_text_pisces);
			
//			mChargeLayout = (RelativeLayout)findViewById(R.id.layout_battery_charge_layout_pisces);
			mNetworkLayout = (RelativeLayout)findViewById(R.id.layout_battery_network_layout_pisces);
			mCallLayout = (RelativeLayout)findViewById(R.id.layout_battery_call_layout_pisces);
			mSmsLayout = (RelativeLayout)findViewById(R.id.layout_battery_sms_layout_pisces);
			
			mChargeImage = (ImageView)findViewById(R.id.layout_battery_charge_image_pisces);
			mNetworkText = (TextView)findViewById(R.id.layout_battery_network_text_pisces);
			mCallText = (TextView)findViewById(R.id.layout_battery_call_text_pisces);
			mSmsText = (TextView)findViewById(R.id.layout_battery_sms_text_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mApplicationThemeStr = ARIES;
			
			layoutInflater.inflate(R.layout.layout_battery_aries, this, true);
			mIndicatorImage = (ImageView)findViewById(R.id.layout_battery_indicator_image_aries);
			mIndicatorText = (TextView)findViewById(R.id.layout_battery_indicator_text_aries);
			
//			mChargeLayout = (RelativeLayout)findViewById(R.id.layout_battery_charge_layout_aries);
			mNetworkLayout = (RelativeLayout)findViewById(R.id.layout_battery_network_layout_aries);
			mCallLayout = (RelativeLayout)findViewById(R.id.layout_battery_call_layout_aries);
			mSmsLayout = (RelativeLayout)findViewById(R.id.layout_battery_sms_layout_aries);
			
			mChargeImage = (ImageView)findViewById(R.id.layout_battery_charge_image_aries);
			mNetworkText = (TextView)findViewById(R.id.layout_battery_network_text_aries);
			mCallText = (TextView)findViewById(R.id.layout_battery_call_text_aries);
			mSmsText = (TextView)findViewById(R.id.layout_battery_sms_text_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mApplicationThemeStr = TAURUS;
			
			layoutInflater.inflate(R.layout.layout_battery_taurus, this, true);
			mIndicatorImage = (ImageView)findViewById(R.id.layout_battery_indicator_image_taurus);
			mIndicatorText = (TextView)findViewById(R.id.layout_battery_indicator_text_taurus);
			
//			mChargeLayout = (RelativeLayout)findViewById(R.id.layout_battery_charge_layout_taurus);
			mNetworkLayout = (RelativeLayout)findViewById(R.id.layout_battery_network_layout_taurus);
			mCallLayout = (RelativeLayout)findViewById(R.id.layout_battery_call_layout_taurus);
			mSmsLayout = (RelativeLayout)findViewById(R.id.layout_battery_sms_layout_taurus);
			
			mChargeImage = (ImageView)findViewById(R.id.layout_battery_charge_image_taurus);
			mNetworkText = (TextView)findViewById(R.id.layout_battery_network_text_taurus);
			mCallText = (TextView)findViewById(R.id.layout_battery_call_text_taurus);
			mSmsText = (TextView)findViewById(R.id.layout_battery_sms_text_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		case Constants.APPLICATION_THEME_BASIC:
			mApplicationThemeStr = BASIC;
			
			layoutInflater.inflate(R.layout.layout_battery_basic, this, true);
			mIndicatorImage = (ImageView)findViewById(R.id.layout_battery_indicator_image_basic);
			mIndicatorText = (TextView)findViewById(R.id.layout_battery_indicator_text_basic);
			
//			mChargeLayout = (RelativeLayout)findViewById(R.id.layout_battery_charge_layout_basic);
			mNetworkLayout = (RelativeLayout)findViewById(R.id.layout_battery_network_layout_basic);
			mCallLayout = (RelativeLayout)findViewById(R.id.layout_battery_call_layout_basic);
			mSmsLayout = (RelativeLayout)findViewById(R.id.layout_battery_sms_layout_basic);
			
			mChargeImage = (ImageView)findViewById(R.id.layout_battery_charge_image_basic);
			mNetworkText = (TextView)findViewById(R.id.layout_battery_network_text_basic);
			mCallText = (TextView)findViewById(R.id.layout_battery_call_text_basic);
			mSmsText = (TextView)findViewById(R.id.layout_battery_sms_text_basic);
			break;
		}
		
		ClickEventHandler clickEventHandler = new ClickEventHandler();
		mNetworkLayout.setOnClickListener(clickEventHandler);
		mCallLayout.setOnClickListener(clickEventHandler);
		mSmsLayout.setOnClickListener(clickEventHandler);
	}		
	
	/*****GENERAL CONSTANTS*****/
	private static final String BATTERY = "battery_";
	
//	private static final String CAPRICORN	= "_capricorn";
//	private static final String AQUARIUS	= "_aquarius";
	private static final String PISCES		= "_pisces";
	private static final String ARIES		= "_aries";
	private static final String TAURUS		= "_taurus";
//	private static final String GEMINI		= "_gemini";
//	private static final String CANCER		= "_cancer";
//	private static final String LEO			= "_leo";
//	private static final String VIRGO		= "_virgo";
//	private static final String LIBRA		= "_libra";
//	private static final String SCORPIO		= "_scorpio";
//	private static final String ARCHER		= "_archer";
	private static final String BASIC		= "_basic";
	/*****GENERAL CONSTANTS*****/
	
	
	private static final String INDICATOR	= "_indicator_";
	private static final String PERCENT 	= "%";
	
	public void setIndicatorLevel(int level) {
		mIndicatorText.setText(level + PERCENT);
		
		if (level == 100)
			level = 4;
		else
			level = level / 20;
		
		mIndicatorImage.setImageResource(mContext.getResources().getIdentifier(BATTERY + INDICATOR + level + mApplicationThemeStr, "drawable", mPackageName));
	}
	
	public static final int STATE_DISCHARGE		= 0;
	public static final int STATE_CHARGE_AC		= 1;
	public static final int STATE_CHARGE_USB	= 2;
	
	private static final String DISCHARGE	= "discharge";
	private static final String CHARGE_AC	= "charge_ac";
	private static final String CHARGE_USB	= "charge_usb";
	
	public void setChargeState(int state) {
		switch (state) {
		case STATE_DISCHARGE:
			mChargeImage.setImageResource(mContext.getResources().getIdentifier(BATTERY + DISCHARGE + mApplicationThemeStr, "drawable", mPackageName));
			break;
		case STATE_CHARGE_AC:
			mChargeImage.setImageResource(mContext.getResources().getIdentifier(BATTERY + CHARGE_AC + mApplicationThemeStr, "drawable", mPackageName));
			break;
		case STATE_CHARGE_USB:
			mChargeImage.setImageResource(mContext.getResources().getIdentifier(BATTERY + CHARGE_USB + mApplicationThemeStr, "drawable", mPackageName));
			break;
		}
	}
	
	/**
	 * Set Network, Call, SMS Usage.
	 */
	public void setPhoneUsage() {
		mMonthNetworkRxUsage = 0;
		mMonthNetworkTxUsage = 0;
		mMonthCallRxUsage = 0;
		mMonthCallTxUsage = 0;
		mMonthSmsRxUsage = 0;
		mMonthSmsTxUsage = 0;
		
		mTodayNetworkRxUsage = 0;
		mTodayNetworkTxUsage = 0;
		mTodayCallRxUsage = 0;
		mTodayCallTxUsage = 0;
		mTodaySmsRxUsage = 0;
		mTodaySmsTxUsage = 0;
		
		Util.setNetworkUsage(mContext);
		Util.setCallUsage(mContext);
		Util.setSMSUsage(mContext);
		
		SharedPreferences pref = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mTodayNetworkRxUsage = pref.getLong(Constants.RECORD_NETWORK_RX_USAGE, 0) + pref.getLong(Constants.RECORD_TEMP_NETWORK_RX_USAGE, 0);
		mTodayNetworkTxUsage = pref.getLong(Constants.RECORD_NETWORK_TX_USAGE, 0) + pref.getLong(Constants.RECORD_TEMP_NETWORK_TX_USAGE, 0);
		mTodayCallRxUsage = pref.getLong(Constants.RECORD_CALL_RX_USAGE, 0);
		mTodayCallTxUsage = pref.getLong(Constants.RECORD_CALL_TX_USAGE, 0);
		mTodaySmsRxUsage = pref.getLong(Constants.RECORD_SMS_RX_USAGE, 0);
		mTodaySmsTxUsage = pref.getLong(Constants.RECORD_SMS_TX_USAGE, 0);
		
		ArrayList<UsageData> sumAmount = Util.loadMonthDataUsage(mContext, System.currentTimeMillis(), pref.getInt(Constants.USAGE_LIMIT_RESET_DATE, Constants.USAGE_LIMIT_DEFAULT_DATE));
		int sumAmountSize = sumAmount.size();
		
		for(int i = 0; i < sumAmountSize; i++) {
			switch(sumAmount.get(i).getType()) {
				case UsageData.DATA_TYPE_NETWORK:
					mMonthNetworkRxUsage += sumAmount.get(i).getRxData();
					mMonthNetworkTxUsage += sumAmount.get(i).getTxData();
					break;
				case UsageData.DATA_TYPE_CALL:
					mMonthCallRxUsage += sumAmount.get(i).getRxData();
					mMonthCallTxUsage += sumAmount.get(i).getTxData();
					break;
				case UsageData.DATA_TYPE_SMS:
					mMonthSmsRxUsage += sumAmount.get(i).getRxData();
					mMonthSmsTxUsage += sumAmount.get(i).getTxData();
					break;
			}
		}
		
		mNetworkText.setText(Util.dataFormatSize(mMonthNetworkRxUsage + mMonthNetworkTxUsage + mTodayNetworkRxUsage + mTodayNetworkTxUsage, false));
		mCallText.setText(Util.callFormatSize(mMonthCallTxUsage + mTodayCallTxUsage));
		mSmsText.setText(Util.smsFormatSize(mMonthSmsTxUsage + mTodaySmsTxUsage));
	}
	
	//Show Usage Dialog.
	private boolean mIsDialogShowing = false;
	
	private class ClickEventHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (v.equals(mNetworkLayout)) {
				if (!mIsDialogShowing) {
					mIsDialogShowing = true;
					UsageDialog networkDialog = new UsageDialog(mContext, R.style.PopupDialog, UsageDialog.USAGE_DIALOG_NETWORK, mApplicationTheme);
					networkDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							mIsDialogShowing = false;
						}
					});
					networkDialog.setUsage(System.currentTimeMillis());
					networkDialog.show();
				}
			} else if (v.equals(mCallLayout)) {
				if (!mIsDialogShowing) {
					mIsDialogShowing = true;
					UsageDialog callDialog = new UsageDialog(mContext, R.style.PopupDialog, UsageDialog.USAGE_DIALOG_CALL, mApplicationTheme);
					callDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							mIsDialogShowing = false;
						}
					});
					callDialog.setUsage(System.currentTimeMillis());
					callDialog.show();
				}
			} else if (v.equals(mSmsLayout)) {
				if (!mIsDialogShowing) {
					mIsDialogShowing = true;
					UsageDialog smsDialog = new UsageDialog(mContext, R.style.PopupDialog, UsageDialog.USAGE_DIALOG_SMS, mApplicationTheme);
					smsDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							mIsDialogShowing = false;
						}
					});
					smsDialog.setUsage(System.currentTimeMillis());
					smsDialog.show();
				}
			}
		}
	}
}
