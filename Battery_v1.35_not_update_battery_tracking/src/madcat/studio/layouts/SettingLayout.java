package madcat.studio.layouts;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.dialogs.ApplicationHelpDialog;
import madcat.studio.dialogs.ApplicationThemeChooser;
import madcat.studio.dialogs.BatteryNotifySettingDialog;
import madcat.studio.dialogs.NotificationThemeChooser;
import madcat.studio.dialogs.UsageLimitDialog;
import madcat.studio.dialogs.WidgetThemeChooser;
import madcat.studio.schedule.ScheduleDialog;
import madcat.studio.service.IntegrityService;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class SettingLayout extends LinearLayout {
	private Context mContext = null;
	
	private SharedPreferences mPref = null;
	
	private int mBackgroundTheme = Constants.APPLICATION_THEME_BASIC;
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	private int mWidgetTheme = Constants.WIDGET_THEME_BASIC;
	private int mNotificationTheme = Constants.NOTIFICATION_THEME_BASIC_RECTANGLE;
	
	private TextView mOptimizationTitleText = null;
	private TextView mOptimizationActivateText = null;
	private TextView mOptimizationModeText = null;
	
	private CheckBox mOptimizationActivateCheckBox = null;
	private RadioGroup mOptimizationModeRadioGroup = null;
	private RadioButton mOptimizationAutoRadioButton = null;
	private RadioButton mOptimizationCustomRadioButton = null;
	
	private TextView mBatteryTrackingSettingText = null;
	private CheckBox mBatteryTrackingActivateCheckBox = null;
	
	private TextView mNotificationTitleText = null;
	private TextView mNotificationActivateText = null;
	
	private CheckBox mNotificationActivateCheckBox = null;
	
	private TextView mUsageLimitTitleText = null;
	private TextView mUsageLimitSettingText = null;
	private Button mUsageLimitSettingButton = null;
	
	private TextView mBatteryNotificationTitleText = null;
	private TextView mBatteryNotificationSettingText = null;
	private Button mBatteryNotificationSettingButton = null;
	
	private TextView mThemeTitleText = null;
	private TextView mApplicationThemeText = null;
	private TextView mWidgetThemeText = null;
	private TextView mNotificationThemeText = null;
	
	private Button mApplicationThemeButton = null;
	private Button mWidgetThemeButton = null;
	private Button mNotificationThemeButton = null;
	
	private TextView mHelpTitleText = null;
	private TextView mHelpText = null;
	
	private Button mHelpButton = null;
	
	private TextView mApplicationInformationText = null;
	
	private TextView mServiceActivateSummaryText = null;
	private TextView mServiceActivateModeSummaryText = null;
	private TextView mBatteryTrackingSummaryText = null;
	private TextView mNotificationActivateSummaryText = null;
	private TextView mUsageLimitSummaryText = null;
	private TextView mBatteryNotificationSummaryText = null;
	private TextView mApplicationThemeSummaryText = null;
	private TextView mWidgetThemeSummaryText = null;
	private TextView mNotificationThemeSummaryText = null;
	private TextView mHelpSummaryText = null;
	
	public SettingLayout(Context context, int applicationTheme) {
		// TODO Auto-generated constructor stub
		super(context);
		
		mContext = context;
		
		mPref = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mBackgroundTheme = applicationTheme;
		mApplicationTheme = applicationTheme;
		mWidgetTheme = mPref.getInt(Constants.WIDGET_THEME, Constants.WIDGET_THEME_BASIC);
		mNotificationTheme = mPref.getInt(Constants.NOTIFICATION_THEME, Constants.NOTIFICATION_THEME_BASIC_RECTANGLE);
		
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.layout_setting, this, true);
		
		mOptimizationTitleText = (TextView)findViewById(R.id.layout_preference_service_title_text);
		mOptimizationActivateText = (TextView)findViewById(R.id.layout_preference_service_activate_text);
		mOptimizationModeText = (TextView)findViewById(R.id.layout_preference_service_mode_text);
		
		mOptimizationActivateCheckBox = (CheckBox)findViewById(R.id.layout_preference_service_activate_checkbox);
		mOptimizationModeRadioGroup = (RadioGroup)findViewById(R.id.layout_preference_service_mode_group);
		mOptimizationAutoRadioButton = (RadioButton)findViewById(R.id.layout_preference_service_auto_radio);
		mOptimizationCustomRadioButton = (RadioButton)findViewById(R.id.layout_preference_service_custom_radio);
		
		mBatteryTrackingSettingText = (TextView)findViewById(R.id.layout_preference_battery_tracking_setting_text);
		mBatteryTrackingActivateCheckBox = (CheckBox)findViewById(R.id.layout_preference_battery_tracking_activate_checkbox);
		
		mNotificationTitleText = (TextView)findViewById(R.id.layout_preference_notification_title_text);
		mNotificationActivateText = (TextView)findViewById(R.id.layout_preference_notification_activate_text);
		
		mNotificationActivateCheckBox = (CheckBox)findViewById(R.id.layout_preference_notification_activate_checkbox);
		
		mUsageLimitTitleText = (TextView)findViewById(R.id.layout_preference_usage_limit_title_text);
		mUsageLimitSettingText = (TextView)findViewById(R.id.layout_preference_usage_limit_setting_text);
		mUsageLimitSettingButton = (Button)findViewById(R.id.layout_preference_usage_limit_setting_button);
		
		mBatteryNotificationTitleText = (TextView)findViewById(R.id.layout_preference_battery_notification_title_text);
		mBatteryNotificationSettingText = (TextView)findViewById(R.id.layout_preference_battery_notification_setting_text);
		mBatteryNotificationSettingButton = (Button)findViewById(R.id.layout_preference_battery_notification_setting_button);
		
		mThemeTitleText = (TextView)findViewById(R.id.layout_preference_theme_title_text);
		mApplicationThemeText = (TextView)findViewById(R.id.layout_preference_application_theme_text);
		mWidgetThemeText = (TextView)findViewById(R.id.layout_preference_widget_theme_text);
		mNotificationThemeText = (TextView)findViewById(R.id.layout_preference_notification_theme_text);
		
		mApplicationThemeButton = (Button)findViewById(R.id.layout_preference_application_theme_button);
		mWidgetThemeButton = (Button)findViewById(R.id.layout_preference_widget_theme_button);
		mNotificationThemeButton = (Button)findViewById(R.id.layout_preference_notification_theme_button);
		
		mHelpTitleText = (TextView)findViewById(R.id.layout_preference_help_title_text);
		mHelpText = (TextView)findViewById(R.id.layout_preference_help_text);
		
		mHelpButton = (Button)findViewById(R.id.layout_preference_help_button);
		
		mApplicationInformationText = (TextView)findViewById(R.id.layout_preference_application_information_text);
		
		mServiceActivateSummaryText = (TextView)findViewById(R.id.layout_preference_service_activiate_summary_text);
		mServiceActivateModeSummaryText = (TextView)findViewById(R.id.layout_preference_service_mode_summary_text);
		mBatteryTrackingSummaryText = (TextView)findViewById(R.id.layout_preference_battery_tracking_summary_text);
		mNotificationActivateSummaryText = (TextView)findViewById(R.id.layout_preference_notification_activate_summary_text);
		mUsageLimitSummaryText = (TextView)findViewById(R.id.layout_preference_usage_limit_summary_text);
		mBatteryNotificationSummaryText = (TextView)findViewById(R.id.layout_preference_battery_notification_summary_text);
		mApplicationThemeSummaryText = (TextView)findViewById(R.id.layout_preference_application_theme_summary_text);
		mWidgetThemeSummaryText = (TextView)findViewById(R.id.layout_preference_widget_theme_summary_text);
		mNotificationThemeSummaryText = (TextView)findViewById(R.id.layout_preference_notification_theme_summary_text);
		mHelpSummaryText = (TextView)findViewById(R.id.layout_preference_help_summary_text);
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mOptimizationTitleText.setBackgroundResource(R.color.pisces_blue);
			mOptimizationActivateText.setTextColor(getResources().getColor(R.color.white));
			mOptimizationModeText.setTextColor(getResources().getColor(R.color.white));
			
			mOptimizationActivateCheckBox.setBackgroundResource(R.drawable.com_preference_toggle_btn_pisces);
			mOptimizationAutoRadioButton.setBackgroundResource(R.drawable.com_preference_toggle_btn_pisces);
			mOptimizationCustomRadioButton.setBackgroundResource(R.drawable.com_preference_toggle_btn_pisces);
			
			mBatteryTrackingSettingText.setTextColor(getResources().getColor(R.color.white));
			mBatteryTrackingActivateCheckBox.setBackgroundResource(R.drawable.com_preference_toggle_btn_pisces);
			
			mNotificationTitleText.setBackgroundResource(R.color.pisces_blue);
			mNotificationActivateText.setTextColor(getResources().getColor(R.color.white));
			
			mNotificationActivateCheckBox.setBackgroundResource(R.drawable.com_preference_toggle_btn_pisces);
			
			mUsageLimitTitleText.setBackgroundResource(R.color.pisces_blue);
			mUsageLimitSettingText.setTextColor(getResources().getColor(R.color.white));
			mUsageLimitSettingButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			
			mBatteryNotificationTitleText.setBackgroundResource(R.color.pisces_blue);
			mBatteryNotificationSettingText.setTextColor(getResources().getColor(R.color.white));
			mBatteryNotificationSettingButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			
			mThemeTitleText.setBackgroundResource(R.color.pisces_blue);
			mApplicationThemeText.setTextColor(getResources().getColor(R.color.white));
			mWidgetThemeText.setTextColor(getResources().getColor(R.color.white));
			mNotificationThemeText.setTextColor(getResources().getColor(R.color.white));
			
			mApplicationThemeButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mWidgetThemeButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mNotificationThemeButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			
			mHelpTitleText.setBackgroundResource(R.color.pisces_blue);
			mHelpText.setTextColor(getResources().getColor(R.color.white));
			mHelpButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			
			mApplicationInformationText.setTextColor(getResources().getColor(R.color.white));
			
			mServiceActivateSummaryText.setTextColor(getResources().getColor(R.color.white));
			mServiceActivateModeSummaryText.setTextColor(getResources().getColor(R.color.white));
			mBatteryTrackingSummaryText.setTextColor(getResources().getColor(R.color.white));
			mNotificationActivateSummaryText.setTextColor(getResources().getColor(R.color.white));
			mUsageLimitSummaryText.setTextColor(getResources().getColor(R.color.white));
			mBatteryNotificationSummaryText.setTextColor(getResources().getColor(R.color.white));
			mApplicationThemeSummaryText.setTextColor(getResources().getColor(R.color.white));
			mWidgetThemeSummaryText.setTextColor(getResources().getColor(R.color.white));
			mNotificationThemeSummaryText.setTextColor(getResources().getColor(R.color.white));
			mHelpSummaryText.setTextColor(getResources().getColor(R.color.white));
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mOptimizationTitleText.setBackgroundResource(R.color.aries_pink);
			mOptimizationActivateText.setTextColor(getResources().getColor(R.color.aries_pink));
			mOptimizationModeText.setTextColor(getResources().getColor(R.color.aries_pink));
			
			mOptimizationActivateCheckBox.setBackgroundResource(R.drawable.com_preference_toggle_btn_aries);
			mOptimizationAutoRadioButton.setBackgroundResource(R.drawable.com_preference_toggle_btn_aries);
			mOptimizationCustomRadioButton.setBackgroundResource(R.drawable.com_preference_toggle_btn_aries);
			
			mBatteryTrackingSettingText.setTextColor(getResources().getColor(R.color.aries_pink));
			mBatteryTrackingActivateCheckBox.setBackgroundResource(R.drawable.com_preference_toggle_btn_aries);
			
			mNotificationTitleText.setBackgroundResource(R.color.aries_pink);
			mNotificationActivateText.setTextColor(getResources().getColor(R.color.aries_pink));
			
			mNotificationActivateCheckBox.setBackgroundResource(R.drawable.com_preference_toggle_btn_aries);
			
			mUsageLimitTitleText.setBackgroundResource(R.color.aries_pink);
			mUsageLimitSettingText.setTextColor(getResources().getColor(R.color.aries_pink));
			mUsageLimitSettingButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			
			mBatteryNotificationTitleText.setBackgroundResource(R.color.aries_pink);
			mBatteryNotificationSettingText.setTextColor(getResources().getColor(R.color.aries_pink));
			mBatteryNotificationSettingButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			
			mThemeTitleText.setBackgroundResource(R.color.aries_pink);
			mApplicationThemeText.setTextColor(getResources().getColor(R.color.aries_pink));
			mWidgetThemeText.setTextColor(getResources().getColor(R.color.aries_pink));
			mNotificationThemeText.setTextColor(getResources().getColor(R.color.aries_pink));
			
			mApplicationThemeButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mWidgetThemeButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mNotificationThemeButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			
			mHelpTitleText.setBackgroundResource(R.color.aries_pink);
			mHelpText.setTextColor(getResources().getColor(R.color.aries_pink));
			mHelpButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			
			mApplicationInformationText.setTextColor(getResources().getColor(R.color.aries_pink));
			
			mServiceActivateSummaryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mServiceActivateModeSummaryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mNotificationActivateSummaryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mBatteryTrackingSummaryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mUsageLimitSummaryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mBatteryNotificationSummaryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mApplicationThemeSummaryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mWidgetThemeSummaryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mNotificationThemeSummaryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mHelpSummaryText.setTextColor(getResources().getColor(R.color.aries_pink));
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mOptimizationTitleText.setBackgroundResource(R.color.taurus_white_brown);
			mOptimizationActivateText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mOptimizationModeText.setTextColor(getResources().getColor(R.color.taurus_brown));
			
			mOptimizationActivateCheckBox.setBackgroundResource(R.drawable.com_preference_toggle_btn_taurus);
			mOptimizationAutoRadioButton.setBackgroundResource(R.drawable.com_preference_toggle_btn_taurus);
			mOptimizationCustomRadioButton.setBackgroundResource(R.drawable.com_preference_toggle_btn_taurus);
			
			mBatteryTrackingSettingText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mBatteryTrackingActivateCheckBox.setBackgroundResource(R.drawable.com_preference_toggle_btn_taurus);
			
			mNotificationTitleText.setBackgroundResource(R.color.taurus_white_brown);
			mNotificationActivateText.setTextColor(getResources().getColor(R.color.taurus_brown));
			
			mUsageLimitTitleText.setBackgroundResource(R.color.taurus_white_brown);
			mUsageLimitSettingText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mUsageLimitSettingButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			
			mNotificationActivateCheckBox.setBackgroundResource(R.drawable.com_preference_toggle_btn_taurus);
			
			mBatteryNotificationTitleText.setBackgroundResource(R.color.taurus_white_brown);
			mBatteryNotificationSettingText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mBatteryNotificationSettingButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			
			mThemeTitleText.setBackgroundResource(R.color.taurus_white_brown);
			mApplicationThemeText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mWidgetThemeText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mNotificationThemeText.setTextColor(getResources().getColor(R.color.taurus_brown));
			
			mApplicationThemeButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mWidgetThemeButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mNotificationThemeButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			
			mHelpTitleText.setBackgroundResource(R.color.taurus_white_brown);
			mHelpText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mHelpButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			
			mApplicationInformationText.setTextColor(getResources().getColor(R.color.taurus_brown));
			
			mServiceActivateSummaryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mServiceActivateModeSummaryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mBatteryTrackingSummaryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mNotificationActivateSummaryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mUsageLimitSummaryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mBatteryNotificationSummaryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mApplicationThemeSummaryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mWidgetThemeSummaryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mNotificationThemeSummaryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mHelpSummaryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		mOptimizationActivateCheckBox.setChecked(mPref.getBoolean(Constants.OPTIMIZATION_EXECUTE, true));
		mNotificationActivateCheckBox.setChecked(mPref.getBoolean(Constants.NOTIFICATION_EXECUTE, true));
		mBatteryTrackingActivateCheckBox.setChecked(mPref.getBoolean(Constants.BATTERY_TRACKING_EXECUTE, true));
		
		if (mPref.getBoolean(Constants.OPTIMIZATION_TYPE, Constants.OPTIMIZATION_TYPE_AUTO)) {
			mOptimizationAutoRadioButton.setChecked(true);
		} else {
			mOptimizationCustomRadioButton.setChecked(true);
		}

		CheckEventHandler checkEventHandler = new CheckEventHandler();
		mOptimizationActivateCheckBox.setOnCheckedChangeListener(checkEventHandler);
		mNotificationActivateCheckBox.setOnCheckedChangeListener(checkEventHandler);
		mBatteryTrackingActivateCheckBox.setOnCheckedChangeListener(checkEventHandler);
		
		mOptimizationModeRadioGroup.setOnCheckedChangeListener(new RadioEventHandler());
		
		ClickEventHandler clickEventHandler = new ClickEventHandler();
		mUsageLimitSettingButton.setOnClickListener(clickEventHandler);
		mBatteryNotificationSettingButton.setOnClickListener(clickEventHandler);
		mApplicationThemeButton.setOnClickListener(clickEventHandler);
		mWidgetThemeButton.setOnClickListener(clickEventHandler);
		mNotificationThemeButton.setOnClickListener(clickEventHandler);
		mHelpButton.setOnClickListener(clickEventHandler);
	}
	
	private void restartSerivce() {
		Intent service = new Intent(IntegrityService.SERVICE_UPDATE_ACTION);
		mContext.stopService(service);
		mContext.startService(service);
	}
	
	private boolean mIsDialogShowing = false;
	
	private class ClickEventHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.layout_preference_usage_limit_setting_button:
				if (!mIsDialogShowing) {
					mIsDialogShowing = true;
					final UsageLimitDialog usageLimitDialog = new UsageLimitDialog(mContext, R.style.PopupDialog, mBackgroundTheme);
					usageLimitDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							mIsDialogShowing = false;
						}
					});
					usageLimitDialog.show();
				}
				break;
			case R.id.layout_preference_battery_notification_setting_button:
				//Battery Notification Dialog Show.
				if (!mIsDialogShowing) {
					mIsDialogShowing = true;
					final BatteryNotifySettingDialog batteryNotifySettingDialog = new BatteryNotifySettingDialog(mContext, R.style.PopupDialog, mBackgroundTheme);
					batteryNotifySettingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							mIsDialogShowing = false;
							restartSerivce();
							
						}
					});
					batteryNotifySettingDialog.show();
				}
				break;
			case R.id.layout_preference_application_theme_button:
				if (!mIsDialogShowing) {
					mIsDialogShowing = true;
					final ApplicationThemeChooser applicationThemeChooser = new ApplicationThemeChooser(mContext, R.style.PopupDialog, mBackgroundTheme);
					applicationThemeChooser.setApplicaitonTheme(mApplicationTheme);
					applicationThemeChooser.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							mApplicationTheme = applicationThemeChooser.getApplicationTheme();
							mIsDialogShowing = false;
						}
					});
					applicationThemeChooser.show();
				}
				break;
			case R.id.layout_preference_widget_theme_button:
				if (!mIsDialogShowing) {
					mIsDialogShowing = true;
					final WidgetThemeChooser widgetThemeChooser = new WidgetThemeChooser(mContext, R.style.PopupDialog, mBackgroundTheme);
					widgetThemeChooser.setWidgetTheme(mWidgetTheme);
					widgetThemeChooser.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							mWidgetTheme = widgetThemeChooser.getWidgetTheme();
							mIsDialogShowing = false;
						}
					});
					widgetThemeChooser.show();
				}
				break;
			case R.id.layout_preference_notification_theme_button:
				if (!mIsDialogShowing) {
					mIsDialogShowing = true;
					final NotificationThemeChooser notificationThemeChooser = new NotificationThemeChooser(mContext, R.style.PopupDialog, mBackgroundTheme);
					notificationThemeChooser.setNotificaitonTheme(mNotificationTheme);
					notificationThemeChooser.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							mNotificationTheme = notificationThemeChooser.getNotificationTheme();
							mIsDialogShowing = false;
						}
					});
					notificationThemeChooser.show();
				}
				break;
			case R.id.layout_preference_help_button:
				if (!mIsDialogShowing) {
					mIsDialogShowing = true;
					ApplicationHelpDialog applicationHelpDialog = new ApplicationHelpDialog(mContext, R.style.PopupDialog, mApplicationTheme);
					applicationHelpDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							mIsDialogShowing = false;
						}
					});
					applicationHelpDialog.show();
				}
				break;
			}
		}
	}
	
	private class RadioEventHandler implements RadioGroup.OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			if (group.equals(mOptimizationModeRadioGroup)) {
				switch (checkedId) {
				case R.id.layout_preference_service_auto_radio:
					SharedPreferences.Editor editor = mPref.edit();
					editor.putBoolean(Constants.OPTIMIZATION_TYPE, Constants.OPTIMIZATION_TYPE_AUTO);
					editor.commit();
					
					restartSerivce();
					break;
				case R.id.layout_preference_service_custom_radio:
					final ScheduleDialog scheduleDialog = new ScheduleDialog(mContext, R.style.PopupDialog);
					WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
					layoutParams.copyFrom(scheduleDialog.getWindow().getAttributes());
					layoutParams.width = WindowManager.LayoutParams.FILL_PARENT;
					layoutParams.height = WindowManager.LayoutParams.FILL_PARENT;
					
					scheduleDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
						@Override
						public void onDismiss(DialogInterface dialog) {
							// TODO Auto-generated method stub
							if (scheduleDialog.isSave()) {
								//Restart Service
								restartSerivce();
							} else {
								mOptimizationAutoRadioButton.setChecked(true);
							}
						}
					});
					scheduleDialog.show();
					scheduleDialog.getWindow().setAttributes(layoutParams);
					break;
				}
			}
		}
	}
	
	private class CheckEventHandler implements CompoundButton.OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			SharedPreferences.Editor editor = mPref.edit();
			
			switch (buttonView.getId()) {
			case R.id.layout_preference_service_activate_checkbox:
				//Battery Optimization Service
				mOptimizationAutoRadioButton.setEnabled(isChecked);
				mOptimizationCustomRadioButton.setEnabled(isChecked);
				
				editor.putBoolean(Constants.OPTIMIZATION_EXECUTE, isChecked);
				editor.commit();
				
				restartSerivce();
				break;
			case R.id.layout_preference_notification_activate_checkbox:
				//Battery Notification Service
				editor.putBoolean(Constants.NOTIFICATION_EXECUTE, isChecked);
				editor.commit();
				
				restartSerivce();
				break;
			case R.id.layout_preference_battery_tracking_activate_checkbox:
				editor.putBoolean(Constants.BATTERY_TRACKING_EXECUTE, isChecked);
				editor.commit();
				
//				restartService();
				break;
			}
		}
	}
}
