package madcat.studio.layouts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.utils.Util;
import android.content.Context;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SystemLayout extends LinearLayout {
	private Context mContext = null;
	
	private LinearLayout mResourceLayout = null;
	private LinearLayout mStorageLayout = null;
	
	private TextView mBatteryText = null;
	private TextView mBuildText = null;
	private TextView mCpuText = null;
	private TextView mResourceCpuText = null;
	private TextView mResourceMemoryText = null;
	private TextView mStorageInternalText = null;
	private TextView mStorageExternalText = null;
	private TextView mTelephonyText = null;
	
	private ProgressBar mResourceCpuProgress = null;
	private ProgressBar mResourceMemoryProgress = null;
	private ProgressBar mStorageInternalProgress = null;
	private ProgressBar mStorageExternalProgress = null;
	
	private ImageView mBatteryImage = null;
	private ImageView mBuildImage = null;
	private ImageView mCpuImage = null;
	private ImageView mResourceImage = null;
	private ImageView mStorageImage = null;
	private ImageView mTelephonyImage = null;
	
	private ResourceWatcher mResourceWatcher = null;
	
	private static final int PADDING_LEFT	= 15;
	private static final int PADDING_RIGHT	= 15;
	private static final int PADDING_TOP	= 30;
	private static final int PADDING_BOTTOM	= 30;
	
	public SystemLayout(Context context, int applicationTheme) {
		// TODO Auto-generated constructor stub
		super(context);
		
		mContext = context;
		
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.layout_system, this, true);
		
		mResourceLayout = (LinearLayout)findViewById(R.id.layout_system_resource_layout);
		mStorageLayout = (LinearLayout)findViewById(R.id.layout_system_storage_layout);
		
		mBatteryText = (TextView)findViewById(R.id.layout_system_battery_text);
		mBuildText = (TextView)findViewById(R.id.layout_system_build_text);
		mCpuText = (TextView)findViewById(R.id.layout_system_cpu_text);
		mResourceCpuText = (TextView)findViewById(R.id.layout_system_resource_cpu_text);
		mResourceMemoryText = (TextView)findViewById(R.id.layout_system_resource_memory_text);
		mStorageInternalText = (TextView)findViewById(R.id.layout_system_storage_internal_text);
		mStorageExternalText = (TextView)findViewById(R.id.layout_system_storage_external_text);
		mTelephonyText = (TextView)findViewById(R.id.layout_system_telephony_text);
		
		mResourceCpuProgress = (ProgressBar)findViewById(R.id.layout_system_resource_cpu_progress);
		mResourceMemoryProgress = (ProgressBar)findViewById(R.id.layout_system_resource_memory_progress);
		mStorageInternalProgress = (ProgressBar)findViewById(R.id.layout_system_storage_internal_progress);
		mStorageExternalProgress = (ProgressBar)findViewById(R.id.layout_system_storage_external_progress);
		
		mBatteryImage = (ImageView)findViewById(R.id.layout_system_battery_image);
		mBuildImage = (ImageView)findViewById(R.id.layout_system_build_image);
		mCpuImage = (ImageView)findViewById(R.id.layout_system_cpu_image);
		mResourceImage = (ImageView)findViewById(R.id.layout_system_resource_image);
		mStorageImage = (ImageView)findViewById(R.id.layout_system_storage_image);
		mTelephonyImage = (ImageView)findViewById(R.id.layout_system_telephony_image);
		
		switch (applicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mBatteryText.setBackgroundResource(R.drawable.system_background_pisces);
			mBuildText.setBackgroundResource(R.drawable.system_background_pisces);
			mCpuText.setBackgroundResource(R.drawable.system_background_pisces);
			mResourceLayout.setBackgroundResource(R.drawable.system_background_pisces);
			mStorageLayout.setBackgroundResource(R.drawable.system_background_pisces);
			mTelephonyText.setBackgroundResource(R.drawable.system_background_pisces);
			
			mBatteryText.setTextColor(getResources().getColor(R.color.pisces_white_blue));
			mBuildText.setTextColor(getResources().getColor(R.color.pisces_white_blue));
			mCpuText.setTextColor(getResources().getColor(R.color.pisces_white_blue));
			mResourceCpuText.setTextColor(getResources().getColor(R.color.pisces_white_blue));
			mResourceMemoryText.setTextColor(getResources().getColor(R.color.pisces_white_blue));
			mStorageInternalText.setTextColor(getResources().getColor(R.color.pisces_white_blue));
			mStorageExternalText.setTextColor(getResources().getColor(R.color.pisces_white_blue));
			mTelephonyText.setTextColor(getResources().getColor(R.color.pisces_white_blue));
			
			mResourceCpuProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_pisces));
			mResourceMemoryProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_pisces));
			mStorageInternalProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_pisces));
			mStorageExternalProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_pisces));
			
			mBatteryImage.setImageResource(R.drawable.system_category_battery_pisces);
			mBuildImage.setImageResource(R.drawable.system_category_build_pisces);
			mCpuImage.setImageResource(R.drawable.system_category_cpu_pisces);
			mResourceImage.setImageResource(R.drawable.system_category_resource_pisces);
			mStorageImage.setImageResource(R.drawable.system_category_storage_pisces);
			mTelephonyImage.setImageResource(R.drawable.system_category_telephony_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mBatteryText.setBackgroundResource(R.drawable.system_background_aries);
			mBuildText.setBackgroundResource(R.drawable.system_background_aries);
			mCpuText.setBackgroundResource(R.drawable.system_background_aries);
			mResourceLayout.setBackgroundResource(R.drawable.system_background_aries);
			mStorageLayout.setBackgroundResource(R.drawable.system_background_aries);
			mTelephonyText.setBackgroundResource(R.drawable.system_background_aries);
			
			mBatteryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mBuildText.setTextColor(getResources().getColor(R.color.aries_pink));
			mCpuText.setTextColor(getResources().getColor(R.color.aries_pink));
			mResourceCpuText.setTextColor(getResources().getColor(R.color.aries_pink));
			mResourceMemoryText.setTextColor(getResources().getColor(R.color.aries_pink));
			mStorageInternalText.setTextColor(getResources().getColor(R.color.aries_pink));
			mStorageExternalText.setTextColor(getResources().getColor(R.color.aries_pink));
			mTelephonyText.setTextColor(getResources().getColor(R.color.aries_pink));
			
			mResourceCpuProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_aries));
			mResourceMemoryProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_aries));
			mStorageInternalProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_aries));
			mStorageExternalProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_aries));
			
			mBatteryImage.setImageResource(R.drawable.system_category_battery_aries);
			mBuildImage.setImageResource(R.drawable.system_category_build_aries);
			mCpuImage.setImageResource(R.drawable.system_category_cpu_aries);
			mResourceImage.setImageResource(R.drawable.system_category_resource_aries);
			mStorageImage.setImageResource(R.drawable.system_category_storage_aries);
			mTelephonyImage.setImageResource(R.drawable.system_category_telephony_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mBatteryText.setBackgroundResource(R.drawable.system_background_taurus);
			mBuildText.setBackgroundResource(R.drawable.system_background_taurus);
			mCpuText.setBackgroundResource(R.drawable.system_background_taurus);
			mResourceLayout.setBackgroundResource(R.drawable.system_background_taurus);
			mStorageLayout.setBackgroundResource(R.drawable.system_background_taurus);
			mTelephonyText.setBackgroundResource(R.drawable.system_background_taurus);
			
			mBatteryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mBuildText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mCpuText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mResourceCpuText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mResourceMemoryText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mStorageInternalText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mStorageExternalText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mTelephonyText.setTextColor(getResources().getColor(R.color.taurus_brown));
			
			mResourceCpuProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_taurus));
			mResourceMemoryProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_taurus));
			mStorageInternalProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_taurus));
			mStorageExternalProgress.setProgressDrawable(getResources().getDrawable(R.drawable.com_progressbar_taurus));
			
			mBatteryImage.setImageResource(R.drawable.system_category_battery_taurus);
			mBuildImage.setImageResource(R.drawable.system_category_build_taurus);
			mCpuImage.setImageResource(R.drawable.system_category_cpu_taurus);
			mResourceImage.setImageResource(R.drawable.system_category_resource_taurus);
			mStorageImage.setImageResource(R.drawable.system_category_storage_taurus);
			mTelephonyImage.setImageResource(R.drawable.system_category_telephony_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		mBatteryText.setPadding(PADDING_LEFT, PADDING_TOP, PADDING_RIGHT, PADDING_BOTTOM);
		mBuildText.setPadding(PADDING_LEFT, PADDING_TOP, PADDING_RIGHT, PADDING_BOTTOM);
		mCpuText.setPadding(PADDING_LEFT, PADDING_TOP, PADDING_RIGHT, PADDING_BOTTOM);
		mResourceLayout.setPadding(PADDING_LEFT, PADDING_TOP, PADDING_RIGHT, PADDING_BOTTOM);
		mStorageLayout.setPadding(PADDING_LEFT, PADDING_TOP, PADDING_RIGHT, PADDING_BOTTOM);
		mTelephonyText.setPadding(PADDING_LEFT, PADDING_TOP, PADDING_RIGHT, PADDING_BOTTOM);
	}
	
	public void initialize() {
		setBuildInfo();
		setCpuInfo();
		setPhoneInfo();
		setStorageInfo();
		startResourceWatcher();
	}
	
	public void setBatteryInfo(int level, int health, int status, int plugged, int voltage, int temperature, String technology) {
		mBatteryText.setText(mContext.getString(R.string.prefix_battery_level) + level + "%\n");
		
		mBatteryText.append(mContext.getString(R.string.prefix_battery_health));
		if (health == BatteryManager.BATTERY_HEALTH_UNKNOWN) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_health_unknown) + "\n");
		} else if (health == BatteryManager.BATTERY_HEALTH_GOOD) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_health_good) + "\n");
		} else if (health == BatteryManager.BATTERY_HEALTH_OVERHEAT) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_health_overheat) + "\n");
		} else if (health == BatteryManager.BATTERY_HEALTH_DEAD) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_health_dead) + "\n");
		} else if (health == BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_health_over_voltage) + "\n");
		} else if (health == BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_health_unspecified_failure) + "\n");
		} else if (health == 7) {	//BatteryManager.BATTERY_HEALTH_COLD
			mBatteryText.append(mContext.getString(R.string.postfix_battery_health_cold) + "\n");
		} else {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_health_unknown) + "\n");
		}
		
		mBatteryText.append(mContext.getString(R.string.prefix_battery_status));
		if (status == BatteryManager.BATTERY_STATUS_UNKNOWN) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_status_unknown) + "\n");
		} else if (status == BatteryManager.BATTERY_STATUS_CHARGING) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_status_charging) + "\n");
		} else if (status == BatteryManager.BATTERY_STATUS_DISCHARGING) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_status_discharging) + "\n");
		} else if (status == BatteryManager.BATTERY_STATUS_NOT_CHARGING) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_status_not_charging) + "\n");
		} else if (status == BatteryManager.BATTERY_STATUS_FULL) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_status_full) + "\n");
		} else {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_status_unknown) + "\n");
		}
		
		mBatteryText.append(mContext.getString(R.string.prefix_battery_source));
		if (plugged == 0) {	//BatteryMawnager.BATTERY_UNPLUGGED
			mBatteryText.append(mContext.getString(R.string.postfix_battery_source_battery) + "\n");
		} else if (plugged == BatteryManager.BATTERY_PLUGGED_AC) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_source_ac) + "\n");
		} else if (plugged == BatteryManager.BATTERY_PLUGGED_USB) {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_source_usb) + "\n");
		} else {
			mBatteryText.append(mContext.getString(R.string.postfix_battery_source_unknown) + "\n");
		}
		
		mBatteryText.append(mContext.getString(R.string.prefix_battery_voltage) + voltage + mContext.getString(R.string.postfix_battery_voltage)+ "\n");
		mBatteryText.append(mContext.getString(R.string.prefix_battery_temperature) + (float)(temperature / 10) + mContext.getString(R.string.postfix_battery_temperature) + "\n");
		mBatteryText.append(mContext.getString(R.string.prefix_battery_type) + technology);
	}
	
	private void setBuildInfo() {
		mBuildText.setText(mContext.getString(R.string.prefix_build_id) + Build.ID + "\n");
		mBuildText.append(mContext.getString(R.string.prefix_build_manufacturer) + Build.MANUFACTURER + "\n");
		mBuildText.append(mContext.getString(R.string.prefix_build_model) + Build.MODEL + "\n");
		mBuildText.append(mContext.getString(R.string.prefix_build_product) + Build.PRODUCT + "\n");
		
		mBuildText.append(mContext.getString(R.string.prefix_build_cpu_abi) + Build.CPU_ABI + "\n");
		mBuildText.append(mContext.getString(R.string.prefix_build_bootloader) + Build.BOOTLOADER + "\n");
		mBuildText.append(mContext.getString(R.string.prefix_build_tags) + Build.TAGS + "\n");
		mBuildText.append(mContext.getString(R.string.prefix_build_type) + Build.TYPE + "\n");
		mBuildText.append(mContext.getString(R.string.prefix_build_user) + Build.USER + "\n");
		mBuildText.append(mContext.getString(R.string.prefix_build_host) + Build.HOST + "\n");
		
		mBuildText.append(mContext.getString(R.string.prefix_build_display) + Build.DISPLAY);
	}
	
	private static final String GET_CPU_INFO = "cat /proc/cpuinfo";
	
	private void setCpuInfo() {
		try {
			Runtime runtime = Runtime.getRuntime();
			Process process = runtime.exec(GET_CPU_INFO);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			String cpuInfo = "";
			String readLine = reader.readLine();
			
			cpuInfo = readLine;
			
			while ((readLine = reader.readLine()) != null)
				cpuInfo += "\n" + readLine;
			
			reader.close();
			
			mCpuText.setText(cpuInfo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void setPhoneInfo() {
		TelephonyManager telephonyManager = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
		
		mTelephonyText.setText(mContext.getString(R.string.prefix_device_id) + telephonyManager.getDeviceId() + "\n\n");
		
		mTelephonyText.append(mContext.getString(R.string.prefix_phone_number) + telephonyManager.getLine1Number() + "\n");
		mTelephonyText.append(mContext.getString(R.string.prefix_phone_type));
		switch (telephonyManager.getPhoneType()) {
		case TelephonyManager.PHONE_TYPE_CDMA:
			mTelephonyText.append(mContext.getString(R.string.postfix_phone_type_cdma) + "\n\n");
			break;
		case TelephonyManager.PHONE_TYPE_GSM:
			mTelephonyText.append(mContext.getString(R.string.postfix_phone_type_gsm) + "\n\n");
			break;
		case TelephonyManager.PHONE_TYPE_NONE:
			mTelephonyText.append(mContext.getString(R.string.postfix_phone_type_none) + "\n\n");
			break;
		}
		
		mTelephonyText.append(mContext.getString(R.string.prefix_network_country_iso) + telephonyManager.getNetworkCountryIso() + "\n");
		mTelephonyText.append(mContext.getString(R.string.prefix_network_operation) + telephonyManager.getNetworkOperator() + "\n");
		mTelephonyText.append(mContext.getString(R.string.prefix_network_operation_name) + telephonyManager.getNetworkOperatorName() + "\n");
		mTelephonyText.append(mContext.getString(R.string.prefix_network_type));
		switch (telephonyManager.getNetworkType()) {
		case TelephonyManager.NETWORK_TYPE_1xRTT:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_1xrtt) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_CDMA:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_cdma) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_EDGE:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_edge) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_EVDO_0:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_evdo_0) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_EVDO_A:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_evdo_a) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_GPRS:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_gprs) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_HSPA:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_hspa) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_HSDPA:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_hsdpa) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_HSUPA:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_hsupa) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_IDEN:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_iden) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_UMTS:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_umts) + "\n\n");
			break;
		case TelephonyManager.NETWORK_TYPE_UNKNOWN:
			mTelephonyText.append(mContext.getString(R.string.postfix_network_type_unknown) + "\n\n");
			break;
		}
		
		mTelephonyText.append(mContext.getString(R.string.prefix_sim_state));
		switch (telephonyManager.getSimState()) {
		case TelephonyManager.SIM_STATE_ABSENT:
			mTelephonyText.append(mContext.getString(R.string.postfix_sim_state_absent) + "\n");
			break;
		case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
			mTelephonyText.append(mContext.getString(R.string.postfix_sim_state_network_lock) + "\n");
			break;
		case TelephonyManager.SIM_STATE_PIN_REQUIRED:
			mTelephonyText.append(mContext.getString(R.string.postfix_sim_state_pin_required) + "\n");
			break;
		case TelephonyManager.SIM_STATE_PUK_REQUIRED:
			mTelephonyText.append(mContext.getString(R.string.postfix_sim_state_puk_required) + "\n");
			break;
		case TelephonyManager.SIM_STATE_READY:
			mTelephonyText.append(mContext.getString(R.string.postfix_sim_state_ready) + "\n");
			break;
		case TelephonyManager.SIM_STATE_UNKNOWN:
			mTelephonyText.append(mContext.getString(R.string.postfix_sim_state_unknown) + "\n");
			break;
		}
		mTelephonyText.append(mContext.getString(R.string.prefix_sim_country_iso) + telephonyManager.getSimCountryIso() + "\n");
		mTelephonyText.append(mContext.getString(R.string.prefix_sim_operation) + telephonyManager.getSimOperator() + "\n");
		mTelephonyText.append(mContext.getString(R.string.prefix_sim_operation_name) + telephonyManager.getSimOperatorName() + "\n");
		mTelephonyText.append(mContext.getString(R.string.prefix_sim_serial_number) + telephonyManager.getSimSerialNumber());
	}
	
	private void setStorageInfo() {
        StatFs internalStat = new StatFs(Environment.getDataDirectory().getPath());
        long freeInternalBytes = (long)internalStat.getAvailableBlocks() * (long)internalStat.getBlockSize();
        long totalInternalBytes = (long)internalStat.getBlockCount() * (long)internalStat.getBlockSize();
        
        mStorageInternalText.setText(mContext.getString(R.string.prefix_internal_storage_total) + Util.dataFormatSize(totalInternalBytes, true) + "\n");
        mStorageInternalText.append(mContext.getString(R.string.prefix_internal_storage_used) + Util.dataFormatSize(totalInternalBytes - freeInternalBytes, true) + "\n");
        mStorageInternalText.append(mContext.getString(R.string.prefix_internal_storage_free) + Util.dataFormatSize(freeInternalBytes, true));
        
        mStorageInternalProgress.setProgress((int)((totalInternalBytes - freeInternalBytes) * 100 / totalInternalBytes));
        
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			StatFs externalStat = new StatFs(Environment.getExternalStorageDirectory().getPath());
			long freeExternalBytes = (long)externalStat.getAvailableBlocks() * (long)externalStat.getBlockSize();
			long totalExternalBytes =  (long)externalStat.getBlockCount() * (long)externalStat.getBlockSize();
			
			mStorageExternalText.setText(mContext.getString(R.string.prefix_external_storage_total) + Util.dataFormatSize(totalExternalBytes, true) + "\n");
			mStorageExternalText.append(mContext.getString(R.string.prefix_external_storage_used) + Util.dataFormatSize(totalExternalBytes - freeExternalBytes, true) + "\n");
			mStorageExternalText.append(mContext.getString(R.string.prefix_external_storage_free) + Util.dataFormatSize(freeExternalBytes, true));
			
			mStorageExternalProgress.setProgress((int)((totalExternalBytes - freeExternalBytes) * 100 / totalExternalBytes));
		} else {
			mStorageExternalText.setText(mContext.getString(R.string.prefix_external_storage_total) + mContext.getString(R.string.postfix_external_storage_not_mounted));
			
			mStorageExternalProgress.setProgress(100);
		}
	}
	
	public void startResourceWatcher() {
		if (mResourceWatcher != null) {
			if (mResourceWatcher.isRunning())
				mResourceWatcher.stop();
			
			mResourceWatcher = null;
		}
		
		mResourceWatcher = new ResourceWatcher();
		mResourceWatcher.execute();
	}
	
	public void stopResourceWatcher() {
		if (mResourceWatcher != null) {
			if (mResourceWatcher.isRunning())
				mResourceWatcher.stop();
			
			mResourceWatcher = null;
		}
	}	
	
	private class ResourceWatcher extends AsyncTask<Void, String, Void> {
		private int period = 3;
		private boolean run = true;
		
		private static final String GET_STAT_INFO = "cat /proc/stat";
		private static final String GET_MEMORY_INFO = "cat /proc/meminfo";
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			Runtime runtime = Runtime.getRuntime();
			Process process = null;
			BufferedReader reader = null;
			
			String[] progress = new String[6];
			
			while (run) {
				try {
					process = runtime.exec(GET_STAT_INFO);
					reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
					
					String[] cpuUsage = reader.readLine().split(" ");
					progress[0] = cpuUsage[2];
					progress[1] = cpuUsage[3];
					progress[2] = cpuUsage[4];
					progress[3] = cpuUsage[5];
					
					reader.close();
					reader = null;
					process = null;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					process = runtime.exec(GET_MEMORY_INFO);
					reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
					
					String[] memTotal = reader.readLine().replace(" ", "").split(":");
					String[] memFree = reader.readLine().replace(" ", "").split(":");
					progress[4] = memTotal[1].toUpperCase().replace(mContext.getString(R.string.postfix_data_size_kb), "");
					progress[5] = memFree[1].toUpperCase().replace(mContext.getString(R.string.postfix_data_size_kb), "");
					
					reader.close();
					reader = null;
					process = null;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				publishProgress(progress);
				
				try {
					Thread.sleep(period * 1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			return null;
		}
		
		private long preUser = -1;
		private long preNice = -1;
		private long preSystem = -1;
		private long preIdle = -1;
		
		@Override
		protected void onProgressUpdate(String... values) {
			// TODO Auto-generated method stub
			long curUser = Long.parseLong(values[0]);
			long curNice = Long.parseLong(values[1]);
			long curSystem = Long.parseLong(values[2]);
			long curIdle = Long.parseLong(values[3]);
			
			long memTotal = Long.parseLong(values[4]) * 1024;
			long memFree = Long.parseLong(values[5]) * 1024;
			
			if (preUser < 0 && preNice < 0 && preSystem < 0) {
				mResourceCpuText.setText(mContext.getString(R.string.prefix_cpu_user) + "0%\n");
				mResourceCpuText.append(mContext.getString(R.string.prefix_cpu_nice) + "0%\n");
				mResourceCpuText.append(mContext.getString(R.string.prefix_cpu_system) + "0%\n");
				mResourceCpuText.append(mContext.getString(R.string.prefix_cpu_idle) + "100%");
				
				mResourceCpuProgress.setProgress(0);
			} else {
				long total = ((curUser - preUser) + (curNice - preNice) + (curSystem - preSystem) + (curIdle - preIdle));
				long user = (curUser - preUser) * 100 / total;
				long nice = (curNice - preNice) * 100 / total;
				long system = (curSystem - preSystem) * 100 / total;
				
				mResourceCpuText.setText(mContext.getString(R.string.prefix_cpu_user) + user + "%\n");
				mResourceCpuText.append(mContext.getString(R.string.prefix_cpu_nice) + nice + "%\n");
				mResourceCpuText.append(mContext.getString(R.string.prefix_cpu_system) + system + "%\n");
				mResourceCpuText.append(mContext.getString(R.string.prefix_cpu_idle) + (100 - user - nice - system) + "%");
				
				mResourceCpuProgress.setProgress((int)(user + nice + system));
			}
			
			preUser = curUser;
			preNice = curNice;
			preSystem = curSystem;
			preIdle = curIdle;
			
			mResourceMemoryText.setText(mContext.getString(R.string.prefix_memory_total) + Util.dataFormatSize(memTotal, true) + "\n");
			mResourceMemoryText.append(mContext.getString(R.string.prefix_memory_used) + Util.dataFormatSize(memTotal - memFree, true) + "\n");
			mResourceMemoryText.append(mContext.getString(R.string.prefix_memory_free) + Util.dataFormatSize(memFree, true));
			
			mResourceMemoryProgress.setProgress((int)((memTotal - memFree) * 100 / memTotal));
		}			
		
		public void stop() {
			run = false;
		}
		
		public boolean isRunning() {
			return run;
		}
	}
}
