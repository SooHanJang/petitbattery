package madcat.studio.schedule.dialogs;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.schedule.constants.ScheduleConstants;
import madcat.studio.utils.Util;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class BrightnessDialog extends Dialog {
	private static final int PADDING = 15;
	
	private LinearLayout mDialogLayout = null;
	private RadioGroup mBtnGroup = null;
	private RadioButton mAutoBtn = null, mMinBtn = null, mMidBtn = null, mMaxBtn = null;
	private Button mOkBtn = null, mCancelBtn = null;
	
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	private int mSelectedValue = ScheduleConstants.DELAY_NORMAL;
	private boolean mFlag = false;
	
	public BrightnessDialog(Context context, int styleId, int applicationTheme, int value) {
		// TODO Auto-generated constructor stub
		super(context, styleId);
		mApplicationTheme = applicationTheme;
		mSelectedValue = value;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_schedule_brightness);
		
		mDialogLayout = (LinearLayout)findViewById(R.id.dialog_schedule_brightness_layout);
		
		mBtnGroup = (RadioGroup)findViewById(R.id.dialog_schedule_brightness_group);
		mAutoBtn = (RadioButton)findViewById(R.id.dialog_schedule_brightness_auto);
		mMinBtn = (RadioButton)findViewById(R.id.dialog_schedule_brightness_min);
		mMidBtn = (RadioButton)findViewById(R.id.dialog_schedule_brightness_mid);
		mMaxBtn = (RadioButton)findViewById(R.id.dialog_schedule_brightness_max);
		
		mOkBtn = (Button)findViewById(R.id.dialog_schedule_brightness_ok);
		mCancelBtn = (Button)findViewById(R.id.dialog_schedule_brightness_cancel);
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mDialogLayout.setBackgroundResource(R.drawable.dialog_background_pisces);

			mMinBtn.setBackgroundResource(R.drawable.com_preference_left_round_btn_pisces);
			mMidBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_pisces);
			mMaxBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_pisces);
			mAutoBtn.setBackgroundResource(R.drawable.com_preference_right_round_btn_pisces);
			
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mDialogLayout.setBackgroundResource(R.drawable.dialog_background_aries);
			
			mMinBtn.setBackgroundResource(R.drawable.com_preference_left_round_btn_aries);
			mMidBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_aries);
			mMaxBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_aries);
			mAutoBtn.setBackgroundResource(R.drawable.com_preference_right_round_btn_aries);
			
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mDialogLayout.setBackgroundResource(R.drawable.dialog_background_taurus);
			
			mMinBtn.setBackgroundResource(R.drawable.com_preference_left_round_btn_taurus);
			mMidBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_taurus);
			mMaxBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_taurus);
			mAutoBtn.setBackgroundResource(R.drawable.com_preference_right_round_btn_taurus);
			
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		ClickEvnetHandler clickEventHandler = new ClickEvnetHandler();
		
		mOkBtn.setOnClickListener(clickEventHandler);
		mCancelBtn.setOnClickListener(clickEventHandler);
		
		mBtnGroup.setOnCheckedChangeListener(new CheckEventHandler());
		
		switch (mSelectedValue) {
		case ScheduleConstants.SCREEN_BRIGHTNESS_AUTO:
			mAutoBtn.setChecked(true);
			break;
		case ScheduleConstants.SCREEN_BRIGHTNESS_MIN:
			mMinBtn.setChecked(true);
			break;
		case ScheduleConstants.SCREEN_BRIGHTNESS_MID:
			mMidBtn.setChecked(true);
			break;
		case ScheduleConstants.SCREEN_BRIGHTNESS_MAX:
			mMaxBtn.setChecked(true);
			break;
		}
		
		mDialogLayout.setPadding(PADDING, PADDING, PADDING, PADDING);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		dismiss();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onStop();
	}
	
	public int getValue() {
		return mSelectedValue;
	}
	
	public boolean isSelected() {
		return mFlag;
	}
	
	private class ClickEvnetHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.dialog_schedule_brightness_ok:
				mFlag = true;
				dismiss();
				break;
			case R.id.dialog_schedule_brightness_cancel:
				mFlag = false;
				dismiss();
				break;
			}
		}
	}
	
	private class CheckEventHandler implements RadioGroup.OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			switch (checkedId) {
			case R.id.dialog_schedule_brightness_auto:
				mSelectedValue = ScheduleConstants.SCREEN_BRIGHTNESS_AUTO;
				break;
			case R.id.dialog_schedule_brightness_min:
				mSelectedValue = ScheduleConstants.SCREEN_BRIGHTNESS_MIN;
				break;
			case R.id.dialog_schedule_brightness_mid:
				mSelectedValue = ScheduleConstants.SCREEN_BRIGHTNESS_MID;
				break;
			case R.id.dialog_schedule_brightness_max:
				mSelectedValue = ScheduleConstants.SCREEN_BRIGHTNESS_MAX;
				break;
			}
		}
	}
}
