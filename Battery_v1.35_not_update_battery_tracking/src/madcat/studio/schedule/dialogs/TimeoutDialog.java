package madcat.studio.schedule.dialogs;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.schedule.constants.ScheduleConstants;
import madcat.studio.utils.Util;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class TimeoutDialog extends Dialog {
	private static final int PADDING = 15;
	
	private LinearLayout mDialogLayout = null;
	private RadioGroup mBtnGroup = null;
	private RadioButton mShortestBtn = null, mShortBtn = null, mNormalBtn = null, mLongBtn = null, mLongestBtn = null;
	private Button mOkBtn = null, mCancelBtn = null;
	
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	private int mSelectedValue = ScheduleConstants.SCREEN_TIMEOUT_NORMAL;
	private boolean mFlag = false;
	
	public TimeoutDialog(Context context, int styleId, int applicationTheme, int value) {
		// TODO Auto-generated constructor stub
		super(context, styleId);
		mApplicationTheme = applicationTheme;
		mSelectedValue = value;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.75f;
		getWindow().setAttributes(lpWindow);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_schedule_timeout);
		
		mDialogLayout = (LinearLayout)findViewById(R.id.dialog_schedule_timeout_layout);
		
		mBtnGroup = (RadioGroup)findViewById(R.id.dialog_schedule_timeout_group);
		mShortestBtn = (RadioButton)findViewById(R.id.dialog_schedule_timeout_shortest);
		mShortBtn = (RadioButton)findViewById(R.id.dialog_schedule_timeout_short);
		mNormalBtn = (RadioButton)findViewById(R.id.dialog_schedule_timeout_normal);
		mLongBtn = (RadioButton)findViewById(R.id.dialog_schedule_timeout_long);
		mLongestBtn = (RadioButton)findViewById(R.id.dialog_schedule_timeout_longest);
		
		mOkBtn = (Button)findViewById(R.id.dialog_schedule_timeout_ok);
		mCancelBtn = (Button)findViewById(R.id.dialog_schedule_timeout_cancel);
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mDialogLayout.setBackgroundResource(R.drawable.dialog_background_pisces);
			
			mShortestBtn.setBackgroundResource(R.drawable.com_preference_left_round_btn_pisces);
			mShortBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_pisces);
			mNormalBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_pisces);
			mLongBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_pisces);
			mLongestBtn.setBackgroundResource(R.drawable.com_preference_right_round_btn_pisces);
			
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mDialogLayout.setBackgroundResource(R.drawable.dialog_background_aries);
			
			mShortestBtn.setBackgroundResource(R.drawable.com_preference_left_round_btn_aries);
			mShortBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_aries);
			mNormalBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_aries);
			mLongBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_aries);
			mLongestBtn.setBackgroundResource(R.drawable.com_preference_right_round_btn_aries);
			
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mDialogLayout.setBackgroundResource(R.drawable.dialog_background_taurus);
			
			mShortestBtn.setBackgroundResource(R.drawable.com_preference_left_round_btn_taurus);
			mShortBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_taurus);
			mNormalBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_taurus);
			mLongBtn.setBackgroundResource(R.drawable.com_preference_rectangle_btn_taurus);
			mLongestBtn.setBackgroundResource(R.drawable.com_preference_right_round_btn_taurus);
			
			mOkBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mCancelBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		ClickEvnetHandler clickEventHandler = new ClickEvnetHandler();
		
		mOkBtn.setOnClickListener(clickEventHandler);
		mCancelBtn.setOnClickListener(clickEventHandler);
		
		mBtnGroup.setOnCheckedChangeListener(new CheckEventHandler());
		
		switch (mSelectedValue) {
		case ScheduleConstants.SCREEN_TIMEOUT_SHORTEST:
			mShortestBtn.setChecked(true);
			break;
		case ScheduleConstants.SCREEN_TIMEOUT_SHORT:
			mShortBtn.setChecked(true);
			break;
		case ScheduleConstants.SCREEN_TIMEOUT_NORMAL:
			mNormalBtn.setChecked(true);
			break;
		case ScheduleConstants.SCREEN_TIMEOUT_LONG:
			mLongBtn.setChecked(true);
			break;
		case ScheduleConstants.SCREEN_TIMEOUT_LONGEST:
			mLongestBtn.setChecked(true);
			break;
		}
		
		mDialogLayout.setPadding(PADDING, PADDING, PADDING, PADDING);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		dismiss();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onStop();
	}
	
	public int getValue() {
		return mSelectedValue;
	}
	
	public boolean isSelected() {
		return mFlag;
	}
	
	private class ClickEvnetHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.dialog_schedule_timeout_ok:
				mFlag = true;
				dismiss();
				break;
			case R.id.dialog_schedule_timeout_cancel:
				mFlag = false;
				dismiss();
				break;
			}
		}
	}
	
	private class CheckEventHandler implements RadioGroup.OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			switch (checkedId) {
			case R.id.dialog_schedule_timeout_shortest:
				mSelectedValue = ScheduleConstants.SCREEN_TIMEOUT_SHORTEST;
				break;
			case R.id.dialog_schedule_timeout_short:
				mSelectedValue = ScheduleConstants.SCREEN_TIMEOUT_SHORT;
				break;
			case R.id.dialog_schedule_timeout_normal:
				mSelectedValue = ScheduleConstants.SCREEN_TIMEOUT_NORMAL;
				break;
			case R.id.dialog_schedule_timeout_long:
				mSelectedValue = ScheduleConstants.SCREEN_TIMEOUT_LONG;
				break;
			case R.id.dialog_schedule_timeout_longest:
				mSelectedValue = ScheduleConstants.SCREEN_TIMEOUT_LONGEST;
				break;
			}
		}
	}
}