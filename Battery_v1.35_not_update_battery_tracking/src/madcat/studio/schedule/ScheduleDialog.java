package madcat.studio.schedule;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.schedule.constants.ScheduleConstants;
import madcat.studio.schedule.layouts.GeneralScheduleLayout;
import madcat.studio.schedule.layouts.MaximumScheduleLayout;
import madcat.studio.schedule.layouts.MinimumScheduleLayout;
import madcat.studio.schedule.layouts.PowerScheduleLayout;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class ScheduleDialog extends Dialog {
	private Context mContext = null;
	
	private LinearLayout mMainLayout = null;
	
	private TextView mTitleText = null;
	private Button mSaveButton = null;
	private Button mCancelButton = null;
	
	private ImageView mDivider = null;
	
	private LinearLayout mScheduleLayout = null;
	
	private LinearLayout.LayoutParams mLayoutParams;
	
	private PowerScheduleLayout mPowerLayout = null;
	private MaximumScheduleLayout mMaximumLayout = null;
	private GeneralScheduleLayout mGeneralLayout = null;
	private MinimumScheduleLayout mMinimumLayout = null;
	
	private SharedPreferences mPref = null;
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	private Integer[] mMaximumRange = null;
	private Integer[] mGeneralRange = null;
	private Integer[] mMinimumRange = null;
	
	private Integer[] mServiceDelay = null;
	private Boolean[] mIsWifiControl = null;
	private Boolean[] mIsBlueToothControl = null;
	private Boolean[] mIsProcessControl = null;
	private Boolean[] mIsScreenControl = null;
	private Integer[] mScreenBrightness = null;
	private Integer[] mScreenTimeout = null;
	
	private boolean mIsSave = false;
	
	public ScheduleDialog(Context context, int styleId) {
		// TODO Auto-generated constructor stub
		super(context, styleId);
		
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_schedule);
		
		mMainLayout = (LinearLayout)findViewById(R.id.dialog_schedule_main_layout);
		
		mTitleText = (TextView)findViewById(R.id.dialog_schedule_title_text);
		mSaveButton = (Button)findViewById(R.id.dialog_schedule_save_btn);
		mCancelButton = (Button)findViewById(R.id.dialog_schedule_cancel_btn);

		mDivider = (ImageView)findViewById(R.id.dialog_schedule_divider);
		
		mScheduleLayout = (LinearLayout)findViewById(R.id.dialog_schedule_layout);
		
		mPref = mContext.getSharedPreferences(Constants.PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mApplicationTheme = mPref.getInt(Constants.APPLICATION_THEME, Constants.APPLICATION_THEME_TAURUS);
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mMainLayout.setBackgroundResource(R.drawable.background_pisces);
			mTitleText.setTextColor(mContext.getResources().getColor(R.color.white));
			mSaveButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mCancelButton.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mDivider.setBackgroundResource(R.color.pisces_blue);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mMainLayout.setBackgroundResource(R.drawable.background_aries);
			mTitleText.setTextColor(mContext.getResources().getColor(R.color.aries_pink));
			mSaveButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mCancelButton.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mDivider.setBackgroundResource(R.color.aries_pink);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mMainLayout.setBackgroundResource(R.drawable.background_taurus);
			mTitleText.setTextColor(mContext.getResources().getColor(R.color.taurus_brown));
			mSaveButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mCancelButton.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mDivider.setBackgroundResource(R.color.taurus_brown);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		mLayoutParams = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		mPowerLayout = new PowerScheduleLayout(mContext, mApplicationTheme);
		mMaximumLayout = new MaximumScheduleLayout(mContext, mApplicationTheme);
		mGeneralLayout = new GeneralScheduleLayout(mContext, mApplicationTheme);
		mMinimumLayout = new MinimumScheduleLayout(mContext, mApplicationTheme);
		
		ClickEventHandler clickEventHandler = new ClickEventHandler();
		mSaveButton.setOnClickListener(clickEventHandler);
		mCancelButton.setOnClickListener(clickEventHandler);
		
		initialize();
		
		mScheduleLayout.addView(mPowerLayout, mLayoutParams);
		mScheduleLayout.addView(mMaximumLayout, mLayoutParams);
		mScheduleLayout.addView(mGeneralLayout, mLayoutParams);
		mScheduleLayout.addView(mMinimumLayout, mLayoutParams);
	}
	
	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
	}
	
	public boolean isSave() {
		return mIsSave;
	}
	
	private void initialize() {
		mMaximumRange = new Integer[2];
		mGeneralRange = new Integer[2];
		mMinimumRange = new Integer[2];
		
		mServiceDelay = new Integer[4];
		mIsWifiControl = new Boolean[4];
		mIsBlueToothControl = new Boolean[4];
		mIsProcessControl = new Boolean[4];
		mIsScreenControl = new Boolean[4];
		mScreenBrightness = new Integer[4];
		mScreenTimeout = new Integer[4];
		
		mMaximumRange[0] = mPref.getInt(ScheduleConstants.SCHEDULE_RANGE_MAXIMUM_START, ScheduleConstants.MAXIMUM_RANGE_START);
		mMaximumRange[1] = mPref.getInt(ScheduleConstants.SCHEDULE_RANGE_MAXIMUM_END, ScheduleConstants.MAXIMUM_RANGE_END);
		
		mGeneralRange[0] = mPref.getInt(ScheduleConstants.SCHEDULE_RANGE_GENERAL_START, ScheduleConstants.GENERAL_RANGE_START);
		mGeneralRange[1] = mPref.getInt(ScheduleConstants.SCHEDULE_RANGE_GENERAL_END, ScheduleConstants.GENERAL_RANGE_END);
		
		mMinimumRange[0] = mPref.getInt(ScheduleConstants.SCHEDULE_RANGE_MINIMUM_START, ScheduleConstants.MINIMUM_RANGE_START);
		mMinimumRange[1] = mPref.getInt(ScheduleConstants.SCHEDULE_RANGE_MINIMUM_END, ScheduleConstants.MINIMUM_RANGE_END);
		
		mServiceDelay[0] = mPref.getInt(ScheduleConstants.SCHEDULE_DELAY_POWER, ScheduleConstants.DELAY_SHORTEST);
		mServiceDelay[1] = mPref.getInt(ScheduleConstants.SCHEDULE_DELAY_MAXIMUM, ScheduleConstants.DELAY_SHORTEST);
		mServiceDelay[2] = mPref.getInt(ScheduleConstants.SCHEDULE_DELAY_GENERAL, ScheduleConstants.DELAY_SHORT);
		mServiceDelay[3] = mPref.getInt(ScheduleConstants.SCHEDULE_DELAY_MINIMUM, ScheduleConstants.DELAY_LONG);
		
		mIsWifiControl[0] = mPref.getBoolean(ScheduleConstants.SCHEDULE_WIFI_POWER, false);
		mIsWifiControl[1] = mPref.getBoolean(ScheduleConstants.SCHEDULE_WIFI_MAXIMUM, false);
		mIsWifiControl[2] = mPref.getBoolean(ScheduleConstants.SCHEDULE_WIFI_GENERAL, true);
		mIsWifiControl[3] = mPref.getBoolean(ScheduleConstants.SCHEDULE_WIFI_MINIMUM, true);
		
		mIsBlueToothControl[0] = mPref.getBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_POWER, false);
		mIsBlueToothControl[1] = mPref.getBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_MAXIMUM, false);
		mIsBlueToothControl[2] = mPref.getBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_GENERAL, true);
		mIsBlueToothControl[3] = mPref.getBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_MINIMUM, true);
		
		mIsProcessControl[0] = mPref.getBoolean(ScheduleConstants.SCHEDULE_PROCESS_POWER, false);
		mIsProcessControl[1] = mPref.getBoolean(ScheduleConstants.SCHEDULE_PROCESS_MAXIMUM, false);
		mIsProcessControl[2] = mPref.getBoolean(ScheduleConstants.SCHEDULE_PROCESS_GENERAL, false);
		mIsProcessControl[3] = mPref.getBoolean(ScheduleConstants.SCHEDULE_PROCESS_MINIMUM, false);
		
		mIsScreenControl[0] = mPref.getBoolean(ScheduleConstants.SCHEDULE_SCREEN_POWER, true);
		mIsScreenControl[1] = mPref.getBoolean(ScheduleConstants.SCHEDULE_SCREEN_MAXIMUM, true);
		mIsScreenControl[2] = mPref.getBoolean(ScheduleConstants.SCHEDULE_SCREEN_GENERAL, true);
		mIsScreenControl[3] = mPref.getBoolean(ScheduleConstants.SCHEDULE_SCREEN_MINIMUM, true);
		
		mScreenBrightness[0] = mPref.getInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_POWER, ScheduleConstants.SCREEN_BRIGHTNESS_MAX);
		mScreenBrightness[1] = mPref.getInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_MAXIMUM, ScheduleConstants.SCREEN_BRIGHTNESS_MAX);
		mScreenBrightness[2] = mPref.getInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_GENERAL, ScheduleConstants.SCREEN_BRIGHTNESS_MID);
		mScreenBrightness[3] = mPref.getInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_MINIMUM, ScheduleConstants.SCREEN_BRIGHTNESS_MIN);
		
		mScreenTimeout[0] = mPref.getInt(ScheduleConstants.SCHEDULE_TIMEOUT_POWER, ScheduleConstants.SCREEN_TIMEOUT_LONGEST);
		mScreenTimeout[1] = mPref.getInt(ScheduleConstants.SCHEDULE_TIMEOUT_MAXIMUM, ScheduleConstants.SCREEN_TIMEOUT_LONG);
		mScreenTimeout[2] = mPref.getInt(ScheduleConstants.SCHEDULE_TIMEOUT_GENERAL, ScheduleConstants.SCREEN_TIMEOUT_NORMAL);
		mScreenTimeout[3] = mPref.getInt(ScheduleConstants.SCHEDULE_TIMEOUT_MINIMUM, ScheduleConstants.SCREEN_TIMEOUT_SHORT);
		
		mPowerLayout.setDelay(mServiceDelay[0]);
		mPowerLayout.setWifiControl(mIsWifiControl[0]);
		mPowerLayout.setBlueToothControl(mIsBlueToothControl[0]);
		mPowerLayout.setProcessControl(mIsProcessControl[0]);
		mPowerLayout.setScreenControl(mIsScreenControl[0]);
		mPowerLayout.setBrightness(mScreenBrightness[0]);
		mPowerLayout.setTimeout(mScreenTimeout[0]);
		mPowerLayout.setExpand(true);
		
		mMaximumLayout.setDelay(mServiceDelay[1]);
		mMaximumLayout.setWifiControl(mIsWifiControl[1]);
		mMaximumLayout.setBlueToothControl(mIsBlueToothControl[1]);
		mMaximumLayout.setProcessControl(mIsProcessControl[1]);
		mMaximumLayout.setScreenControl(mIsScreenControl[1]);
		mMaximumLayout.setBrightness(mScreenBrightness[1]);
		mMaximumLayout.setTimeout(mScreenTimeout[1]);
		mMaximumLayout.setExpand(false);
		
		mGeneralLayout.setDelay(mServiceDelay[2]);
		mGeneralLayout.setWifiControl(mIsWifiControl[2]);
		mGeneralLayout.setBlueToothControl(mIsBlueToothControl[2]);
		mGeneralLayout.setProcessControl(mIsProcessControl[2]);
		mGeneralLayout.setScreenControl(mIsScreenControl[2]);
		mGeneralLayout.setBrightness(mScreenBrightness[2]);
		mGeneralLayout.setTimeout(mScreenTimeout[2]);
		mGeneralLayout.setExpand(false);
		
		mMinimumLayout.setDelay(mServiceDelay[3]);
		mMinimumLayout.setWifiControl(mIsWifiControl[3]);
		mMinimumLayout.setBlueToothControl(mIsBlueToothControl[3]);
		mMinimumLayout.setProcessControl(mIsProcessControl[3]);
		mMinimumLayout.setScreenControl(mIsScreenControl[3]);
		mMinimumLayout.setBrightness(mScreenBrightness[3]);
		mMinimumLayout.setTimeout(mScreenTimeout[3]);
		mMinimumLayout.setExpand(false);
	}
	
	private class ClickEventHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.dialog_schedule_save_btn:
				SharedPreferences.Editor editor = mPref.edit();
				
				editor.putInt(ScheduleConstants.SCHEDULE_DELAY_POWER, mPowerLayout.getDelay());
				editor.putBoolean(ScheduleConstants.SCHEDULE_WIFI_POWER, mPowerLayout.isWifiControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_POWER, mPowerLayout.isBlueToothControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_PROCESS_POWER, mPowerLayout.isProcessControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_SCREEN_POWER, mPowerLayout.isScreenControl());
				editor.putInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_POWER, mPowerLayout.getBrightness());
				editor.putInt(ScheduleConstants.SCHEDULE_TIMEOUT_POWER, mPowerLayout.getTimeout());
				
				editor.putInt(ScheduleConstants.SCHEDULE_DELAY_MAXIMUM, mMaximumLayout.getDelay());
				editor.putBoolean(ScheduleConstants.SCHEDULE_WIFI_MAXIMUM, mMaximumLayout.isWifiControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_MAXIMUM, mMaximumLayout.isBlueToothControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_PROCESS_MAXIMUM, mMaximumLayout.isProcessControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_SCREEN_MAXIMUM, mMaximumLayout.isScreenControl());
				editor.putInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_MAXIMUM, mMaximumLayout.getBrightness());
				editor.putInt(ScheduleConstants.SCHEDULE_TIMEOUT_MAXIMUM, mMaximumLayout.getTimeout());
				
				editor.putInt(ScheduleConstants.SCHEDULE_DELAY_GENERAL, mGeneralLayout.getDelay());
				editor.putBoolean(ScheduleConstants.SCHEDULE_WIFI_GENERAL, mGeneralLayout.isWifiControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_GENERAL, mGeneralLayout.isBlueToothControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_PROCESS_GENERAL, mGeneralLayout.isProcessControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_SCREEN_GENERAL, mGeneralLayout.isScreenControl());
				editor.putInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_GENERAL, mGeneralLayout.getBrightness());
				editor.putInt(ScheduleConstants.SCHEDULE_TIMEOUT_GENERAL, mGeneralLayout.getTimeout());
				
				editor.putInt(ScheduleConstants.SCHEDULE_DELAY_MINIMUM, mMinimumLayout.getDelay());
				editor.putBoolean(ScheduleConstants.SCHEDULE_WIFI_MINIMUM, mMinimumLayout.isWifiControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_BLUETOOTH_MINIMUM, mMinimumLayout.isBlueToothControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_PROCESS_MINIMUM, mMinimumLayout.isProcessControl());
				editor.putBoolean(ScheduleConstants.SCHEDULE_SCREEN_MINIMUM, mMinimumLayout.isScreenControl());
				editor.putInt(ScheduleConstants.SCHEDULE_BRIGHTNESS_MINIMUM, mMinimumLayout.getBrightness());
				editor.putInt(ScheduleConstants.SCHEDULE_TIMEOUT_MINIMUM, mMinimumLayout.getTimeout());
				
				editor.putBoolean(Constants.OPTIMIZATION_TYPE, Constants.OPTIMIZATION_TYPE_CUSTOM);
				
				editor.commit();
				
				mIsSave = true;
				dismiss();
				break;
			case R.id.dialog_schedule_cancel_btn:
				mIsSave = false;
				dismiss();
				break;
			}
		}
	}
}
