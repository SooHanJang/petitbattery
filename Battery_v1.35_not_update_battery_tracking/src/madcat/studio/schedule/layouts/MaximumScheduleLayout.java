package madcat.studio.schedule.layouts;

import madcat.studio.battery.pro.R;
import madcat.studio.constants.Constants;
import madcat.studio.schedule.constants.ScheduleConstants;
import madcat.studio.schedule.dialogs.BrightnessDialog;
import madcat.studio.schedule.dialogs.DelayDialog;
import madcat.studio.schedule.dialogs.TimeoutDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MaximumScheduleLayout extends LinearLayout {
	private Context mContext = null;
	private int mApplicationTheme = Constants.APPLICATION_THEME_BASIC;
	
	private LinearLayout mScheduleLayout = null;
	private TextView mScheduleTitleText = null, mRangeText = null, mDelayText = null, mWifiText = null, mBlueToothText = null, 
					mProcessText = null, mScreenText = null, mBrightnessText = null, mTimeoutText = null;
	private Button mRangeBtn = null, mDelayBtn = null, mBrightnessBtn = null, mTimeoutBtn = null;
	private CheckBox mShowCheckBox = null, mWifiCheckBox = null, mBlueToothCheckBox = null, mProcessCheckBox = null, mScreenCheckBox = null;
	private ImageView mAboveDivider = null, mBelowDivider = null;
	
	public MaximumScheduleLayout(Context context, int applicationTheme) {
		// TODO Auto-generated constructor stub
		super(context);
		
		mContext = context;
		mApplicationTheme = applicationTheme;
		
		LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		layoutInflater.inflate(R.layout.layout_schedule_maximum, this, true);
		
		mScheduleLayout = (LinearLayout)findViewById(R.id.layout_schedule_maximum_layout);
		mScheduleTitleText = (TextView)findViewById(R.id.layout_schedule_maximum_title_text);
		mRangeText = (TextView)findViewById(R.id.layout_schedule_maximum_range_text);
		mDelayText = (TextView)findViewById(R.id.layout_schedule_maximum_delay_text);
		mWifiText = (TextView)findViewById(R.id.layout_schedule_maximum_wifi_text);
		mBlueToothText = (TextView)findViewById(R.id.layout_schedule_maximum_bluetooth_text);
		mProcessText = (TextView)findViewById(R.id.layout_schedule_maximum_process_text);
		mScreenText = (TextView)findViewById(R.id.layout_schedule_maximum_screen_text);
		mBrightnessText = (TextView)findViewById(R.id.layout_schedule_maximum_brightness_text);
		mTimeoutText = (TextView)findViewById(R.id.layout_schedule_maximum_timeout_text);
		
		mRangeBtn = (Button)findViewById(R.id.layout_schedule_maximum_range_btn);
		mDelayBtn = (Button)findViewById(R.id.layout_schedule_maximum_delay_btn);
		mBrightnessBtn = (Button)findViewById(R.id.layout_schedule_maximum_brightness_btn);
		mTimeoutBtn = (Button)findViewById(R.id.layout_schedule_maximum_timeout_btn);
		
		mShowCheckBox = (CheckBox)findViewById(R.id.layout_schedule_maximum_show_checkbox);
		mWifiCheckBox = (CheckBox)findViewById(R.id.layout_schedule_maximum_wifi_checkbox);
		mBlueToothCheckBox = (CheckBox)findViewById(R.id.layout_schedule_maximum_bluetooth_checkbox);
		mProcessCheckBox = (CheckBox)findViewById(R.id.layout_schedule_maximum_process_checkbox);
		mScreenCheckBox = (CheckBox)findViewById(R.id.layout_schedule_maximum_screen_checkbox);
		
		mAboveDivider = (ImageView)findViewById(R.id.layout_schedule_maximum_above_divider);
		mBelowDivider = (ImageView)findViewById(R.id.layout_schedule_maximum_below_divider);
		
		switch (mApplicationTheme) {
//		case Constants.APPLICATION_THEME_CAPRICORN:
//			break;
//		case Constants.APPLICATION_THEME_AQUARIUS:
//			break;
		case Constants.APPLICATION_THEME_PISCES:
			mScheduleTitleText.setTextColor(getResources().getColor(R.color.white));
			mRangeText.setTextColor(getResources().getColor(R.color.white));
			mDelayText.setTextColor(getResources().getColor(R.color.white));
			mWifiText.setTextColor(getResources().getColor(R.color.white));
			mBlueToothText.setTextColor(getResources().getColor(R.color.white));
			mProcessText.setTextColor(getResources().getColor(R.color.white));
			mScreenText.setTextColor(getResources().getColor(R.color.white));
			mBrightnessText.setTextColor(getResources().getColor(R.color.white));
			mTimeoutText.setTextColor(getResources().getColor(R.color.white));
			
			mRangeBtn.setBackgroundResource(R.drawable.preference_round_btn_pisces);
			mDelayBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mBrightnessBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			mTimeoutBtn.setBackgroundResource(R.drawable.com_preference_round_btn_pisces);
			
			mShowCheckBox.setBackgroundResource(R.drawable.com_preference_expand_btn_pisces);
			
			mAboveDivider.setBackgroundResource(R.color.pisces_blue);
			mBelowDivider.setBackgroundResource(R.color.pisces_blue);
			break;
		case Constants.APPLICATION_THEME_ARIES:
			mScheduleTitleText.setTextColor(getResources().getColor(R.color.aries_pink));
			mRangeText.setTextColor(getResources().getColor(R.color.aries_pink));
			mDelayText.setTextColor(getResources().getColor(R.color.aries_pink));
			mWifiText.setTextColor(getResources().getColor(R.color.aries_pink));
			mBlueToothText.setTextColor(getResources().getColor(R.color.aries_pink));
			mProcessText.setTextColor(getResources().getColor(R.color.aries_pink));
			mScreenText.setTextColor(getResources().getColor(R.color.aries_pink));
			mBrightnessText.setTextColor(getResources().getColor(R.color.aries_pink));
			mTimeoutText.setTextColor(getResources().getColor(R.color.aries_pink));
			
			mRangeBtn.setBackgroundResource(R.drawable.preference_round_btn_aries);
			mDelayBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mBrightnessBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			mTimeoutBtn.setBackgroundResource(R.drawable.com_preference_round_btn_aries);
			
			mShowCheckBox.setBackgroundResource(R.drawable.com_preference_expand_btn_aries);
			
			mAboveDivider.setBackgroundResource(R.color.aries_pink);
			mBelowDivider.setBackgroundResource(R.color.aries_pink);
			break;
		case Constants.APPLICATION_THEME_TAURUS:
			mScheduleTitleText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mRangeText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mDelayText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mWifiText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mBlueToothText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mProcessText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mScreenText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mBrightnessText.setTextColor(getResources().getColor(R.color.taurus_brown));
			mTimeoutText.setTextColor(getResources().getColor(R.color.taurus_brown));
			
			mRangeBtn.setBackgroundResource(R.drawable.preference_round_btn_taurus);
			mDelayBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mBrightnessBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			mTimeoutBtn.setBackgroundResource(R.drawable.com_preference_round_btn_taurus);
			
			mShowCheckBox.setBackgroundResource(R.drawable.com_preference_expand_btn_taurus);
			
			mAboveDivider.setBackgroundResource(R.color.taurus_brown);
			mBelowDivider.setBackgroundResource(R.color.taurus_brown);
			break;
//		case Constants.APPLICATION_THEME_GEMINI:
//			
//			break;
//		case Constants.APPLICATION_THEME_CANCER:
//			
//			break;
//		case Constants.APPLICATION_THEME_LEO:
//			
//			break;
//		case Constants.APPLICATION_THEME_VIRGO:
//			
//			break;
//		case Constants.APPLICATION_THEME_LIBRA:
//			
//			break;
//		case Constants.APPLICATION_THEME_SCORPIO:
//			
//			break;
//		case Constants.APPLICATION_THEME_ARCHER:
//			
//			break;
		}
		
		ClickEventHandler clickEventHandler = new ClickEventHandler();
		mDelayBtn.setOnClickListener(clickEventHandler);
		mBrightnessBtn.setOnClickListener(clickEventHandler);
		mTimeoutBtn.setOnClickListener(clickEventHandler);
		
		CheckEventHandler checkEventHandler = new CheckEventHandler();
		mShowCheckBox.setOnCheckedChangeListener(checkEventHandler);
		mScreenCheckBox.setOnCheckedChangeListener(checkEventHandler);
	}
	
	public void setExpand(boolean expand) {
		mShowCheckBox.setChecked(expand);
	}
	
	private int mDelay = 0;
	private int mBrightness = 0;
	private int mTimeout = 0;
	
	public void setDelay(int delay) {
		mDelay = delay;
		switch (mDelay) {
		case ScheduleConstants.DELAY_SHORTEST:
			mDelayBtn.setText(mContext.getString(R.string.delay_dialog_shortest));
			break;
		case ScheduleConstants.DELAY_SHORT:
			mDelayBtn.setText(mContext.getString(R.string.delay_dialog_short));
			break;
		case ScheduleConstants.DELAY_NORMAL:
			mDelayBtn.setText(mContext.getString(R.string.delay_dialog_normal));
			break;
		case ScheduleConstants.DELAY_LONG:
			mDelayBtn.setText(mContext.getString(R.string.delay_dialog_long));
			break;
		case ScheduleConstants.DELAY_LONGEST:
			mDelayBtn.setText(mContext.getString(R.string.delay_dialog_longest));
			break;
		}
	}
	
	public int getDelay() {
		return mDelay;
	}
	
	public void setBrightness(int brightness) {
		mBrightness = brightness;
		switch (mBrightness) {
		case ScheduleConstants.SCREEN_BRIGHTNESS_AUTO:
			mBrightnessBtn.setText(mContext.getString(R.string.brightness_dialog_auto));
			break;
		case ScheduleConstants.SCREEN_BRIGHTNESS_MIN:
			mBrightnessBtn.setText(mContext.getString(R.string.brightness_dialog_min));
			break;
		case ScheduleConstants.SCREEN_BRIGHTNESS_MID:
			mBrightnessBtn.setText(mContext.getString(R.string.brightness_dialog_mid));
			break;
		case ScheduleConstants.SCREEN_BRIGHTNESS_MAX:
			mBrightnessBtn.setText(mContext.getString(R.string.brightness_dialog_max));
			break;
		}
	}
	
	public int getBrightness() {
		return mBrightness;
	}
	
	public void setTimeout(int timeout) {
		mTimeout = timeout;
		switch (mTimeout) {
		case ScheduleConstants.SCREEN_TIMEOUT_SHORTEST:
			mTimeoutBtn.setText(mContext.getString(R.string.timeout_dialog_shortest));
			break;
		case ScheduleConstants.SCREEN_TIMEOUT_SHORT:
			mTimeoutBtn.setText(mContext.getString(R.string.timeout_dialog_short));
			break;
		case ScheduleConstants.SCREEN_TIMEOUT_NORMAL:
			mTimeoutBtn.setText(mContext.getString(R.string.timeout_dialog_normal));
			break;
		case ScheduleConstants.SCREEN_TIMEOUT_LONG:
			mTimeoutBtn.setText(mContext.getString(R.string.timeout_dialog_long));
			break;
		case ScheduleConstants.SCREEN_TIMEOUT_LONGEST:
			mTimeoutBtn.setText(mContext.getString(R.string.timeout_dialog_longest));
			break;
		}
	}
	
	public int getTimeout() {
		return mTimeout;
	}
	
	public void setWifiControl(boolean control) {
		mWifiCheckBox.setChecked(control);
	}
	
	public boolean isWifiControl() {
		return mWifiCheckBox.isChecked();
	}
	
	public void setBlueToothControl(boolean control) {
		mBlueToothCheckBox.setChecked(control);
	}
	
	public boolean isBlueToothControl() {
		return mBlueToothCheckBox.isChecked();
	}
	
	public void setProcessControl(boolean control) {
		mProcessCheckBox.setChecked(control);
	}
	
	public boolean isProcessControl() {
		return mProcessCheckBox.isChecked();
	}
	
	public void setScreenControl(boolean control) {
		mScreenCheckBox.setChecked(control);
	}
	
	public boolean isScreenControl() {
		return mScreenCheckBox.isChecked();
	}
	
	private class ClickEventHandler implements View.OnClickListener {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.layout_schedule_maximum_delay_btn:
				final DelayDialog delayDialog = new DelayDialog(mContext, R.style.PopupDialog, mApplicationTheme, mDelay);
				delayDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						if (delayDialog.isSelected()) {
							setDelay(delayDialog.getValue());
						}
					}
				});
				delayDialog.show();
				break;
			case R.id.layout_schedule_maximum_brightness_btn:
				final BrightnessDialog brightnessDialog = new BrightnessDialog(mContext, R.style.PopupDialog, mApplicationTheme, mBrightness);
				brightnessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						if (brightnessDialog.isSelected()) {
							setBrightness(brightnessDialog.getValue());
						}
					}
				});
				brightnessDialog.show();
				break;
			case R.id.layout_schedule_maximum_timeout_btn:
				final TimeoutDialog timeoutDialog = new TimeoutDialog(mContext, R.style.PopupDialog, mApplicationTheme, mTimeout);
				timeoutDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
					@Override
					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						if (timeoutDialog.isSelected()) {
							setTimeout(timeoutDialog.getValue());
						}
					}
				});
				timeoutDialog.show();
				break;
			}
		}
	}
	
	private class CheckEventHandler implements CompoundButton.OnCheckedChangeListener {
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// TODO Auto-generated method stub
			switch (buttonView.getId()) {
			case R.id.layout_schedule_maximum_show_checkbox:
				if (isChecked) {
					mScheduleLayout.setVisibility(VISIBLE);
				} else {
					mScheduleLayout.setVisibility(GONE);
				}
				break;
			case R.id.layout_schedule_maximum_screen_checkbox:
				mBrightnessBtn.setEnabled(isChecked);
				mTimeoutBtn.setEnabled(isChecked);
				break;
			}
		}
	}
}
