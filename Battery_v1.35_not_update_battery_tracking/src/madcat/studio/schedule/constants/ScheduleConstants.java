package madcat.studio.schedule.constants;

public class ScheduleConstants {
	//CUSTOM SERVICE VALUES.
	public static final String	SCHEDULE_DELAY_POWER				= "schedule_delay_pow";
	public static final String	SCHEDULE_DELAY_MAXIMUM				= "schedule_delay_max";
	public static final String	SCHEDULE_DELAY_GENERAL				= "schedule_delay_gen";
	public static final String	SCHEDULE_DELAY_MINIMUM				= "schedule_delay_min";
	
	public static final String	SCHEDULE_RANGE_MAXIMUM_START		= "schedule_range_max_start";
	public static final String	SCHEDULE_RANGE_MAXIMUM_END			= "schedule_range_max_end";
	
	public static final String	SCHEDULE_RANGE_GENERAL_START		= "schedule_range_gen_start";
	public static final String	SCHEDULE_RANGE_GENERAL_END			= "schedule_range_gen_end";
	
	public static final String	SCHEDULE_RANGE_MINIMUM_START		= "schedule_range_min_start";
	public static final String	SCHEDULE_RANGE_MINIMUM_END			= "schedule_range_min_end";
	
	public static final String	SCHEDULE_WIFI_POWER					= "schedule_wifi_pow";
	public static final String	SCHEDULE_WIFI_MAXIMUM				= "schedule_wifi_max";
	public static final String	SCHEDULE_WIFI_GENERAL				= "schedule_wifi_gen";
	public static final String	SCHEDULE_WIFI_MINIMUM				= "schedule_wifi_min";
	
	public static final String	SCHEDULE_BLUETOOTH_POWER			= "schedule_bluetooth_pow";
	public static final String	SCHEDULE_BLUETOOTH_MAXIMUM			= "schedule_bluetooth_max";
	public static final String	SCHEDULE_BLUETOOTH_GENERAL			= "schedule_bluetooth_gen";
	public static final String	SCHEDULE_BLUETOOTH_MINIMUM			= "schedule_bluetooth_min";
	
	public static final String	SCHEDULE_PROCESS_POWER				= "schedule_process_pow";
	public static final String	SCHEDULE_PROCESS_MAXIMUM			= "schedule_process_max";
	public static final String	SCHEDULE_PROCESS_GENERAL			= "schedule_process_gen";
	public static final String	SCHEDULE_PROCESS_MINIMUM			= "schedule_process_min";
	
	public static final String	SCHEDULE_SCREEN_POWER				= "schedule_screen_pow";
	public static final String	SCHEDULE_SCREEN_MAXIMUM				= "schedule_screen_max";
	public static final String	SCHEDULE_SCREEN_GENERAL				= "schedule_screen_gen";
	public static final String	SCHEDULE_SCREEN_MINIMUM				= "schedule_screen_min";
	
	public static final String	SCHEDULE_BRIGHTNESS_POWER			= "schedulee_brightness_pow";
	public static final String	SCHEDULE_BRIGHTNESS_MAXIMUM			= "schedulee_brightness_max";
	public static final String	SCHEDULE_BRIGHTNESS_GENERAL			= "schedulee_brightness_gen";
	public static final String	SCHEDULE_BRIGHTNESS_MINIMUM			= "schedulee_brightness_min";
	
	public static final String	SCHEDULE_TIMEOUT_POWER				= "schedule_timeout_pow";
	public static final String	SCHEDULE_TIMEOUT_MAXIMUM			= "schedule_timeout_max";
	public static final String	SCHEDULE_TIMEOUT_GENERAL			= "schedule_timeout_gen";
	public static final String	SCHEDULE_TIMEOUT_MINIMUM			= "schedule_timeout_min";
	
	//DEFAULT VALUES
	//MINUTE
	public static final int		DELAY_SHORTEST						= 5;
	public static final int		DELAY_SHORT							= 10;
	public static final int		DELAY_NORMAL						= 15;
	public static final int		DELAY_LONG							= 20;
	public static final int		DELAY_LONGEST						= 30;
	
	public static final int		SCREEN_BRIGHTNESS_AUTO				= 0;
	public static final int		SCREEN_BRIGHTNESS_MAX				= 255;
	public static final int		SCREEN_BRIGHTNESS_MID				= 180;
	public static final int		SCREEN_BRIGHTNESS_MIN				= 104;

	//SECOND
	public static final int		SCREEN_TIMEOUT_SHORTEST				= 5;
	public static final int		SCREEN_TIMEOUT_SHORT				= 10;
	public static final int		SCREEN_TIMEOUT_NORMAL				= 15;
	public static final int		SCREEN_TIMEOUT_LONG					= 30;
	public static final int		SCREEN_TIMEOUT_LONGEST				= 60;
	
	public static final int		POWER_DELAY							= DELAY_SHORTEST;
	public static final boolean	POWER_WIFI							= true;
	public static final boolean	POWER_BLUETOOTH						= true;
	public static final boolean	POWER_PROCESS						= false;
	public static final boolean	POWER_SCREEN						= true;
	public static final int		POWER_BRIGHTNESS					= SCREEN_BRIGHTNESS_MAX;
	public static final int		POWER_TIMEOUT						= SCREEN_TIMEOUT_LONGEST;
	
	public static final int		MAXIMUM_DELAY						= DELAY_SHORTEST;
	public static final int		MAXIMUM_RANGE_START					= 80;
	public static final int		MAXIMUM_RANGE_END					= 100;
	public static final boolean	MAXIMUM_WIFI						= true;
	public static final boolean	MAXIMUM_BLUETOOTH					= true;
	public static final boolean	MAXIMUM_PROCESS						= false;
	public static final boolean	MAXIMUM_SCREEN						= true;
	public static final int		MAXIMUM_BRIGHTNESS					= SCREEN_BRIGHTNESS_MAX;
	public static final int		MAXIMUM_TIMEOUT						= SCREEN_TIMEOUT_LONG;
	
	public static final int		GENERAL_DELAY						= DELAY_SHORT;
	public static final int		GENERAL_RANGE_START					= 40;
	public static final int		GENERAL_RANGE_END					= 80;
	public static final boolean	GENERAL_WIFI						= true;
	public static final boolean	GENERAL_BLUETOOTH					= true;
	public static final boolean	GENERAL_PROCESS						= false;
	public static final boolean	GENERAL_SCREEN						= true;
	public static final int		GENERAL_BRIGHTNESS					= SCREEN_BRIGHTNESS_MID;
	public static final int		GENERAL_TIMEOUT						= SCREEN_TIMEOUT_NORMAL;
	
	public static final int		MINIMUM_DELAY						= DELAY_LONG;
	public static final int		MINIMUM_RANGE_START					= 0;
	public static final int		MINIMUM_RANGE_END					= 40;
	public static final boolean	MINIMUM_WIFI						= true;
	public static final boolean	MINIMUM_BLUETOOTH					= true;
	public static final boolean	MINIMUM_PROCESS						= false;
	public static final boolean	MINIMUM_SCREEN						= true;
	public static final int		MINIMUM_BRIGHTNESS					= SCREEN_BRIGHTNESS_MIN;
	public static final int		MINIMUM_TIMEOUT						= SCREEN_TIMEOUT_SHORT;
}
